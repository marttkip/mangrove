<?php

class Reports_model extends CI_Model 
{
	public function get_queue_total($date = NULL, $where = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		if($where == NULL)
		{
			$where = 'close_card = 0 AND visit_date = \''.$date.'\'';
		}
		
		else
		{
			$where .= ' AND close_card = 0 AND visit_date = \''.$date.'\' ';
		}
		
		$this->db->select('COUNT(visit_id) AS queue_total');
		$this->db->where($where);
		$query = $this->db->get('visit');
		
		$result = $query->row();
		
		return $result->queue_total;
	}
	
	public function get_daily_balance($date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		//select the user by email from the database
		$this->db->select('SUM(visit_charge_units*visit_charge_amount) AS total_amount');
		$this->db->where('visit_charge_timestamp LIKE \''.$date.'%\'');
		$this->db->from('visit_charge');
		$query = $this->db->get();
		
		$result = $query->row();
		
		return $result->total_amount;
	}
	
	public function get_patients_total($date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		$this->db->select('COUNT(visit_id) AS patients_total');
		$this->db->where('visit_date = \''.$date.'\'');
		$query = $this->db->get('visit');
		
		$result = $query->row();
		
		return $result->patients_total;
	}
	
	public function get_all_payment_methods()
	{
		$this->db->select('*');
		$query = $this->db->get('payment_method');
		
		return $query;
	}
	
	public function get_payment_method_total($payment_method_id, $date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where('payments.visit_id = visit.visit_id AND payment_method_id = '.$payment_method_id.' AND visit_date = \''.$date.'\'');
		$query = $this->db->get('payments, visit');
		
		$result = $query->row();
		
		return $result->total_paid;
	}
	
	public function get_all_order_types()
	{
		$this->db->select('*');
		$query = $this->db->get('order_status');
		
		return $query;
	}
	
	public function get_orders_total($order_status_id, $date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		$where = 'created LIKE \''.$date.'%\' AND order_status = '.$order_status_id;
		
		$this->db->select('COUNT(order_id) AS total');
		$this->db->where($where);
		$query = $this->db->get('orders');
		
		$result = $query->row();
		
		return $result->total;
	}
	
	public function get_products_total($category_id)
	{
		$where = 'product.category_id = category.category_id AND product.product_id = order_item.product_id AND (category.category_id = '.$category_id.' OR category.category_parent = '.$category_id.') AND orders.order_status = 2 AND orders.order_id = order_item.order_id';
		
		$this->db->select('SUM(quantity*price) AS total');
		$this->db->where($where);
		$query = $this->db->get('product, category, order_item, orders');
		
		$result = $query->row();
		$total = $result->total;;
		
		if($total == NULL)
		{
			$total = 0;
		}
		
		return $total;
	}
	
	public function get_all_appointments($date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		$where = 'visit.visit_type = visit_type.visit_type_id AND visit.appointment_id = 1 AND visit.visit_date >= \''.$date.'\'';
		
		$this->db->select('visit_date, time_start, time_end, visit_type_name');
		$this->db->where($where);
		$query = $this->db->get('visit, visit_type');
		
		return $query;
	}
	
	public function get_all_sessions($date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		$where = 'personnel.personnel_id = session.personnel_id AND session.session_name_id = session_name.session_name_id AND session_time LIKE \''.$date.'%\'';
		
		$this->db->select('session_name_name, session_time, personnel_fname, personnel_onames');
		$this->db->where($where);
		$this->db->order_by('session_time', 'DESC');
		$query = $this->db->get('session, session_name, personnel');
		
		return $query;
	}
	
	public function get_usage_total()
	{
		$this->db->select('SUM(clicks) AS total');
		$query = $this->db->get('product');	
		
		$result = $query->row();
		
		return $result->total;
	}
	
	public function get_total_airlines()
	{
		$this->db->select('COUNT(airline_id) AS total_airlines');
		$this->db->where('airline_status = 1');
		$query = $this->db->get('airline');
		
		$result = $query->row();
		
		return $result->total_airlines;
	}
	
	public function get_total_visitors()
	{
		$this->db->select('COUNT(visitor_id) AS total_visitors');
		$this->db->where('visitor_status = 1');
		$query = $this->db->get('visitor');
		
		$result = $query->row();
		
		return $result->total_visitors;
	}
	
	public function get_active_flights()
	{
		$this->db->select('COUNT(flight_id) AS total_flights');
		$this->db->where('flight_status = 1');
		$query = $this->db->get('flight');
		
		$result = $query->row();
		
		return $result->total_flights;
	}
	
	public function get_total_payments()
	{
		//select the user by email from the database
		$this->db->select('SUM(payment_amount*payment_quantity) AS total_payments');
		$this->db->where('payment_status = 1');
		$this->db->from('payment');
		$query = $this->db->get();
		
		$result = $query->row();
		
		return $result->total_payments;
	}

	public function get_payment_methods()
	{
		$this->db->select('*');
		$query = $this->db->get('payment_method');

		return $query;
	}
	public function get_personnel_by_type($job_title_name=NULL)
	{

		$this->db->where('personnel.personnel_id > 0 and personnel.personnel_status = 1');
		$query = $this->db->get('personnel');

		return $query;

	}
	public function get_payments_amount($payment_method_id)
	{

		$dashboard_date_from = $this->session->userdata('dashboard_date_from'); 
		$dashboard_date_to = $this->session->userdata('dashboard_date_to'); 
		if(!empty($dashboard_date_from) && !empty($dashboard_date_to))
		{
			$visit_date = ' AND pos_payments.payment_date BETWEEN \''.$dashboard_date_from.'\' AND \''.$dashboard_date_to.'\'';
	
		}

		else if(!empty($dashboard_date_from))
		{
			$visit_date = ' AND pos_payments.payment_date = \''.$dashboard_date_from.'\'';
			
		}

		else if(empty($dashboard_date_from) AND !empty($dashboard_date_to))
		{
			$visit_date = ' AND pos_payments.payment_date = \''.$dashboard_date_to.'\'';
		
		}

		else
		{
			$visit_date = ' AND pos_payments.payment_date = \''.date('Y-m-d').'\'';
			
		}

		$where = 'pos_payments.payment_method_id = payment_method.payment_method_id AND pos_payments.pos_order_id = pos_order.pos_order_id AND pos_payments.payment_id = pos_payment_item.payment_id  AND pos_payments.cancel = 0 AND order_invoice.pos_order_id = pos_order.pos_order_id AND pos_payments.payment_method_id = '.$payment_method_id.$visit_date;

		$table = 'pos_payments,pos_payment_item ,pos_order,order_invoice, payment_method';

		$this->db->select('SUM(pos_payments.amount_paid) AS total_amount');
		$this->db->where($where);
		$query = $this->db->get($table);

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach($query->result() AS $key => $value)
			{
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}
		
		return $total_amount;

	}




	public function get_personnel_revenue($personnel_id,$store_id,$sale_type = null)
	{
	

		$where = 'pos_order.pos_order_id = pos_order_item.pos_order_id AND pos_order_item.order_invoice_id > 0  AND pos_order_item.pos_order_item_deleted = 0 AND pos_order_deleted = 0 AND pos_order_item.product_id = product.product_id AND pos_order.created_by = '.$personnel_id;
		$dashboard_date_from = $this->session->userdata('dashboard_date_from'); 
		$dashboard_date_to = $this->session->userdata('dashboard_date_to'); 

		if(!empty($dashboard_date_from) && !empty($dashboard_date_to))
		{
			$visit_date = ' AND pos_order.order_date BETWEEN \''.$dashboard_date_from.'\' AND \''.$dashboard_date_to.'\'';
	
		}

		else if(!empty($dashboard_date_from))
		{
			$visit_date = ' AND pos_order.order_date = \''.$dashboard_date_from.'\'';
			
		}

		else if(empty($dashboard_date_from) AND !empty($dashboard_date_to))
		{
			$visit_date = ' AND pos_order.order_date = \''.$dashboard_date_to.'\'';
		
		}

		else
		{
			$visit_date = ' AND pos_order.order_date = \''.date('Y-m-d').'\'';
			
		}

		// if($type == 0)
		// {
		// 	$where .= ' AND pos_order.sale_type = 0';
		// }
		// else
		// {
			// $where .= ' AND pos_order_item.store_id ='.$store_id;
		// }


			if(!empty($sale_type))
			{
				$where .= ' AND pos_order.sale_type = '.$sale_type;
			}
			else
			{
				$where .= ' AND pos_order.sale_type = 0';
			}


		$this->db->where($where.$visit_date);
		$this->db->select('SUM(pos_order_item.pos_order_item_quantity*pos_order_item.pos_order_item_amount)  AS total_amount');
		$query = $this->db->get('pos_order,pos_order_item,product');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}




		$where = 'pos_order.pos_order_id = pos_order_item.pos_order_id AND pos_order_item.pos_order_item_deleted = 0 AND pos_order_item.order_invoice_id > 0 AND pos_order_deleted = 0 AND pos_order_item.pos_order_item_id = pos_order_item_return.pos_order_item_id AND pos_order_item_return.pos_order_item_return_delete = 0 AND pos_order.pos_order_id = pos_order_return.pos_order_id AND pos_order_return.pos_order_return_id = pos_order_item_return.pos_order_return_id AND pos_order.created_by = '.$personnel_id;

			if(!empty($dashboard_date_from) && !empty($dashboard_date_to))
			{
				$where .= ' AND pos_order.order_date BETWEEN \''.$dashboard_date_from.'\' AND \''.$dashboard_date_to.'\'';
		
			}

			else if(!empty($dashboard_date_from))
			{
				$where .= ' AND pos_order.order_date = \''.$dashboard_date_from.'\'';
				
			}

			else if(empty($dashboard_date_from) AND !empty($dashboard_date_to))
			{
				$where .= ' AND pos_order.order_date = \''.$dashboard_date_to.'\'';
			
			}

			else
			{
				$where .= ' AND pos_order.order_date = \''.date('Y-m-d').'\'';
				
			}

			// if($type == 0)
			// {
			// 	$where .= ' AND pos_order.sale_type = 0';
			// }
			// else
			// {
				$where .= ' AND pos_order_item_return.store_id = '.$store_id;
			// }

			if(!empty($sale_type))
			{
				$where .= ' AND pos_order.sale_type = '.$sale_type;
			}
			else
			{
				$where .= ' AND pos_order.sale_type = 0';
			}

        $this->db->select('SUM((pos_order_item_return.units* pos_order_item_return.product_order_item_amount)) AS total_amount');
        $this->db->where($where);
        $query = $this->db->get('pos_order,pos_order_item,pos_order_item_return,pos_order_return');

        $total_return_amount = 0;

        if($query->num_rows() > 0)
        {
            foreach ($query->result() as $key => $value) {
                # code...
                $total_return_amount = $value->total_amount;
            }
        }

        if(empty($total_return_amount))
        {
            $total_return_amount = 0;
        }
        $total_amount = $total_amount - $total_return_amount;


        $total_amount = round($total_amount);


		return $total_amount;
	}


	public function get_personnel_buying_price($personnel_id,$store_id,$sale_type=null)
	{
	

		$where = 'pos_order.pos_order_id = pos_order_item.pos_order_id AND pos_order_item.product_id = product.product_id AND pos_order_item.pos_order_item_deleted = 0 AND pos_order_item.order_invoice_id > 0 AND pos_order_deleted = 0 AND pos_order.created_by = '.$personnel_id;
		$dashboard_date_from = $this->session->userdata('dashboard_date_from'); 
		$dashboard_date_to = $this->session->userdata('dashboard_date_to'); 

		if(!empty($dashboard_date_from) && !empty($dashboard_date_to))
		{
			$visit_date = ' AND pos_order.order_date BETWEEN \''.$dashboard_date_from.'\' AND \''.$dashboard_date_to.'\'';
	
		}

		else if(!empty($dashboard_date_from))
		{
			$visit_date = ' AND pos_order.order_date = \''.$dashboard_date_from.'\'';
			
		}

		else if(empty($dashboard_date_from) AND !empty($dashboard_date_to))
		{
			$visit_date = ' AND pos_order.order_date = \''.$dashboard_date_to.'\'';
		
		}

		else
		{
			$visit_date = ' AND pos_order.order_date = \''.date('Y-m-d').'\'';
			
		}

		// if($type == 0)
		// {
		// 	$where .= ' AND pos_order.sale_type = 0';
		// }
		// else
		// {
			// $where .= ' AND pos_order_item.store_id = '.$store_id;
		// }

			if(!empty($sale_type))
			{
				$where .= ' AND pos_order.sale_type = '.$sale_type;
			}
			else
			{
				$where .= ' AND pos_order.sale_type = 0';
			}



		$this->db->where($where.$visit_date);
		$this->db->select('SUM(product.product_buying_price*pos_order_item.pos_order_item_quantity) AS total_amount');
		$query = $this->db->get('pos_order,pos_order_item,product');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}
		return $total_amount;
	}

	public function get_personnel_collection($personnel_id)
	{
		$visit_search = $this->session->userdata('cash_report_search');

		$where = 'pos_order.pos_order_id = pos_payments.pos_order_id AND pos_payments.cancel = 0 AND pos_order.pos_order_deleted = 0 AND pos_order.created_by = '.$personnel_id;
		

		$dashboard_date_from = $this->session->userdata('dashboard_date_from'); 
		$dashboard_date_to = $this->session->userdata('dashboard_date_to'); 
		if(!empty($dashboard_date_from) && !empty($dashboard_date_to))
		{
			$visit_date = ' AND pos_payments.payment_date BETWEEN \''.$dashboard_date_from.'\' AND \''.$dashboard_date_to.'\'';
	
		}

		else if(!empty($dashboard_date_from))
		{
			$visit_date = ' AND pos_payments.payment_date = \''.$dashboard_date_from.'\'';
			
		}

		else if(empty($dashboard_date_from) AND !empty($dashboard_date_to))
		{
			$visit_date = ' AND pos_payments.payment_date = \''.$dashboard_date_to.'\'';
		
		}

		else
		{
			$visit_date = ' AND pos_payments.payment_date = \''.date('Y-m-d').'\'';
			
		}

		$this->db->where($where);
		$this->db->select('SUM(amount_paid) AS total_amount');
		$query = $this->db->get('pos_payments,pos_order');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}
		return $total_amount;
	}

	public function get_product_purchases($store_id)
	{


		$where = 'v_creditor_ledger_aging.transactionId = orders.order_id AND (v_creditor_ledger_aging.transactionClassification = "Supplies Credit Note" OR v_creditor_ledger_aging.transactionClassification = "Supplies Invoices")';
		$dashboard_date_from = $this->session->userdata('dashboard_date_from'); 
		$dashboard_date_to = $this->session->userdata('dashboard_date_to'); 

		if(!empty($dashboard_date_from) && !empty($dashboard_date_to))
		{
			$visit_date = ' AND v_creditor_ledger_aging.transactionDate BETWEEN \''.$dashboard_date_from.'\' AND \''.$dashboard_date_to.'\'';
	
		}

		else if(!empty($dashboard_date_from))
		{
			$visit_date = ' AND v_creditor_ledger_aging.transactionDate = \''.$dashboard_date_from.'\'';
			
		}

		else if(empty($dashboard_date_from) AND !empty($dashboard_date_to))
		{
			$visit_date = ' AND v_creditor_ledger_aging.transactionDate = \''.$dashboard_date_to.'\'';
		
		}

		else
		{
			$visit_date = ' AND v_creditor_ledger_aging.transactionDate = \''.date('Y-m-d').'\'';
			
		}

		$where .= ' AND orders.store_id = '.$store_id;
		


		$this->db->where($where.$visit_date);
		$this->db->select('(SUM(v_creditor_ledger_aging.dr_amount) - SUM(v_creditor_ledger_aging.cr_amount)) AS total_amount');
		$query = $this->db->get('v_creditor_ledger_aging,orders');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}
		return $total_amount;
	}



	public function get_petty_cash_expense()
	{


		$where = 'finance_purchase.finance_purchase_deleted = 0';

		$dashboard_date_from = $this->session->userdata('dashboard_date_from'); 
		$dashboard_date_to = $this->session->userdata('dashboard_date_to'); 

		if(!empty($dashboard_date_from) && !empty($dashboard_date_to))
		{
			$visit_date = ' AND finance_purchase.transaction_date BETWEEN \''.$dashboard_date_from.'\' AND \''.$dashboard_date_to.'\'';
	
		}

		else if(!empty($dashboard_date_from))
		{
			$visit_date = ' AND finance_purchase.transaction_date = \''.$dashboard_date_from.'\'';
			
		}

		else if(empty($dashboard_date_from) AND !empty($dashboard_date_to))
		{
			$visit_date = ' AND finance_purchase.transaction_date = \''.$dashboard_date_to.'\'';
		
		}

		else
		{
			$visit_date = ' AND finance_purchase.transaction_date = \''.date('Y-m-d').'\'';
			
		}

	


		$this->db->where($where.$visit_date);
		$this->db->select('SUM(finance_purchase.finance_purchase_amount) AS total_amount');
		$query = $this->db->get('finance_purchase');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}
		return $total_amount;
	}

	public function get_other_bills()
	{


		$where = 'account_payments.account_payment_deleted = 0';

		$dashboard_date_from = $this->session->userdata('dashboard_date_from'); 
		$dashboard_date_to = $this->session->userdata('dashboard_date_to'); 
		
		if(!empty($dashboard_date_from) && !empty($dashboard_date_to))
		{
			$visit_date = ' AND account_payments.payment_date BETWEEN \''.$dashboard_date_from.'\' AND \''.$dashboard_date_to.'\'';
	
		}

		else if(!empty($dashboard_date_from))
		{
			$visit_date = ' AND account_payments.payment_date = \''.$dashboard_date_from.'\'';
			
		}

		else if(empty($dashboard_date_from) AND !empty($dashboard_date_to))
		{
			$visit_date = ' AND account_payments.payment_date = \''.$dashboard_date_to.'\'';
		
		}

		else
		{
			$visit_date = ' AND account_payments.payment_date = \''.date('Y-m-d').'\'';
			
		}

	


		$this->db->where($where.$visit_date);
		$this->db->select('SUM(account_payments.amount_paid) AS total_amount');
		$query = $this->db->get('account_payments');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}
		return $total_amount;
	}
}