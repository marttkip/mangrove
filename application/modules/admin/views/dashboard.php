<?php
$contacts = $this->site_model->get_contacts();
$logo = $contacts['logo'];
?>

    <div class="col-md-4">
        <div class="center-align">
            <a onclick="close_account() "  >
                <i class="fa fa-lock" style="font-size: 250px !important;"></i>
                <br>
                LOCK
            </a>
        </div> 
    </div>

	<div class="col-md-8">
    	<div class="panel-body">
    	<?php
    			$success = $this->session->userdata('success_message');
    			$error = $this->session->userdata('error_message');
    			
    			if(!empty($success))
    			{
    				echo '
    					<div class="alert alert-success">'.$success.'</div>
    				';
    				
    				$this->session->unset_userdata('success_message');
    			}
    			
    			if(!empty($error))
    			{
    				echo '
    					<div class="alert alert-danger">'.$error.'</div>
    				';
    				
    				$this->session->unset_userdata('error_message');
    			}
    			
    		?>
          
    		<div class="row">
                <div class="col-md-4">
                    <img src="<?php echo base_url();?>assets/logo/<?php echo $logo;?>">
                </div>
                <div class="col-md-6">
                    <h5><strong>Profile Details</strong></h5>
                    <div class="form-group">
                        <label class="col-md-4 control-label"><strong> Personnel Name:</strong> </label>
                        
                        <div class="col-md-8">
                            <?php echo $this->session->userdata('first_name');?>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"> <strong>Username:</strong> </label>
                        
                        <div class="col-lg-8">
                            <?php echo $this->session->userdata('username');?>
                        </div>
                        
                    </div>
                 
                </div>
                
    		</div>
    	</div>
    </div>

	<script type="text/javascript">
     
     function close_account()
    {
        // alert("dadada");
            var config_url = $('#config_url').val();
            
        window.location.href = config_url+"logout-admin";
    }   
    </script>