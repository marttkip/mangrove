<?php
$personnel_id = $this->session->userdata('personnel_id');
$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');

if($personnel_id > 0 AND $authorize_invoice_changes == FALSE)
{


    $chart_array = array();
    $total_charts = '';
        $total_wholesale_charts = '';
        for ($i = 12; $i >= 0; $i--) {
        $months = date("Y-m", strtotime( date( 'Y-m-d' )." -$i months"));
        $months_explode = explode('-', $months);
        $year = $months_explode[0];
        $month = $months_explode[1];
        $last_visit = date('M Y',strtotime($months));

        $where = 'pos_order.pos_order_id = pos_order_item.pos_order_id  AND YEAR(pos_order.order_date) = '.$year.' AND MONTH(pos_order.order_date) = "'.$month.'" AND pos_order_item.order_invoice_id > 0 AND pos_order.sale_type = 0 AND pos_order_item.pos_order_item_deleted = 0 AND pos_order.pos_order_deleted = 0 AND product.product_id = pos_order_item.product_id';
        $table = 'pos_order,pos_order_item,product';
        $select = 'SUM(pos_order_item.pos_order_item_amount * pos_order_item.pos_order_item_quantity) AS number';

        $total_retail_sales = $this->dashboard_model->count_items_group($table, $where, $select);



         $where = 'pos_order.pos_order_id = pos_order_item.pos_order_id  AND YEAR(pos_order.order_date) = '.$year.' AND MONTH(pos_order.order_date) = "'.$month.'" AND pos_order_item.order_invoice_id > 0 AND (pos_order.sale_type = 1 OR pos_order.sale_type = 2)  AND pos_order_item.pos_order_item_deleted = 0 AND pos_order.pos_order_deleted = 0 AND product.product_id = pos_order_item.product_id';
        $table = 'pos_order,pos_order_item,product';
        $select = 'SUM(pos_order_item.pos_order_item_amount * pos_order_item.pos_order_item_quantity) AS number';

        $total_wholesale_sales = $this->dashboard_model->count_items_group($table, $where, $select);


        $total_charts .= '["'.$last_visit.'", '.$total_retail_sales.'],';


        $total_wholesale_charts .= '["'.$last_visit.'", '.$total_wholesale_sales.'],';

    }



    ?>
                        <!-- start: page -->
                        <div class="row">
                            <div class="col-md-6 ">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="chart-data-selector" id="salesSelectorWrapper">
                                                    <h2>
                                                        Sales:
                                                        <strong>
                                                            <select class="form-control" id="salesSelector">
                                                                <option value="Porto Admin" selected>Retail</option>
                                                                <option value="Porto Drupal" selected>Wholesale</option>
                                                            </select>
                                                        </strong>
                                                    </h2>

                                                    <div id="salesSelectorItems" class="chart-data-selector-items mt-sm">
                                                        <!-- Flot: Sales Porto Admin -->
                                                        <div class="chart chart-sm" data-sales-rel="Porto Admin" id="flotDashSales1" class="chart-active"></div>
                                                        <script>

                                                            var flotDashSales1Data = [{
                                                                data: [
                                                                    <?php echo $total_charts?>
                                                                ],
                                                                color: "#CCCCCC"
                                                            }];

                                                            // See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

                                                        </script>

                                                        <!-- Flot: Sales Porto Drupal -->
                                                        <div class="chart chart-sm" data-sales-rel="Porto Drupal" id="flotDashSales2" class="chart-hidden"></div>
                                                        <script>

                                                            var flotDashSales2Data = [{
                                                                data: [
                                                                    <?php echo $total_wholesale_charts?>
                                                                ],
                                                                color: "#2baab1"
                                                            }];

                                                            // See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

                                                        </script>

                                                        
                                                    </div>

                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </section>
                            </div>
    <?php

    $date_today = date('Y-m-d');
    $where = 'pos_order.pos_order_id = pos_order_item.pos_order_id  AND pos_order.order_date = "'.$date_today.'" AND pos_order_item.order_invoice_id > 0 AND pos_order.sale_type = 0 AND pos_order_item.pos_order_item_deleted = 0 AND pos_order.pos_order_deleted = 0';
    $table = 'pos_order,pos_order_item';
    $retail_orders = $this->dashboard_model->count_items($table, $where);


    $where = 'pos_order.pos_order_id = pos_order_item.pos_order_id  AND pos_order.order_date = "'.$date_today.'" AND pos_order_item.order_invoice_id > 0 AND pos_order.sale_type = 0 AND pos_order_item.pos_order_item_deleted = 0 AND pos_order.pos_order_deleted = 0';
    $table = 'pos_order,pos_order_item';
    $select = 'SUM(pos_order_item.pos_order_item_quantity) AS number';
    // $cpm_group = 'pos_order_item.pos_order_id';
    $total_retail_order_items = $this->dashboard_model->count_items_group($table, $where, $select);



    $where = 'pos_order.pos_order_id = pos_order_item.pos_order_id  AND pos_order.order_date = "'.$date_today.'" AND pos_order_item.order_invoice_id > 0 AND (pos_order.sale_type = 1 OR pos_order.sale_type = 2) AND pos_order_item.pos_order_item_deleted = 0 AND pos_order.pos_order_deleted = 0';
    $table = 'pos_order,pos_order_item';
    $wholesale_orders = $this->dashboard_model->count_items($table, $where);


    $where = 'pos_order.pos_order_id = pos_order_item.pos_order_id  AND pos_order.order_date = "'.$date_today.'" AND pos_order_item.order_invoice_id > 0 AND (pos_order.sale_type = 1 OR pos_order.sale_type = 2) AND pos_order_item.pos_order_item_deleted = 0 AND pos_order.pos_order_deleted = 0';
    $table = 'pos_order,pos_order_item';
    $select = 'SUM(pos_order_item.pos_order_item_quantity) AS number';
    // $cpm_group = 'pos_order_item.pos_order_id';
    $total_wholesale_order_items = $this->dashboard_model->count_items_group($table, $where, $select);





    $where = 'pos_payments.payment_id = pos_payment_item.payment_id AND pos_payments.cancel = 0 AND pos_payments.payment_date = "'.$date_today.'" ';
    $table = 'pos_payments,pos_payment_item';
    $select = 'SUM(pos_payment_item.payment_item_amount) AS number';
    $total_money_received = $this->dashboard_model->count_items_group($table, $where, $select);


    // profit 


    $where = 'pos_order.pos_order_id = pos_order_item.pos_order_id  AND pos_order.order_date = "'.$date_today.'" AND pos_order_item.order_invoice_id > 0 AND pos_order.sale_type <3 AND pos_order_item.pos_order_item_deleted = 0 AND pos_order.pos_order_deleted = 0 AND product.product_id = pos_order_item.product_id';
    $table = 'pos_order,pos_order_item,product';
    $select = 'SUM(product.product_buying_price*pos_order_item.pos_order_item_quantity) AS number';
    // $cpm_group = 'pos_order_item.pos_order_id';
    $total_buying_price = $this->dashboard_model->count_items_group($table, $where, $select);



    $where = 'pos_order.pos_order_id = pos_order_item.pos_order_id  AND pos_order.order_date = "'.$date_today.'" AND pos_order_item.order_invoice_id > 0 AND pos_order.sale_type < 3 AND pos_order_item.pos_order_item_deleted = 0 AND pos_order.pos_order_deleted = 0 AND product.product_id = pos_order_item.product_id';
    $table = 'pos_order,pos_order_item,product';
    $select = 'SUM(pos_order_item.pos_order_item_amount * (pos_order_item.pos_order_item_quantity)) AS number';
    // $cpm_group = 'pos_order_item.pos_order_id';
    $total_selling_price = $this->dashboard_model->count_items_group($table, $where, $select);



    $profit = $total_selling_price - $total_buying_price;



    ?>
                            <div class="col-md-6 ">
                                <div class="row">
                                    <div class="col-md-6">
                                        <section class="panel panel-featured-left panel-featured-primary">
                                            <div class="panel-body">
                                                <div class="widget-summary">
                                                    <div class="widget-summary-col widget-summary-col-icon">
                                                        <div class="summary-icon bg-primary">
                                                            <i class="fa fa-shopping-cart"></i>
                                                        </div>
                                                    </div>
                                                    <div class="widget-summary-col">
                                                        <div class="summary">
                                                            <h4 class="title">Retail Sales</h4>
                                                            <div class="info">
                                                                <strong class="amount"><?php echo $retail_orders?> Orders</strong>
                                                                <span class="text-primary">(<?php echo $total_retail_order_items?> items sold)</span>
                                                            </div>
                                                        </div>
                                                        <div class="summary-footer">
                                                            <a class="text-muted text-uppercase" href="<?php echo site_url().'administrative-reports/sales-report'?>">(view all)</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                    <div class="col-md-6">
                                        <section class="panel panel-featured-left panel-featured-secondary">
                                            <div class="panel-body">
                                                <div class="widget-summary">
                                                    <div class="widget-summary-col widget-summary-col-icon">
                                                        <div class="summary-icon bg-secondary">
                                                            <i class="fa fa-usd"></i>
                                                        </div>
                                                    </div>
                                                    <div class="widget-summary-col">
                                                        <div class="summary">
                                                            <h4 class="title">Day Collection</h4>
                                                            <div class="info">
                                                                <strong class="amount">Kes <?php echo number_format($total_money_received,2);?></strong>
                                                            </div>
                                                        </div>
                                                        <div class="summary-footer">
                                                            <a class="text-muted text-uppercase" href="<?php echo site_url().'administrative-reports/revenue-collection-report'?>">(view)</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                    <div class="col-md-6">
                                        <section class="panel panel-featured-left panel-featured-tertiary">
                                            <div class="panel-body">
                                                <div class="widget-summary">
                                                    <div class="widget-summary-col widget-summary-col-icon">
                                                        <div class="summary-icon bg-tertiary">
                                                            <i class="fa fa-shopping-cart"></i>
                                                        </div>
                                                    </div>

                                                    <div class="widget-summary-col">
                                                        <div class="summary">
                                                            <h4 class="title">Credit Sales</h4>
                                                            <div class="info">
                                                                <strong class="amount"><?php echo $wholesale_orders?> Orders</strong>
                                                                <span class="text-primary">(<?php echo $total_wholesale_order_items?> items sold)</span>
                                                            </div>
                                                        </div>
                                                        <div class="summary-footer">
                                                            <a class="text-muted text-uppercase" href="<?php echo site_url().'administrative-reports/sales-report'?>">(view all)</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                    <div class="col-md-6">
                                        <section class="panel panel-featured-left panel-featured-quartenary">
                                            <div class="panel-body">
                                                <div class="widget-summary">
                                                    <div class="widget-summary-col widget-summary-col-icon">
                                                        <div class="summary-icon bg-quartenary">
                                                            <i class="fa fa-usd"></i>
                                                        </div>
                                                    </div>
                                                    <div class="widget-summary-col">
                                                        <div class="summary">
                                                            <h4 class="title">Today's Profit</h4>
                                                            <div class="info">
                                                                <strong class="amount">Kes. <?php echo number_format($profit,2)?></strong>
                                                            </div>
                                                        </div>
                                                        <div class="summary-footer">
                                                            <a class="text-muted text-uppercase" href="<?php echo site_url().'administrative-reports/sales-report'?>">(report)</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <section class="panel">
                                    <header class="panel-heading">
                                        <div class="panel-actions">
                                            <!-- <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                            <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
                                            <a href="<?php echo site_url();?>administrative-reports/drug-expiries" class="panel-action" > <i class="fa fa-print"></i> View All List</a>

                                        </div>

                                        <h2 class="panel-title">Product Expiries</h2>
                                        <p class="panel-subtitle"></p>
                                    </header>
                                    <div class="panel-body" style="height: 50vh;overflow-y:scroll;">

                                        <?php
                                        $query_stock = $this->admin_model->get_drugs_expiry(5);
                                        $stock_result ='';
                                        if($query_stock->num_rows() > 0)
                                        {
                                            $count = 0;
                                            foreach ($query_stock->result() as $key => $value) {
                                                // code...

                                                $product_code = $value->product_code;
                                                $product_name = $value->product_name;
                                                $stock_amount = $value->stock_amount;
                                                $whole_sale_stock = $value->whole_sale_stock;
                                                $reorder_level = $value->reorder_level;
                                                $product_id = $value->product_id;
                                                $store_id = $value->store_id;
                                                $store_name = $value->store_name;
                                                $last_expiry_date_retail = $value->last_expiry_date_retail;
                                                $last_expiry_date_wholesale = $value->last_expiry_date_wholesale;


                                                // if($store_id == 6)
                                                // {
                                                //  $quantity = $whole_sale_stock;
                                                //  $expiry_date = $last_expiry_date_wholesale;
                                                // }
                                                // else
                                                // {
                                                    $quantity = $stock_amount;
                                                    $expiry_date = $last_expiry_date_retail;
                                                // }

                                                if($expiry_date <= date('Y-m-d'))
                                                {
                                                    $class = "warning";
                                                }
                                                else
                                                {
                                                    $class = '';
                                                }

                                                $count++;
                                                $stock_result .= '<tr onclick="view_drug_trail('.$product_id.',5)">
                                                                    <td class="'.$class.'">'.$count.'</td>
                                                                    <td class="'.$class.'">'.strtoupper($product_code).'</td>
                                                                    <td class="'.$class.'">'.$product_name.'</td>
                                                                    <td class="'.$class.'">'.$expiry_date.'</td>
                                                                    <td>'.$quantity.'</td>
                                                                </tr>';

                                            }
                                        }

                                        else
                                        {
                                            $stock_result .= '<tr>
                                                                    <td colspan="5">No products out of Stock</td>
                                                                </tr>';
                                        }

                                        ?>

                                        <table class="table table-hover table-striped table-condensed table-bordered">
                                            <thead>
                                                <th></th>
                                                <th>Product Code</th>
                                                <th>Product Name</th>
                                                <th>Expiry Date</th>
                                                <th>Current Stock</th>
                                            </thead>
                                            <tbody>
                                                <?php echo $stock_result;?>
                                            </tbody>
                                        </table>

                                        <!-- Flot: Basic -->
                                        <div class="chart chart-md" id="flotDashBasic"></div>
                                        <script>

                                            var flotDashBasicData = [{
                                                data: [
                                                    [0, 170],
                                                    [1, 169],
                                                    [2, 173],
                                                    [3, 188],
                                                    [4, 147],
                                                    [5, 113],
                                                    [6, 128],
                                                    [7, 169],
                                                    [8, 173],
                                                    [9, 128],
                                                    [10, 128]
                                                ],
                                                label: "Series 1",
                                                color: "#CCCCCC"
                                            }, {
                                                data: [
                                                    [0, 115],
                                                    [1, 124],
                                                    [2, 114],
                                                    [3, 121],
                                                    [4, 115],
                                                    [5, 83],
                                                    [6, 102],
                                                    [7, 148],
                                                    [8, 147],
                                                    [9, 103],
                                                    [10, 113]
                                                ],
                                                label: "Series 2",
                                                color: "#2baab1"
                                            }, {
                                                data: [
                                                    [0, 70],
                                                    [1, 69],
                                                    [2, 73],
                                                    [3, 88],
                                                    [4, 47],
                                                    [5, 13],
                                                    [6, 28],
                                                    [7, 69],
                                                    [8, 73],
                                                    [9, 28],
                                                    [10, 28]
                                                ],
                                                label: "Series 3",
                                                color: "#734ba9"
                                            }];

                                            // See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

                                        </script>

                                    </div>
                                </section>
                            </div>
                            <div class="col-md-6">
                                <section class="panel">
                                    <header class="panel-heading">
                                        <div class="panel-actions">
                                            <!-- <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                            <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
                                            <a href="<?php echo site_url();?>administrative-reports/out-of-stock" class="panel-action" > <i class="fa fa-print"></i> View All List</a>
                                        </div>
                                        <h2 class="panel-title">Out of Stock Retail Store </h2>
                                        <p class="panel-subtitle"></p>
                                    </header>
                                    <div class="panel-body" style="height: 50vh;overflow-y:scroll;">
                                        <?php
                                        $query_stock = $this->admin_model->get_drugs_out_of_stock(5);
                                        $stock_result ='';
                                        if($query_stock->num_rows() > 0)
                                        {
                                            $count = 0;
                                            foreach ($query_stock->result() as $key => $value) {
                                                // code...

                                                $product_code = $value->product_code;
                                                $product_name = $value->product_name;
                                                $stock_amount = $value->stock_amount;
                                                $whole_sale_stock = $value->whole_sale_stock;
                                                $reorder_level = $value->reorder_level;
                                                $product_id = $value->product_id;
                                                $store_id = $value->store_id;
                                                $store_name = $value->store_name;

                                                if($store_id == 6)
                                                {

                                                    $quantity = $whole_sale_stock;
                                                }
                                                else
                                                {
                                                    $quantity = $stock_amount;
                                                }



                                                $count++;
                                                $stock_result .= '<tr onclick="view_drug_trail('.$product_id.',5)">
                                                                    <td>'.$count.'</td>
                                                                    <td>'.strtoupper($product_code).'</td>
                                                                    <td>'.$product_name.'</td>
                                                                    <td>'.$reorder_level.'</td>
                                                                    <td>'.$quantity.'</td>
                                                                </tr>';

                                            }
                                        }

                                        else
                                        {
                                            $stock_result .= '<tr>
                                                                    <td colspan="5">No products out of Stock</td>
                                                                </tr>';
                                        }

                                        ?>

                                        <table class="table table-hover table-striped table-condensed table-bordered">
                                            <thead>
                                                <th></th>
                                                <th>Product Code</th>
                                                <th>Product Name</th>
                                                <th>Reorder Level</th>
                                                <th>Current Stock</th>
                                            </thead>
                                            <tbody>
                                                <?php echo $stock_result;?>
                                            </tbody>
                                        </table>
                                        

                                    </div>
                                </section>
                            </div>
                        </div>
<?php
}
else if($personnel_id == 0 OR $authorize_invoice_changes)
{
    ?>

    <?php

    $chart_array = array();
    $total_charts = '';
        $total_wholesale_charts = '';
        for ($i = 12; $i >= 0; $i--) {
        $months = date("Y-m", strtotime( date( 'Y-m-d' )." -$i months"));
        $months_explode = explode('-', $months);
        $year = $months_explode[0];
        $month = $months_explode[1];
        $last_visit = date('M Y',strtotime($months));

        $where = 'pos_order.pos_order_id = pos_order_item.pos_order_id  AND YEAR(pos_order.order_date) = '.$year.' AND MONTH(pos_order.order_date) = "'.$month.'" AND pos_order_item.order_invoice_id > 0 AND pos_order.sale_type = 0 AND pos_order_item.pos_order_item_deleted = 0 AND pos_order.pos_order_deleted = 0 AND product.product_id = pos_order_item.product_id';
        $table = 'pos_order,pos_order_item,product';
        $select = 'SUM(pos_order_item.pos_order_item_amount * pos_order_item.pos_order_item_quantity) AS number';

        $total_retail_sales = $this->dashboard_model->count_items_group($table, $where, $select);



         $where = 'pos_order.pos_order_id = pos_order_item.pos_order_id  AND YEAR(pos_order.order_date) = '.$year.' AND MONTH(pos_order.order_date) = "'.$month.'" AND pos_order_item.order_invoice_id > 0 AND (pos_order.sale_type = 1 OR pos_order.sale_type = 2)  AND pos_order_item.pos_order_item_deleted = 0 AND pos_order.pos_order_deleted = 0 AND product.product_id = pos_order_item.product_id';
        $table = 'pos_order,pos_order_item,product';
        $select = 'SUM(pos_order_item.pos_order_item_amount * pos_order_item.pos_order_item_quantity) AS number';

        $total_wholesale_sales = $this->dashboard_model->count_items_group($table, $where, $select);


        $total_charts .= '["'.$last_visit.'", '.$total_retail_sales.'],';


        $total_wholesale_charts .= '["'.$last_visit.'", '.$total_wholesale_sales.'],';

    }



    ?>
    <!-- start: page -->
    <div class="row">
        <div class="col-md-6 ">
            <section class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="chart-data-selector" id="salesSelectorWrapper">
                                <h2>
                                    Sales:
                                    <strong>
                                        <select class="form-control" id="salesSelector">
                                            <option value="Porto Admin" selected>Retail</option>
                                            <option value="Porto Drupal" selected>Wholesale</option>
                                        </select>
                                    </strong>
                                </h2>

                                <div id="salesSelectorItems" class="chart-data-selector-items mt-sm">
                                    <!-- Flot: Sales Porto Admin -->
                                    <div class="chart chart-sm" data-sales-rel="Porto Admin" id="flotDashSales1" class="chart-active"></div>
                                    <script>

                                        var flotDashSales1Data = [{
                                            data: [
                                                <?php echo $total_charts?>
                                            ],
                                            color: "#CCCCCC"
                                        }];

                                        // See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

                                    </script>

                                    <!-- Flot: Sales Porto Drupal -->
                                    <div class="chart chart-sm" data-sales-rel="Porto Drupal" id="flotDashSales2" class="chart-hidden"></div>
                                    <script>

                                        var flotDashSales2Data = [{
                                            data: [
                                                <?php echo $total_wholesale_charts?>
                                            ],
                                            color: "#2baab1"
                                        }];

                                        // See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

                                    </script>

                                    
                                </div>

                            </div>
                        </div>
                        
                    </div>
                </div>
            </section>
        </div>
        <?php

    $date_today = date('Y-m-d');
    $where = 'pos_order.pos_order_id = pos_order_item.pos_order_id  AND pos_order.order_date = "'.$date_today.'" AND pos_order_item.order_invoice_id > 0 AND pos_order.sale_type = 0 AND pos_order_item.pos_order_item_deleted = 0 AND pos_order.pos_order_deleted = 0';
    $table = 'pos_order,pos_order_item';
    $retail_orders = $this->dashboard_model->count_items($table, $where);


    $where = 'pos_order.pos_order_id = pos_order_item.pos_order_id  AND pos_order.order_date = "'.$date_today.'" AND pos_order_item.order_invoice_id > 0 AND pos_order.sale_type = 0 AND pos_order_item.pos_order_item_deleted = 0 AND pos_order.pos_order_deleted = 0';
    $table = 'pos_order,pos_order_item';
    $select = 'SUM(pos_order_item.pos_order_item_quantity) AS number';
    // $cpm_group = 'pos_order_item.pos_order_id';
    $total_retail_order_items = $this->dashboard_model->count_items_group($table, $where, $select);



    $where = 'pos_order.pos_order_id = pos_order_item.pos_order_id  AND pos_order.order_date = "'.$date_today.'" AND pos_order_item.order_invoice_id > 0 AND (pos_order.sale_type = 1 OR pos_order.sale_type = 2) AND pos_order_item.pos_order_item_deleted = 0 AND pos_order.pos_order_deleted = 0';
    $table = 'pos_order,pos_order_item';
    $wholesale_orders = $this->dashboard_model->count_items($table, $where);


    $where = 'pos_order.pos_order_id = pos_order_item.pos_order_id  AND pos_order.order_date = "'.$date_today.'" AND pos_order_item.order_invoice_id > 0 AND (pos_order.sale_type = 1 OR pos_order.sale_type = 2) AND pos_order_item.pos_order_item_deleted = 0 AND pos_order.pos_order_deleted = 0';
    $table = 'pos_order,pos_order_item';
    $select = 'SUM(pos_order_item.pos_order_item_quantity) AS number';
    // $cpm_group = 'pos_order_item.pos_order_id';
    $total_wholesale_order_items = $this->dashboard_model->count_items_group($table, $where, $select);





    $where = 'pos_payments.payment_id = pos_payment_item.payment_id AND pos_payments.cancel = 0 AND pos_payments.payment_date = "'.$date_today.'" ';
    $table = 'pos_payments,pos_payment_item';
    $select = 'SUM(pos_payment_item.payment_item_amount) AS number';
    $total_money_received = $this->dashboard_model->count_items_group($table, $where, $select);


    // profit 


    $where = 'pos_order.pos_order_id = pos_order_item.pos_order_id  AND pos_order.order_date = "'.$date_today.'" AND pos_order_item.order_invoice_id > 0 AND pos_order.sale_type < 3 AND pos_order_item.pos_order_item_deleted = 0 AND pos_order.pos_order_deleted = 0 AND product.product_id = pos_order_item.product_id';
    $table = 'pos_order,pos_order_item,product';
    $select = 'SUM(product.product_buying_price*pos_order_item.pos_order_item_quantity) AS number';
    // $cpm_group = 'pos_order_item.pos_order_id';
    $total_buying_price = $this->dashboard_model->count_items_group($table, $where, $select);



    $where = 'pos_order.pos_order_id = pos_order_item.pos_order_id  AND pos_order.order_date = "'.$date_today.'" AND pos_order_item.order_invoice_id > 0 AND pos_order.sale_type < 3 AND pos_order_item.pos_order_item_deleted = 0 AND pos_order.pos_order_deleted = 0 AND product.product_id = pos_order_item.product_id';
    $table = 'pos_order,pos_order_item,product';
    $select = '(SUM(pos_order_item.pos_order_item_amount * (pos_order_item.pos_order_item_quantity))) AS number';
    // $cpm_group = 'pos_order_item.pos_order_id';
    $total_selling_price = $this->dashboard_model->count_items_group($table, $where, $select);



    $profit = $total_selling_price - $total_buying_price;

    if(empty($profit) or empty($total_buying_price))
    {
        $profit_percentage = 0;
    }
    else
    {
        $profit_percentage = ($profit/$total_buying_price)*100;
    }





    ?>
        <div class="col-md-6 ">
            <div class="row">
                <div class="col-md-6">
                    <section class="panel panel-featured-left panel-featured-primary">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fa fa-shopping-cart"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"><?php echo date('jS M Y');?> Retail Sales</h4>
                                        <div class="info">
                                            <strong class="amount"><?php echo $retail_orders?> Orders</strong>
                                            <span class="text-primary">(<?php echo $total_retail_order_items?> items sold)</span>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                        <a class="text-muted text-uppercase" href="<?php echo site_url().'administrative-reports/sales-report'?>">(view all)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-6">
                    <section class="panel panel-featured-left panel-featured-secondary">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-secondary">
                                        <i class="fa fa-usd"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title">Today's <?php echo date('jS M Y');?> Collections</h4>
                                        <div class="info">
                                            <strong class="amount">Kes <?php echo number_format($total_money_received,2);?></strong>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                        <a class="text-muted text-uppercase" href="<?php echo site_url().'administrative-reports/revenue-collection-report'?>">(view)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-6">
                    <section class="panel panel-featured-left panel-featured-tertiary">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-tertiary">
                                        <i class="fa fa-shopping-cart"></i>
                                    </div>
                                </div>

                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title">Wholesale / Credit Sales</h4>
                                        <div class="info">
                                            <strong class="amount"><?php echo $wholesale_orders?> Orders</strong>
                                            <span class="text-primary">(<?php echo $total_wholesale_order_items?> items sold)</span>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                        <a class="text-muted text-uppercase" href="<?php echo site_url().'administrative-reports/sales-report'?>">(view all)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-6">
                    <section class="panel panel-featured-left panel-featured-quartenary">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-quartenary">
                                        <i class="fa fa-usd"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"><?php echo date('jS M Y');?> Profit</h4>
                                        <div class="info">
                                            <strong class="amount">Kes. <?php echo number_format($profit,2)?> - <?php echo number_format($profit_percentage,2);?>%</strong>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                        <a class="text-muted text-uppercase" href="<?php echo site_url().'administrative-reports/sales-report'?>">(report)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <?php
    $search_title = '';
    $dashboard_date_from = $this->session->userdata('dashboard_date_from'); 
    $dashboard_date_to = $this->session->userdata('dashboard_date_to'); 
    if(!empty($dashboard_date_from) && !empty($dashboard_date_to))
    {
        $visit_date = ' AND pos_payments.payment_date BETWEEN \''.$dashboard_date_from.'\' AND \''.$dashboard_date_to.'\'';
        $petty_cash_date = ' AND petty_cash.petty_cash_date BETWEEN \''.$dashboard_date_from.'\' AND \''.$dashboard_date_to.'\'';

        $search_title .= 'Payments from '.date('jS M Y', strtotime($dashboard_date_from)).' to '.date('jS M Y', strtotime($dashboard_date_to)).' ';
    }

    else if(!empty($dashboard_date_from))
    {
        $visit_date = ' AND pos_payments.payment_date = \''.$dashboard_date_from.'\'';
        $petty_cash_date = ' AND petty_cash.petty_cash_date = \''.$dashboard_date_from.'\'';
        $search_title .= 'Payments of '.date('jS M Y', strtotime($dashboard_date_from)).' ';
    }

    else if(empty($dashboard_date_from) AND !empty($dashboard_date_to))
    {
        $visit_date = ' AND pos_payments.payment_date = \''.$dashboard_date_to.'\'';
        $petty_cash_date = ' AND petty_cash.petty_cash_date = \''.$dashboard_date_to.'\'';
        $search_title .= 'Payments of '.date('jS M Y', strtotime($dashboard_date_to)).' ';
    }

    else
    {
        $search_title .= date('jS M Y', strtotime(date('Y-m-d'))).' ';
        $dashboard_date_from = date('Y-m-d');
        $dashboard_date_to = date('Y-m-d');
    }

    ?>
    <div class="row">

        <?php
        echo form_open("admin/search_dashboard_report", array("class" => "form-horizontal"));
        ?>
        <div class="col-md-8 ">
            <div class="col-md-5" >
        
                 <div class="form-group">
                    <label class="col-md-4 left-align">Date From: </label>
                    
                    <div class="col-md-8">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="dashboard_date_from" placeholder="Date From" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-5" >
                <div class="form-group">
                    <label class="col-md-4 left-align">Date To: </label>
                    
                    <div class="col-md-8">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="dashboard_date_to" placeholder="Date To" autocomplete="off">
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="col-md-2" >
                 <div class="form-group">
                    <div class="center-align">
                        <button type="submit" class="btn btn-info">Search</button>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-4 ">
            <h5>Reporting for <?php echo $search_title;?></h5>
        </div>
        <?php
        echo form_close();
        ?>
    </div>
    <div class="row">
        <div class="col-md-3">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <!-- <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
                    </div>

                    <h2 class="panel-title">Transactions Summaries</h2>
                    <p class="panel-subtitle"></p>
                </header>
                <div class="panel-body" style="height: 45vh;overflow-y:scroll;">

                    <table class="table table-hover table-condensed table-bordered">
                        <thead>
                            <th>#</th>
                            <th>Payment Method</th>
                            <th>Amount Collected</th>
                        </thead>
                        <tbody>
                             <?php
                                $total_cash_breakdown = 0;
                                $payment_methods = $this->reports_model->get_payment_methods();
                                if($payment_methods->num_rows() > 0)
                                {
                                    $x = 0;
                                    $total_amount = 0;
                                    foreach($payment_methods->result() as $res)
                                    {
                                        $method_name = $res->payment_method;
                                        $payment_method_id = $res->payment_method_id;
                                       
                                        $amount_paid = $this->reports_model->get_payments_amount($payment_method_id);
                                        
                                        $x++;
                                        $total_amount += $amount_paid;
                                        echo 
                                        '
                                        <tr>
                                            <td>'.$x.'</td>
                                            <th>'.strtoupper($method_name).'</th>
                                            <td><a href="'.site_url().'search-payments/'.$payment_method_id.'/'.$dashboard_date_from.'/'.$dashboard_date_to.'">'.number_format($amount_paid, 2).'</a></td>
                                        </tr>
                                        ';
                                    }
                                    
                                    echo 
                                    '
                                    <tr>
                                        <th></th>
                                        <th>TOTAL</th>
                                        <th><a href="'.site_url().'search-payments/0/'.$dashboard_date_from.'/'.$dashboard_date_to.'">'.number_format($total_amount, 2).'</a></th>
                                    </tr>
                                    ';
                                }
                                ?>
                        </tbody>
                        
                    </table>
                </div>
            </section>
        </div>
        <div class="col-md-5">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <!-- <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
                    </div>

                    <h2 class="panel-title">Personnel Sales Summaries</h2>
                    <p class="panel-subtitle"></p>
                </header>
                <div class="panel-body" style="height: 45vh;overflow-y:scroll;">

                    <table class="table table-hover table-condensed table-bordered">
                        <thead>
                            <th>#</th>
                            <th>Personnel Name</th>
                            <th>Retail Revenue</th>
                            <th>Credit Sales</th>
                        </thead>
                        <tbody>
                            <?php
                                $staff_sales = $this->reports_model->get_personnel_by_type();
                                $total_collection = 0;
                                $total_retail_revenue = 0;
                                $total_wholesale_revenue = 0;
                                $total_retail_buying_price = 0;
                                $total_wholesale_buying_price = 0;
                                 $total_credit_revenue = 0;

                                // var_dump($staff_sales);die()
                                if($staff_sales->num_rows() > 0)
                                {
                                    $counting= 0;
                                    foreach($staff_sales->result() as $res)
                                    {
                                        $personnel_fname = $res->personnel_fname;
                                        $personnel_onames = $res->personnel_onames;
                                        $personnel_id = $res->personnel_id;


                                        $retail_revenue = $this->reports_model->get_personnel_revenue($personnel_id,5,0);
                                        $credit_revenue = $this->reports_model->get_personnel_revenue($personnel_id,5,1);
                                        $wholesale_revenue = $this->reports_model->get_personnel_revenue($personnel_id,6);
                                        $retail_buying_price = $this->reports_model->get_personnel_buying_price($personnel_id,5,0);
                                        $credit_buying_price = $this->reports_model->get_personnel_buying_price($personnel_id,5,1);
                                        $wholesale_buying_price = $this->reports_model->get_personnel_buying_price($personnel_id,6);

                                        $buying_price = $retail_buying_price + $wholesale_buying_price + $credit_buying_price;


                                        $total_retail_buying_price += $retail_buying_price;
                                        $total_wholesale_buying_price += $wholesale_buying_price;


                                        $total_retail_revenue += $retail_revenue;
                                        $total_credit_revenue += $credit_revenue;
                                        $total_wholesale_revenue += $wholesale_revenue;
                                        $profit = ($retail_revenue + $wholesale_revenue + $credit_revenue) - $buying_price;
                                        $total_collection +=$profit;

                                        // var_dump($retail_revenue);die();
                                        if(empty($profit) or empty($buying_price))
                                        {
                                            $profit_percentage_made = 0;
                                        }
                                        else
                                        {
                                            $profit_percentage_made = ($profit/$buying_price)*100;
                                        }
                                        $counting++;
                                        echo 
                                            '
                                            <tr>
                                                <th>'.$counting.'</th>
                                                <th>'.$personnel_fname.' '.$personnel_onames.'</th>
                                                <td>'.number_format($retail_revenue, 2).'</td>
                                                <td>'.number_format($credit_revenue, 2).'</a></td>
                                            </tr>
                                            ';
                                    }
                                    
                                    echo 
                                    '
                                    <tr>
                                        <th></th>
                                        <th>Total</th>
                                        <td>'.number_format($total_retail_revenue, 2).'</td>
                                        <td>'.number_format($total_credit_revenue, 2).'</td>
                                    </tr>
                                    ';
                                }


                                $retail_profit = $total_retail_revenue - $total_retail_buying_price;

                                $wholesale_profit = $total_wholesale_revenue - $total_wholesale_buying_price;
                                ?>
                        </tbody>
                        
                    </table>
                </div>
            </section>
        </div>
       
    </div>

    <?php

}
?>
<script type="text/javascript">
    

function view_drug_trail(product_id,store_id)
{
    document.getElementById("sidebar-right").style.display = "block"; 
    document.getElementById("existing-sidebar-div").style.display = "none"; 

    var config_url = $('#config_url').val();
    var data_url = config_url+"pos/drug_trail/"+product_id+"/"+store_id;
    //window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{appointment_id: 1},
    dataType: 'text',
    success:function(data){
        
            document.getElementById("current-sidebar-div").style.display = "block"; 
            $("#current-sidebar-div").html(data);
            $("#visit-invoice-div").html(data);
        
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
    }

    });
}

function close_side_bar()
{
    // $('html').removeClass('sidebar-right-opened');
    document.getElementById("sidebar-right").style.display = "none"; 
    document.getElementById("current-sidebar-div").style.display = "none"; 
    document.getElementById("existing-sidebar-div").style.display = "none"; 
    tinymce.remove();
}

</script>

