<div class="col-md-12">
<div class="col-print-4">
        <a onclick="<?php echo site_url().'logout-admin';?>"  class="btn btn-sm btn-danger">
        <i class="fa fa-lock" ></i>
       
        LOGOUT
    </a>
    </div>
</div>

<?php
        $personnel_id = $this->session->userdata('personnel_id');
        
        if($personnel_id == 0)
        {
                $parents = $this->sections_model->all_parent_sections('section_position');
        }
        
        else
        {
                $personnel_roles = $this->personnel_model->get_personnel_roles($personnel_id);
                
                $parents = $personnel_roles;
        }
        $children = $this->sections_model->all_child_sections();

        $page = explode("/",uri_string());
        $total = count($page);
        $section_title = ucfirst($page[0]);

        
        $sections = '';
        $total_parents = $parents->result();
        // $section_title = $this->
        if($parents->num_rows() > 0)
        {
                $counter = 0;
                foreach($parents->result() as $res)
                {
                        $section_parent = $res->section_parent;
                        $section_id = $res->section_id;
                        $section_name = $res->section_name;
                        $section_icon = $res->section_icon;
                        

                        

                        if($counter % 4 == 0) {
                                if($counter == 0)
                                        $sections .= '<div class="col-md-12">';    
                                else
                                       $sections .= '</div><div class="col-md-12">';    
                        }
                        if($section_parent == 0)
                        {
                                $web_name = strtolower($this->site_model->create_web_name($section_name));
                                $link = site_url().$web_name;
                                $section_children = $this->admin_model->check_children($children, $section_id, $web_name);
                                $total_children = count($section_children);
                                
                                if($total_children == 0)
                                {
                                                // var_dump($section_name); die();
                                        if($section_title == $section_name)
                                        {
                                                // $sections .= '<li class="active">';
                                        }
                                        
                                        else
                                        {
                                                // $sections .= '<li>';
                                        }
                                        // $sections .= '
                                        //         <a href="'.$link.'">
                                        //                 <i class="fa fa-'.$section_icon.'" aria-hidden="true"></i>
                                        //                 <span>'.$section_name.'</span>
                                        //         </a>
                                        // </li>
                                        // ';
                                        $sections .= ' <div class="col-md-3">
                                                                <div class="col-print-12">
                                                                       <a  class="btn btn-lg btn-primary col-print-12" href="'.$link.'" id="link-item">   <i class="fa fa-'.$section_icon.'" aria-hidden="true"></i> 
                                                                        <br><span style="font-size:15px !important;">'.$section_name.'</span>
                                                                       </a>
                                                                </div>   
                                                                     
                                                        </div>';
                                }
                                
                                else
                                {
                                        if($section_title == $section_name)
                                        {
                                                // $sections .= '<li class="active dropdown">';
                                        }
                                        
                                        else
                                        {
                                                // $sections .= '<li class="dropdown">';
                                        }

                                         $sections .= ' <div class="col-md-3">
                                                                <div class="col-print-5">
                                                                       <a  class="btn btn-lg btn-primary col-print-12" href="'.$link.'" id="link-item">   <i class="fa fa-'.$section_icon.'" aria-hidden="true"></i> </a>
                                                                </div>   
                                                               ';
                                        // $sections .= '
                                        //         <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-'.$section_icon.'" aria-hidden="true"></i> '.$section_name.' <b class="caret"></b></a> 
                                                
                                        //         <ul class="dropdown-menu">';
                                        $sections .='<div class="col-print-7">
                                                        <div style="padding: 5px;">
                                                        <h6>'.$section_name.'</h6>
                                                        <ul style="list-style: none;">';
                                        //children
                                        for($r = 0; $r < $total_children; $r++)
                                        {
                                                $name = $section_children[$r]['section_name'];
                                                $link = $section_children[$r]['link'];
                                                
                                                $sections .= '
                                                        <li>
                                                                <a href="'.$link.'">
                                                                         '.$name.'
                                                                </a>
                                                        </li>
                                                ';
                                        }
                                        
                                        $sections .= '
                                                        </div>
                                                        </ul>
                                                </div>        
                                        </div>
                                        ';
                                }
                        }
                        
                        else
                        {
                                //get parent section
                                $parent_query = $this->sections_model->get_section($section_parent);
                                
                                $parent_row = $parent_query->row();
                                $parent_name = $parent_row->section_name;
                                $section_icon = $parent_row->section_icon;
                                
                                $web_name = strtolower($this->site_model->create_web_name($parent_name));
                                $link = site_url().$web_name.'/'.strtolower($this->site_model->create_web_name($section_name));
                                
                                // $sections .= '
                                // <li>
                                //         <a href="'.$link.'">
                                //                 <i class="fa fa-'.$section_icon.'" aria-hidden="true"></i>
                                //                 <span>'.$section_name.'</span>
                                //         </a>
                                // </li>
                                // ';

                                 $sections .= ' <div class="col-md-3">

                                                                <div class="col-print-12">
                                                                       <a  class="btn btn-lg btn-primary col-print-12" href="'.$link.'" id="link-item">   <i class="fa fa-'.$section_icon.'" aria-hidden="true"></i>
                                                                                <br><span style="font-size:15px !important;">'.$section_name.'</span>
                                                                        </a>
                                                                </div>       
                                                        </div>';
                        }



                        $counter++;
                        if($total_parents == $counter)
                        {
                                // check if the number is divisible by 4
                                if($counter % 4 == 0) {

                                }
                                else
                                {
                                        $sections .= '</div>';
                                }


                        }

                }
        }


        
?>      
<div class="col-md-12">
       
        
        <?php echo $sections;?>

        
</div>