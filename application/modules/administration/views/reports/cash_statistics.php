<div class="row statistics">
    <div class="col-md-2 col-sm-12">
         <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Visits</h2>
              </header>             
        
              <!-- Widget content -->
              <div class="panel-body">
                <h4>Visit Breakdown</h4>
                <table class="table table-striped table-hover table-condensed">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Visits</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>Sales Visit</th>
                            <td><?php echo $outpatients;?></td>
                        </tr>
                    </tbody>
                </table>
                <!-- Text -->
                <h5>Total Sales Count</h5>
                <h4><?php echo $total_patients;?></h4>
                
                <div class="clearfix"></div>
            </div>
        </section>
    </div>
    
    <div class="col-md-10 col-sm-12">
         <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Transaction breakdown <?php echo $title;?></h2>
            </header>             
        
              <!-- Widget content -->
              <div class="panel-body">
                
                <div class="row">
                    <!-- End Transaction Breakdown -->
                    
                    <div class="col-md-3">
                        <h4>Cash Breakdown</h4>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                <?php
                                $total_cash_breakdown = 0;
                              
                                if($payment_methods->num_rows() > 0)
                                {
                                    foreach($payment_methods->result() as $res)
                                    {
                                        $method_name = $res->payment_method;
                                        $payment_method_id = $res->payment_method_id;
                                        $total = 0;
                                        
                                        if($normal_payments->num_rows() > 0)
                                        {
                                            foreach($normal_payments->result() as $res2)
                                            {
                                                $payment_method_id2 = $res2->payment_method_id;
                                               
                                            
                                                if($payment_method_id == $payment_method_id2)
                                                {
                                                   
                                                    $total += $res2->amount_paid;
                                                }
                                            }
                                        }
                                        
                                        $total_cash_breakdown += $total;
                                    
                                        echo 
                                        '
                                        <tr>
                                            <th>'.$method_name.'</th>
                                            <td>'.number_format($total, 2).'</td>
                                        </tr>
                                        ';
                                    }
                                    
                                    echo 
                                    '
                                    <tr>
                                        <th>Total</th>
                                        <td>'.number_format($total_cash_breakdown, 2).'</td>
                                    </tr>
                                    ';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-5">
                       <h4>Staff Sales</h4>
                        <table class="table table-striped table-hover table-condensed">
                            <thead>
                                <th>Name</th>
                                <th>Revenue</th>
                                <th>Collection</th>
                            </thead>
                            <tbody>
                                <?php
                                $staff_sales = $this->reports_model->get_personnel_by_type('Pharmacist');
                                $total_sales = 0;
                                $total_collection = 0;

                                // var_dump($staff_sales);die()
                                if($staff_sales->num_rows() > 0)
                                {
                                    foreach($staff_sales->result() as $res)
                                    {
                                        $personnel_fname = $res->personnel_fname;
                                        $personnel_onames = $res->personnel_onames;
                                        $personnel_id = $res->personnel_id;
                                        $revenue = $this->reports_model->get_personnel_revenue($personnel_id);
                                        $collection = $this->reports_model->get_personnel_collection($personnel_id);
                                        $total_collection =$collection;
                                        $total_sales += $revenue;
                                    
                                        echo 
                                        '
                                        <tr>
                                            <th>'.$personnel_fname.' '.$personnel_onames.'</th>
                                            <td>'.number_format($revenue, 2).'</td>
                                            <td>'.number_format($collection, 2).'</td>
                                        </tr>
                                        ';
                                    }
                                    
                                    echo 
                                    '
                                    <tr>
                                        <th>Total</th>
                                        <td>'.number_format($total_sales, 2).'</td>
                                        <td>'.number_format($total_collection, 2).'</td>
                                    </tr>
                                    ';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <h3>Cash / Petty Cash Transfer</h3> 
                        <h4>- Ksh <?php echo number_format($total_transfers, 2);?></h4>
                         <h3>Todays  Collection</h3> 
                        <h4>- Ksh <?php echo number_format($total_cash - $total_transfers, 2);?></h4>
                        <h3>Total Collection</h3>   
                        <h4>- Ksh <?php echo number_format($total_cash_breakdown-$total_transfers, 2);?></h4>
                    </div>
                    
                </div>
            </div>
        </section>
    </div>
</div>