<?php

$categories_result ='
						<table class="table table-bordered ">
							<thead>
								<th>#</th>
								<th>Name</th>
								<th>Code</th>
								<th>Category</th>
								<th>Price</th>
							</thead>
							<tbody>';
// var_dump($query->num_rows());die();

if(!empty($query))
{


	if($query->num_rows() > 0)
	{
		$x=0;
		foreach ($query->result() as $key => $value) {
			# code...

			$service_charge_name = $value->service_charge_name;
			$service_charge_amount = $value->service_charge_amount;
			$service_charge_id = $value->service_charge_id;
			$product_code = $value->product_code;
			$part_no = $value->part_no;
			$vehicle_name = $value->vehicle_name;
			$vehicle_model = $value->vehicle_model;
			$product_id = $value->product_id;
			$brand_name = $value->brand_name;
			$category_name = $value->category_name;
			$service_name = $value->service_name;


			
			$x++;
			$categories_result .= '<tr>
										<td>'.$x.'</td>
					        			<td>'.strtoupper(strtolower($service_charge_name)).'</td>
					        			<td>'.$product_code.' </td>
					        			<td>'.strtoupper(strtolower($service_name)).'</td>
					        			<td>'.number_format($service_charge_amount,2).'</td>
					        		</tr>';

			
		}
	}
	else
	{
		$categories_result .= '<tr >
									<td colspan="7">Could not be able to find that product</td>
					        	</tr>';
	}
}
else
{
	$categories_result .= '<tr >
									<td colspan="7">Search a product to sale</td>
					        	</tr>';
}
$categories_result .='</tbody>
				</table>';
echo $categories_result;
?>