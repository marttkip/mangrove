<?php 
	
	$contacts = $this->site_model->get_contacts();
	
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$email2 = $contacts['email'];
		$logo = $contacts['logo'];
		$company_name = $contacts['company_name'];
		$phone = $contacts['phone'];
		
		if(!empty($facebook))
		{
			$facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank" title="Facebook">Facebook</a></li>';
		}
		
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
	}
?>
<!doctype html>
<html class="fixed">
	<head>
        <?php echo $this->load->view('admin/includes/header', $contacts, TRUE); ?>
    </head>

	<body>
    	<!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->
    	<section class="" style="margin-top:1% !important;">
    		<div class="col-print-6">
    			<div class="col-md-12">
            		<input type="text" class="form-control" name="product_name" id="product_name" onkeyup="search_products()" placeholder="" autocomplete="off"  style="padding: 15px 5px !important;font-size: 12px marigin-top: !important;text-align: center;"  onfocus="this.removeAttribute('readonly');">
            	</div>
            	<div class="col-md-12 panel-body" style="height: 80vh !important;overflow-y: scroll;margin-top: 5px;">
            		<div id="product-list-item"></div>
            	</div>

    		</div>
            <div class="col-print-6">

				 <input type="hidden" id="base_url" value="<?php echo site_url();?>">
        		<input type="hidden" id="config_url" value="<?php echo site_url();?>">
				<?php
					$login_error = $this->session->userdata('login_error');
					$this->session->unset_userdata('login_error');
					
					if(!empty($login_error))
					{
						echo '<div class="alert alert-danger">'.$login_error.'</div>';
					}
				?>
				<form action="<?php echo site_url().$this->uri->uri_string();?>" method="post" id="sign-in" autocomplete="off">
                	<div class="col-md-12" style="margin-bottom: 10px;">
                		<input type="password" class="form-control" name="usernumber" id="usernumber" readonly="readonly" placeholder="" autocomplete="off"  style="padding: 30px 5px !important;font-size: 36px !important;text-align: center;"  onfocus="this.removeAttribute('readonly');">
                	</div>
					<div class="col-md-12">
						<div class="col-print-4">
		        			<a  class="btn btn-lg btn-primary col-md-12" id="link-item" onclick="enter_number(1)"> 1 </a>
		        	
						</div>
						<div class="col-print-4">
		        			<a  class="btn btn-lg btn-primary  col-md-12" id="link-item" onclick="enter_number(2)"> 2 </a>
		        	
						</div>
						<div class="col-print-4">
		        			<a  class="btn btn-lg btn-primary col-md-12" id="link-item" onclick="enter_number(3)"> 3 </a>
		        	
						</div>
						<div class="col-print-4">
		        			<a  class="btn btn-lg btn-primary  col-md-12" id="link-item" onclick="enter_number(4)"> 4 </a>
		        	
						</div>
						<div class="col-print-4">
		        			<a  class="btn btn-lg btn-primary  col-md-12" id="link-item" onclick="enter_number(5)"> 5 </a>
		        	
						</div>
						<div class="col-print-4">
		        			<a  class="btn btn-lg btn-primary col-md-12" id="link-item" onclick="enter_number(6)"> 6 </a>
		        	
						</div>
						<div class="col-print-4">
		        			<a  class="btn btn-lg btn-primary col-md-12" id="link-item" onclick="enter_number(7)"> 7 </a>
		        	
						</div>
						<div class="col-print-4">
		        			<a  class="btn btn-lg btn-primary col-md-12" id="link-item" onclick="enter_number(8)"> 8 </a>
		        	
						</div>
						<div class="col-print-4">
		        			<a  class="btn btn-lg btn-primary col-md-12" id="link-item" onclick="enter_number(9)"> 9 </a>
		        	
						</div>
						<div class="col-print-4">
		        			
		        			<a  class="btn btn-lg btn-danger col-md-12" id="link-item" onclick="clear_data()"> <i class="fa fa-trash"></i> </a>
						</div>
						<div class="col-print-4">
		        			<a  class="btn btn-lg btn-primary col-md-12" id="link-item" onclick="enter_number(0)"> 0 </a>
		        	
						</div>
						<div class="col-print-4">
		        			<button type="submit"  class="btn btn-lg btn-success col-md-12" id="link-item"> <i class="fa fa-arrow-right"></i> </button>
		        	
						</div>
					</div>
				</form>

				<br>
				<hr>

				<!-- <div class="row" style="margin-top: 50px;"> -->
					<div class="col-print-12">
						<a class="btn btn-sm btn-warning" style="width: 100%;" href="<?php echo site_url().'login-admin'?>"> Admin Login</a>
					</div>
					
				<!-- </div> -->
				
			</div>
		</section>
		<!-- end: page -->
		<script type="text/javascript">
			
			function search_products()
			{
					var config_url = $('#config_url').val();

					var product_name = $('#product_name').val();

					var data_url = config_url+"auth/get_product_list_items_inquiry";
					// window.alert(data_url);
	
					$.ajax({
					type:'POST',
					url: data_url,
					data:{description: product_name},
					dataType: 'text',
				   success:function(data){
						// alert(data);
						//window.alert("You have successfully updated the symptoms");
						//obj.innerHTML = XMLHttpRequestObject.responseText;
						$("#product-list-item").html(data);
						// alert(data);
					},
					error: function(xhr, status, error) {
					//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
					alert(error);
					}

					});
			}
		</script>
        		
		<!-- Vendor -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/theme.init.js"></script>

		<script type="text/javascript">
			function enter_number(number)
			{


				var usernumber = document.getElementById("usernumber").value;

				document.getElementById("usernumber").value = usernumber+number;

			}
			function clear_data()
			{
				document.getElementById("usernumber").value = '';
			}

			$(document).on("submit","form#sign-in",function(e)
			{
				e.preventDefault();
				
				var form_data = new FormData(this);


				var config_url = $('#config_url').val();	
				// alert(config_url);
				var data_url = config_url+"auth/userlogin";
					 
					 
			   $.ajax({
				   type:'POST',
				   url: data_url,
				   data:form_data,
				   dataType: 'text',
				   processData: false,
				   contentType: false,
				   success:function(data){
				      var data = jQuery.parseJSON(data);
				    	
				      	if(data.status == "success")
						{
							window.location.href = config_url+'sales';

							
						}
						else
						{
							alert('Sorry user cannot be found');
						}
				   
				   },
				   error: function(xhr, status, error) {

				   		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				   
				   }
				   });
				
				
				 
			

				
			});
		</script>
	</body>
</html>
