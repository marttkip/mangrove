<?php

//if users exist display them
$result ='';
if ($query->num_rows() > 0)
{
    
    foreach ($query->result() as $row)
    {//var_dump($query);die();

        $product_id = $row->product_id;
        $product_name = $row->product_name;
        $vehicle_model = $row->vehicle_model;
        $vehicle_name = $row->vehicle_name;
        $product_code = $row->product_code;
        $code_num = $row->code_num;
        $product_status = $row->product_status;
        $product_description = $row->product_description;
        $vehicle_origin = $row->vehicle_origin;

        $category_id = $row->category_id;
        $created = $row->created;
        $created_by = $row->created_by;
        $last_modified = $row->last_modified;
        $modified_by = $row->modified_by;
        $category_name = $row->category_name;
        $store_id = $row->store_id;
        $product_unitprice = $row->product_unitprice;
        $reorder_level = $row->reorder_level;
        $vatable = $row->vatable;
        $stock_take = $row->stock_take;
        $part_no = $row->part_no;
        $valley_number = $row->valley_number;
        $product_code = $row->product_code;
        $total_quantity = $row->total_quantity;
        // var_dump($total_quantity);die();

        for ($i=0; $i < $total_quantity; $i++) { 
            # code...

            $result .= '<div class="col-print-2" >
                            <b>'.$product_code.'<br></b>'.$vehicle_name.'
                            '.$part_no.''.$vehicle_origin.'
                            <br>'.$code_num.' '.$vehicle_model.'<br>
                        </div>';
        }

        
    }
}

// var_dump($query->num_rows());die();
?>

<!DOCTYPE html>
<html lang="en">

    <head>
          <title> Valley Label</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
            body
            {
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
                font-size:9px;                
            }
            .receipt_spacing{letter-spacing:0px; font-size: 10px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            
            .col-md-6 {
                width: 60%;
             }
        
            h3
            {
                font-size: 30px;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 10px;}
            .title-img{float:left; padding-left:10px;}
            img.logo{ margin:0 auto;}
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {  
                            width:20%; float:right; 
                            /*background-color: green; */
                            color:black;
                            height: 73px !important;
                            padding: 20px;
                            word-spacing: 3px;
                            margin-top: 5px;
                            /*margin: 1px;*/
                            border-radius: 5px;
                        }
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}

            table.table{
                border-spacing: 0 !important;
                font-size:9px;
                margin-top:8px;
                border-collapse: inherit !important;
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
            }
            /*th, td {
                border: 2px solid #000 !important;
                padding: 0.5em 1em !important;
            }
            thead tr:first-child th:first-child {
                border-radius: 20px 0 0 0 !important;
            }
            thead tr:first-child th:last-child {
                border-radius: 0 20px 0 0 !important;
            }
            tbody tr:last-child td:first-child {
                border-radius: 0 0 0 20px !important;
            }
            tbody tr:last-child td:last-child {
                border-radius: 0 0 20px 20px !important;
            }
            tbody tr:first-child td:first-child {
                border-radius: 20px 20px 0 0 !important;
                border-bottom: 0px solid #000 !important;
            }*/
            .padd
            {
                padding:50px;
            }
            
        </style>
    </head>
    <body class="receipt_spacing">
        <!-- <div class="padd"> -->
            <div class="col-print-12">
                <?php echo $result;?>
            </div>
        <!-- </div> -->
       
    </body>
    
</html>