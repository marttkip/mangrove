<?php
$v_data['all_categories'] = $all_categories;
$v_data['all_brands'] = $all_brands;
$v_data['all_generics'] = $all_generics;

$parent_store = 0;


echo $this->load->view('products/search_products_stock', $v_data, TRUE);

	//get personnel approval rightts
	$personnel_id = $this->session->userdata('personnel_id');
	$approval_id = 2;//$this->inventory_management_model->get_approval_id($personnel_id);
// var_dump($approval_id); die();
?>
<div class="row">
    <div class="col-md-12">
		<section class="panel panel-featured panel-featured-info">
		    <header class="panel-heading">
		        <h2 class="panel-title pull-left"><?php echo $title;?></h2>
		         <div class="widget-icons pull-right">
                 	
                     <a href="<?php echo base_url();?>inventory/download-all-stock" target="_blank" class="btn btn-default btn-sm fa fa-print"> Download All</a>

                   

		          </div>
		          <div class="clearfix"></div>
		    </header>
		    <div class="panel-body">
				<?php

						$error = $this->session->userdata('error_message');
						$success = $this->session->userdata('success_message');
						$search_result ='';
						$search_result2  ='';
						if(!empty($error))
						{
							$search_result2 = '<div class="alert alert-danger">'.$error.'</div>';
							$this->session->unset_userdata('error_message');
						}

						if(!empty($success))
						{
							$search_result2 ='<div class="alert alert-success">'.$success.'</div>';
							$this->session->unset_userdata('success_message');
						}

						$search = $this->session->userdata('product_stock_search');

						if(!empty($search))
						{
							$search_result = '<a href="'.site_url().'inventory_management/close_stock_list_search" class="btn btn-success btn-sm">Close Search</a>';
						}

						$inventory_search_start_date = $this->session->userdata('inventory_search_start_date');
						if(!empty($inventory_search_start_date))
						{
							$search_start_date = $inventory_search_start_date;
							if($search_start_date == '')
							{
								$search_start_date = NULL;
							}
						}
						else
						{
							$search_start_date = NULL;
						}

						$inventory_search_end_date = $this->session->userdata('inventory_search_end_date');
						if(!empty($inventory_search_start_date))
						{
							$search_end_date = $inventory_search_end_date;
							if($search_end_date == '')
							{
								$search_end_date = NULL;
							}
						}
						else
						{
							$search_end_date = NULL;
						}


						$result = '';
						$result .= ''.$search_result2.'';
						$result .= '
								';

						//if users exist display them
						if ($query->num_rows() > 0)
						{
							$count = $page;

							
							$result .=
							'
							<div class="row">
							<div class="col-md-12 table-responsive">
								<table class="table table-bordered">

								  <thead>
		                                <th>#</th>
		                                <th>Code</th>
		                                <th>Name</th>
		                                <th>Category</th>
		                                <th>Vehicle Name</th>
		                                <th>Pat No</th>
		                                <th>Brand</th>
		                                <th>Vatable</th>
		                                <th>Store</th>
		                                <th>Shop</th>
		                                <th>Total Stock</th>
		                                <th>Price</th>
		                                <th>Status</th>
		                            </thead>
								  <tbody>
							';

							//get all administrators
							// $personnel_query = $this->personnel_model->get_all_personnel();

							foreach ($query->result() as $row)
							{//var_dump($query);die();

								$product_id = $row->product_id;
								$product_name = $row->product_name;
								$product_code = $row->product_code;
								$product_status = $row->product_status;
								$product_description = $row->product_description;
								$vehicle_name = $row->vehicle_name;
								$category_id = $row->category_id;
								$created = $row->created;
								$created_by = $row->created_by;
								$last_modified = $row->last_modified;
								$modified_by = $row->modified_by;
								$category_name = $row->category_name;
								$reorder_level = $row->reorder_level;
								$vatable = $row->vatable;
								$regenerate_id = $row->regenerate_id;
								$brand_name = $row->brand_name;
								$stock_take = $row->stock_take;
								$part_no = $row->part_no;
								$product_unitprice = $row->product_unitprice;
								
								$product_unitprice = $row->product_unitprice;
		                        $product_deleted = $row->product_deleted;
		                        // $in_service_charge_status = $row->in_service_charge_status;
		                        // var_dump($owning_store_id);

								//status
								if($product_status == 1)
								{
									$status = 'Active';
								}
								else
								{
									$status = 'Disabled';
								}


								if($vatable == 1)
								{
									$vatable_status = 'Yes';
								}
								else
								{
									$vatable_status = 'No';
								}

								if($stock_take == 0 OR empty($stock_take))
								{
									$regenerate = 'info';
									$closed = 'readonly';
								}
								else
								{
									$regenerate = 'primary';
									$closed = '';
								}



								$button = '';
								//create deactivated status display
								if($product_status == 0)
								{
									$status = '<span class="label label-danger">Deactivated</span>';
									
								}
								//create activated status display
								else if($product_status == 1)
								{
									$status = '<span class="label label-success">Active</span>';
									
								}




                                $inventory_start_date = $this->inventory_management_model->get_inventory_start_date();
	
								// this is only for the young store
								// pharmacy store
								$store_id = 6;
				                $child_store_stock = $this->inventory_management_model->child_store_stock($inventory_start_date, $product_id,$store_id);


				                $store_id = 5;
                				$parent_store_stock = $this->inventory_management_model->parent_stock_store($inventory_start_date, $product_id,$store_id);

                                   
                                  
								$total_stock = $child_store_stock+$parent_store_stock;
								$color = 'default';
                				if($total_stock < $reorder_level )
                				{
                					$color = 'danger';
                				}
								// $child_store_stock = $this->inventory_management_model->child_store_stock($inventory_start_date, $product_id,6);
								$count++;
								
								$result .=
								'
									<tr >
										<td class="'.$color.'">'.$count.'</td>
										<td class="'.$regenerate.'">'.$product_code.'</td>
										<td class="'.$color.'">'.$product_name.'</td>
										<td class="'.$color.'">'.$category_name.'</td>
										<td class="'.$color.'">'.$vehicle_name.'</td>
										<td class="'.$color.'">'.$part_no.'</td>
										<td>'.$brand_name.'</td>
										<td>'.$vatable_status.'</td>
		                                <td >'.$parent_store_stock.'</td>
		                                <td >'.$child_store_stock.'</td>
		                                <td>'.$total_stock.'</td>
		                                 <td >'.number_format($product_unitprice,2).'</td>
		                                <td>'.$status.'</td>
									</tr>
								';

							}

							$result .=
							'
										  </tbody>
										</table>
										</div>
									</div>
							';
						}

						else
						{
							$result .= '';
						}

						$result .= '</div>';
						echo $result;
				?>
				<div class="widget-foot">
			    <?php
			    if(isset($links)){echo $links;}
			    ?>
			    </div>
			</div>

		</section>
	</div>
</div>
