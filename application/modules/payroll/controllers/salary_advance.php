<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/accounts/controllers/accounts.php";

class Salary_advance extends accounts 
{
	var $csv_path;
	
	function __construct()
	{
		parent:: __construct();
		
		$this->load->library('image_lib');
		$this->load->model('salary_advance_model');
		$this->csv_path = realpath(APPPATH . '../assets/csv');
	}
    
	/*
	*
	*	Default action is to show all the salary advances
	*
	*/
	public function index($order = 'salary_advance.branch_id', $order_method = 'ASC') 
	{
		$branch_id = $this->session->userdata('branch_id2');
		$branch_name = $this->session->userdata('branch_name2');
		$where = 'personnel.personnel_type_id = personnel_type.personnel_type_id AND salary_advance.branch_id = branch.branch_id AND salary_advance.personnel_id = personnel.personnel_id';
		
		if(($branch_id == FALSE) || (empty($branch_id)))
		{
			
		}
		
		else
		{
			$where .= ' AND salary_advance.branch_id = '.$branch_id;
		}
		
		//search advances
		$search = $this->session->userdata('search_advances');
		$title = $this->session->userdata('advances_search_title');
	
		if(!empty($title))
		{
			$where .= $search;
			$title = $title;
		}
		else{
			$title = 'Salary Advance';
		}
		//var_dump($where); die();
		$table = 'salary_advance, personnel, personnel_type, branch';
		//pagination
		$segment = 4;
		$this->load->library('pagination');
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['base_url'] = site_url().'salary-advance/'.$order.'/'.$order_method;
		$config['uri_segment'] = $segment;
		$config['per_page'] = 250;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
	
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->salary_advance_model->get_all_advances($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = $title;
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['branches'] = $this->branches_model->all_branches();
		$v_data['months'] = $this->salary_advance_model->all_months();
		$v_data['page'] = $page;
		$v_data['salary_advance_query'] = $this->salary_advance_model->get_all_advances($table, $where, $config["per_page"], $page, $order, $order_method);
		$data['content'] = $this->load->view('salary_advance/all_advances', $v_data, true);
			//var_dump($v_data['salary_advance_query'] );die();
		$this->load->view('admin/templates/general_page', $data);
	}
	public function search_salary_advance()
	{
		$branch_id2 = $this->input->post('branch_id');
		$month = $this->input->post('month');
		$branch_name2 = $this->salary_advance_model->get_branch_name($branch_id2);
		$month_name2 = $this->salary_advance_model->get_month_name($month);
		
		//var_dump($branch_id2); die();
		if(!empty($month) && !empty($branch_id2))
		{
			$search = " AND salary_advance.branch_id= '".$branch_id2."' AND salary_advance.month_id = ".$month;
			$title = " Advances for ".$branch_name2."/".$month_name2;
			
			$this->session->set_userdata('branch_id2', $branch_id2);
			$this->session->set_userdata('branch_name2', $branch_name2);
			$this->session->set_userdata('search_advances', $search);
			$this->session->set_userdata('advances_search_title', $title);
		}
		
		redirect('salary-advance');
	}
	public function close_advance_search()
	{
		$this->session->unset_userdata('branch_id2');
		$this->session->unset_userdata('branch_name2');
		$this->session->unset_userdata('search_advances');
		$this->session->unset_userdata('advances_search_title', $title);

		redirect('salary-advance');
	}
	//import salary advance
	public function import_salary_advance()
	{
		$v_data['title'] = $data['title'] = $this->site_model->display_page_title();
		
		$data['content'] = $this->load->view('salary_advance/import_advance', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	//salary advance template
	public function advances_template()
	{
		$this->salary_advance_model->advances_template();	
	}
	//do the salary advance import
	function do_advance_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->salary_advance_model->import_csv_salary_advance($this->csv_path);
				
				if($response == FALSE)
				{
					$v_data['import_response_error'] = 'Something went wrong. Please try again.';
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		$v_data['title'] = $data['title'] = $this->site_model->display_page_title();
		
		$data['content'] = $this->load->view('salary_advance/import_advance', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function download_salary_advance($order = 'salary_advance.branch_id', $order_method = 'ASC')
	{
		$branch_id = $this->session->userdata('branch_id2');
		$branch_name = $this->session->userdata('branch_name2');
		$where = 'personnel.personnel_type_id = personnel_type.personnel_type_id AND salary_advance.branch_id = branch.branch_id AND salary_advance.personnel_id = personnel.personnel_id';
		
		//search advances
		$search = $this->session->userdata('search_advances');
		$title = $this->session->userdata('advances_search_title');
	
		if(!empty($title))
		{
			$where .= $search;
			$title = $title;
		}
		else{
			$title = 'Salary Advance';
		}

		$table = 'salary_advance, personnel, personnel_type, branch';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['base_url'] = site_url().'salary-advance';
		$config['uri_segment'] = $segment;
		$config['per_page'] = 250;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->salary_advance_model->get_all_advances($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['contacts'] = $this->site_model->get_contacts();

		$data['branches'] = $this->branches_model->all_branches();
		
		$data['order'] = $order;
		$data['order_method'] = $order_method;
		$data['query'] = $query;
		$data['page'] = $page;	
		$data['salary_advance_query'] = $this->salary_advance_model->get_all_advances($table, $where, null,null, $order, $order_method);
		// var_dump($data['salary_advance_query']); die();
		 $this->load->view('salary_advance/print_salary_advance', $data);
			
	}
}
?>