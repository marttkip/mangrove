<?php

		if($branches->num_rows() > 0)
		{
			$row = $branches->result();
			$branch_id = $row[0]->branch_id;
			$branch_name = $row[0]->branch_name;
			$branch_image_name = $row[0]->branch_image_name;
			$branch_address = $row[0]->branch_address;
			$branch_post_code = $row[0]->branch_post_code;
			$branch_city = $row[0]->branch_city;
			$branch_phone = $row[0]->branch_phone;
			$branch_email = $row[0]->branch_email;
			$branch_location = $row[0]->branch_location;

			$data['branch_name'] = $branch_name;
			$data['branch_image_name'] = $branch_image_name;
			$data['branch_id'] = $branch_id;
			$data['branch_address'] = $branch_address;
			$data['branch_post_code'] = $branch_post_code;
			$data['branch_city'] = $branch_city;
			$data['branch_phone'] = $branch_phone;
			$data['branch_email'] = $branch_email;
			$data['branch_location'] = $branch_location;
		}
		$result = '';
		
		//if advances exist exist display them
		if ($salary_advance_query->num_rows() > 0)
		{
			$count = $page; 
		
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed" id="customers">
				<thead>
					<tr>
						<th>#</th>
						<th>Payroll Number</th>
						<th>Personnel Name</th>
						<th>Account Number</th>
						<th>Salary Advance Amount</th>
					</tr>
				</thead>
				<tbody>
				  
			';
      $total_advance = 0;
			foreach($salary_advance_query->result() as $advance_details)
			{
				$payroll_number = $advance_details->personnel_number;
				$personnel_name = $advance_details->personnel_fname.' '.$advance_details->personnel_onames;
				$account_number = $advance_details->bank_account_number;
				$advance_amount = $advance_details->advance_amount;
				$bank_branch_id = $advance_details->bank_branch_id;
        $year = $advance_details->year;
        $month_id = $advance_details->month_id;
				if(!empty($bank_branch_id))
				{
					$bank_code = $this->salary_advance_model->get_branch_code($bank_branch_id);
				}
				else 
				{
					$bank_code = '';
				}
        $total_advance += $advance_amount;
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$payroll_number.'</td>
						<td>'.$personnel_name.'</td>
						<td>'.$account_number.'</td>
						<td>'.number_format($advance_amount,2).'</td>
					</tr> 
				';
			}
      $result .= 
        '
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>Total</td>
            <td>'.number_format($total_advance,2).'</td>
          </tr> 
        ';
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no advances made";
		}

    if($month_id < 10)
    {
      $month = '0'.$month_id;
    }
    $last_visit = $year.'-'.$month.'-20';
    $advance_date = date('F Y',strtotime($last_visit));
?>
	

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Payroll</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css">
		<script type="text/javascript" src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery/jquery.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>tableExport.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>jquery.base64.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>html2canvas.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>libs/sprintf.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>jspdf.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>libs/base64.js"></script>
		<style type="text/css">
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
            .center-align{margin:0 auto; text-align:center;}

            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            .row .col-md-12 table {
                border:solid #000 !important;
                border-width:1px 0 0 1px !important;
                font-size:10px;
            }
            .row .col-md-12 th, .row .col-md-12 td {
                border:solid #000 !important;
                border-width:0 1px 1px 0 !important;
            }

            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{max-height:70px; margin:0 auto;}
            .table {margin-bottom: 0;}
        </style>
    </head>
    <body class="receipt_spacing" onLoad="window.print();return false;">
    	<div class="col-md-12 center-align">
    		<table class="table table-condensed">
                <tr>
                    <th>Salary Advance for <?php echo $advance_date;?></th>
                    <th class="align-right">
						            <?php echo $branch_name;?><br/>
                        <?php echo $branch_address;?> <?php echo $branch_post_code;?> <?php echo $branch_city;?><br/>
                        E-mail: <?php echo $branch_email;?><br/>
                        Tel : <?php echo $branch_phone;?><br/>
                        <?php echo $branch_location;?>
                    </th>
                    <th>
                        <img src="<?php echo base_url().'assets/logo/'.$branch_image_name;?>" alt="<?php echo $branch_name;?>" class="img-responsive logo"/>
                    </th>
                </tr>
            </table>
        </div>

        <div class="row receipt_bottom_border" >
        	<div class="col-md-12">
            	<?php echo $result;?>
            </div>
        
        </div>

        <div class="row">
          <div class="col-xs-12">
          <div class="col-xs-12" style="margin-bottom: 30px; margin-top: 20px;">
              <div class="col-xs-4 pull-left">
                  Prepared by : ......................................................
                </div>
                <div class="col-xs-4 pull-left">
                  Signature : ......................................................
                </div>
                <div class="col-xs-4 pull-left">
                  Date : ......................................................
                </div>
            </div>
            <div class="col-xs-12" style="margin-bottom: 30px;">
              <div class="col-xs-4 pull-left">
                  Confirmed by : ......................................................
                </div>
                <div class="col-xs-4 pull-left">
                  Signature : ......................................................
                </div>
                <div class="col-xs-4 pull-left">
                  Date : ......................................................
                </div>
            </div>
            
        </div>
    </div>
    <div class="row">
      <div class="col-xs-12 center-align">
        <?php echo 'Date Printed : '.date('jS M Y H:i:s',strtotime(date('Y-m-d H:i:s')));?>
      </div>
    </div>
			<a href="#" onclick="javascript:xport.toCSV('testTable');">XLS</a>
<!--<a href="#" onClick ="$('#customers').tableExport({type:'csv',escape:'false'});">CSV</a>
<a href="#" onClick ="$('#customers').tableExport({type:'pdf',escape:'false'});">PDF</a>-->

    </body>
</html>
<script type="text/javascript">
	var xport = {
  _fallbacktoCSV: true,
  toXLS: function(tableId, filename) {
    this._filename = (typeof filename == 'undefined') ? tableId : filename;

    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls');
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }

    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it

      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};

</script>
