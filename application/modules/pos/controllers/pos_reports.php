<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/administration/controllers/administration.php";
error_reporting(E_ALL);
class Pos_reports extends administration
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('reception/reception_model');
		$this->load->model('reports_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('nurse/nurse_model');
		$this->load->model('pos/pos_model');
		$this->load->model('pos/pos_reports_model');
		$this->load->model('admin/dashboard_model');
	}

	public function cash_sales()
	{

		$where = 'pos_order_item.pos_order_id = pos_order.pos_order_id AND sale_type = 0  AND pos_order.pos_order_status >= 3';
		$table = 'pos_order_item,pos_order';

		$visit_search = $this->session->userdata('cash_sale_search');

		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		else
		{
			$where .= ' AND pos_order.order_date = "'.date('Y-m-d').'"';
		}
		$segment = 3;

		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'administrative-reports/cash-sales';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 5000000;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->pos_reports_model->get_all_order_sales($table, $where, $config["per_page"], $page, 'ASC');

		$v_data['query'] = $query;
		$v_data['page'] = $page;


		$total_amount = $this->pos_reports_model->get_total_sale($table,$where);


		$page_title = $this->session->userdata('cash_sale_title');
		if(empty($page_title))
		{
			$page_title = 'All cash sales for '.date('Y-m-d');
		}

		$v_data['total_amount'] = $total_amount;
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = $page_title;
		
		$data['content'] = $this->load->view('pos/reports/cash_sales', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);

	}

	public function search_cash_sales()
	{
		
		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');

		$search_title = 'Showing reports for: ';

		if(!empty($date_from) && !empty($date_to))
		{
			$date = ' AND pos_order.order_date BETWEEN \''.$date_from.'\' AND \''.$date_to.'\'';
			
			$search_title .= 'Cash sale from '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
		}

		else if(!empty($date_from))
		{
			$date = ' AND pos_order.order_date = \''.$date_from.'\'';
			$search_title .= 'Cash sale of '.date('jS M Y', strtotime($date_from)).' ';
		}

		else if(!empty($date_to))
		{
			$date = ' AND pos_order.order_date = \''.$date_to.'\'';
			$search_title .= 'Cash sale of '.date('jS M Y', strtotime($date_to)).' ';
		}

		else
		{
			$date = '';
		}

		$search = $date;
		

		$this->session->set_userdata('cash_sale_search', $search);
		$this->session->set_userdata('cash_sale_title', $search_title);

		redirect('administrative-reports/cash-sales');
	}

	public function close_cash_sales_search()
	{
		$this->session->unset_userdata('cash_sale_search');
		$this->session->unset_userdata('cash_sale_title');
		redirect('administrative-reports/cash-sales');
	}

	public function credit_sales()
	{
		$where = 'pos_order_item.pos_order_id = pos_order.pos_order_id AND pos_order.order_date = "' .date('Y-m-d') .'" AND sale_type = 1 AND pos_order_item.order_invoice_id > 0';
		$table = 'pos_order_item,pos_order';

		$visit_search = $this->session->userdata('credit_sale_search');

		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		
		$segment = 3;

		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'credit-sale/credit-sale-daily-list';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 1000000000000;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->pos_reports_model->get_all_order_sales($table, $where, $config["per_page"], $page, 'ASC');

		$v_data['query'] = $query;
		$v_data['page'] = $page;

		$total_amount = $this->pos_reports_model->get_total_sale($table,$where);
		$v_data['total_amount'] = $total_amount;
		$page_title = $this->session->userdata('credit_sale_title');
		if(empty($page_title))
		{
			$page_title = 'All credit sales for '.date('jS M Y');
		}
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = $page_title;
		
		$data['content'] = $this->load->view('pos/reports/credit_sales', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_credit_sales()
	{
		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');

		$search_title = 'Showing reports for: ';

		if(!empty($date_from) && !empty($date_to))
		{
			$date = ' AND pos_order.order_date BETWEEN \''.$date_from.'\' AND \''.$date_to.'\'';
			
			$search_title .= 'Credit sale from '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
		}

		else if(!empty($date_from))
		{
			$date = ' AND pos_order.order_date = \''.$date_from.'\'';
			$search_title .= 'Credit sale of '.date('jS M Y', strtotime($date_from)).' ';
		}

		else if(!empty($date_to))
		{
			$date = ' AND pos_order.order_date = \''.$date_to.'\'';
			$search_title .= 'Credit sale of '.date('jS M Y', strtotime($date_to)).' ';
		}

		else
		{
			$date = '';
		}

		$search = $date;
		

		$this->session->set_userdata('credit_sale_search', $search);
		$this->session->set_userdata('credit_sale_title', $search_title);

		redirect('credit-sale/credit-sale-daily-list');
	}
	public function close_credit_sales_search()
	{
		$this->session->unset_userdata('credit_sale_search');
		$this->session->unset_userdata('credit_sale_title');
		redirect('credit-sale/credit-sale-daily-list');
		
	}
	public function view_sold_items($pos_order_id,$order_invoice_id = 0)
	{
		$data['pos_order_id'] = $pos_order_id;
		$data['order_invoice_id'] = $order_invoice_id;
		
		$page=$this->load->view('reports/sale_items',$data,true);

		echo $page;
	}
	public function payments_report()
	{
		$where = 'pos_order.pos_order_id = order_invoice.pos_order_id AND pos_payments.pos_order_id = pos_order.pos_order_id AND pos_payments.cancel = 0 AND pos_payment_item.payment_id = pos_payments.payment_id AND payment_method.payment_method_id = pos_payments.payment_method_id';
		$table = 'pos_order,pos_payments,pos_payment_item,payment_method,order_invoice';


		// $where = 'pos_payments.pos_order_id = pos_order.pos_order_id AND pos_payments.payment_id = pos_payment_item.payment_id  AND pos_payments.cancel = 0 AND payment_method.payment_method_id = pos_payments.payment_method_id';
		// $table = 'pos_order,pos_payments,pos_payment_item,payment_method';

		// var_dump($table);die();
		$visit_search = $this->session->userdata('payment_report_date');

		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		else
		{
			$where .= ' AND pos_order.order_date = "' .date('Y-m-d') .'"';
		}
		
		$segment = 3;

		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'administrative-reports/payments-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->pos_reports_model->get_all_payments_report($table, $where, $config["per_page"], $page, 'ASC');

		$v_data['query'] = $query;
		$v_data['page'] = $page;


		$total_amount = 0;//$this->pos_reports_model->get_total_sale($table,$where);


		$page_title = $this->session->userdata('payment_report_title');
		if(empty($page_title))
		{
			$page_title = 'All payments report for '.date('Y-m-d');
		}

		$v_data['total_amount'] = $total_amount;
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = $page_title;
		
		$data['content'] = $this->load->view('pos/reports/payments_report', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_payments_report()
	{
		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');

		$search_title = 'Showing reports for: ';

		if(!empty($date_from) && !empty($date_to))
		{
			$date = ' AND pos_order.order_date BETWEEN \''.$date_from.'\' AND \''.$date_to.'\'';
			
			$search_title .= 'Payments Report from '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
		}

		else if(!empty($date_from))
		{
			$date = ' AND pos_order.order_date = \''.$date_from.'\'';
			$search_title .= 'Payments Report of '.date('jS M Y', strtotime($date_from)).' ';
		}

		else if(!empty($date_to))
		{
			$date = ' AND pos_order.order_date = \''.$date_to.'\'';
			$search_title .= 'Payments Report of '.date('jS M Y', strtotime($date_to)).' ';
		}

		else
		{
			$date = '';
		}

		$search = $date;
		

		$this->session->set_userdata('payment_report_date', $search);
		$this->session->set_userdata('payment_report_title', $search_title);

		redirect('administrative-reports/payments-report');
	}

	public function close_payments_search()
	{
		$this->session->unset_userdata('payment_report_date');
		$this->session->unset_userdata('payment_report_title');

		redirect('administrative-reports/payments-report');
	}
	

	public function debtors_report()
	{
	


		$where = 'pos_order_item.pos_order_id = pos_order.pos_order_id  ';
		$table = 'pos_order_item,pos_order';

		$visit_search = $this->session->userdata('debtor_report_date');

		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		else
		{
			$where .= ' AND pos_order.order_date = "' .date('Y-m-d') .'"';
		}
		
		$segment = 3;

		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'administrative-reports/debtors-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->pos_reports_model->get_all_debtors_report($table, $where, $config["per_page"], $page, 'ASC');

		$v_data['query'] = $query;
		$v_data['page'] = $page;


		$total_amount = 0;//$this->pos_reports_model->get_total_sale($table,$where);


		$page_title = $this->session->userdata('debtor_report_title');
		if(empty($page_title))
		{
			$page_title = 'All debtors report for '.date('Y-m-d');
		}

		$v_data['total_amount'] = $total_amount;
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = $page_title;
		
		$data['content'] = $this->load->view('pos/reports/debtors_report', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_debtors_report()
	{
		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');

		$search_title = 'Showing reports for: ';

		if(!empty($date_from) && !empty($date_to))
		{
			$date = ' AND pos_order.order_date BETWEEN \''.$date_from.'\' AND \''.$date_to.'\'';
			
			$search_title .= 'debtor Report from '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
		}

		else if(!empty($date_from))
		{
			$date = ' AND pos_order.order_date = \''.$date_from.'\'';
			$search_title .= 'debtor Report of '.date('jS M Y', strtotime($date_from)).' ';
		}

		else if(!empty($date_to))
		{
			$date = ' AND pos_order.order_date = \''.$date_to.'\'';
			$search_title .= 'debtor Report of '.date('jS M Y', strtotime($date_to)).' ';
		}

		else
		{
			$date = '';
		}

		$search = $date;
		

		$this->session->set_userdata('debtor_report_date', $search);
		$this->session->set_userdata('debtor_report_title', $search_title);

		redirect('administrative-reports/debtors-report');
	}

	public function close_debtors_search()
	{
		$this->session->unset_userdata('debtor_report_date');
		$this->session->unset_userdata('debtor_report_title');

		redirect('administrative-reports/debtors-report');
	}

	public function sales_report()
	{

		// var_dump("dasda");die();
		$where = 'pos_order_item.pos_order_id = pos_order.pos_order_id AND service_charge.service_charge_id = pos_order_item.service_charge_id AND pos_order.pos_order_status = 3 ';
		$table = 'pos_order_item,pos_order,service_charge';

		$visit_search = $this->session->userdata('sales_report_date');

		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		else
		{
			$where .= ' AND pos_order.order_date = "' .date('Y-m-d') .'"';
		}
		
		$segment = 3;

		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'administrative-reports/sales-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 2000;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->pos_reports_model->get_all_sales_report_per_item($table, $where, $config["per_page"], $page, 'ASC');

		$v_data['query'] = $query;
		$v_data['page'] = $page;


		$total_amount = 0;//$this->pos_reports_model->get_total_sale($table,$where);


		$page_title = $this->session->userdata('sales_report_title');
		if(empty($page_title))
		{
			$page_title = 'Product report for '.date('Y-m-d');
		}

		$v_data['total_amount'] = $total_amount;
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = $page_title;
		
		$data['content'] = $this->load->view('pos/reports/sales_report', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);

	}

	public function search_sales_report()
	{
		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');

		$search_title = 'Showing reports for: ';

		if(!empty($date_from) && !empty($date_to))
		{
			$date = ' AND pos_order.order_date BETWEEN \''.$date_from.'\' AND \''.$date_to.'\'';
			
			$search_title .= 'Sales Report from '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
		}

		else if(!empty($date_from))
		{
			$date = ' AND pos_order.order_date = \''.$date_from.'\'';
			$search_title .= 'Sales Report of '.date('jS M Y', strtotime($date_from)).' ';
		}

		else if(!empty($date_to))
		{
			$date = ' AND pos_order.order_date = \''.$date_to.'\'';
			$search_title .= 'Sales Report of '.date('jS M Y', strtotime($date_to)).' ';
		}

		else
		{
			$date = '';
		}

		$search = $date;
		

		$this->session->set_userdata('sales_report_date', $search);
		$this->session->set_userdata('sales_report_title', $search_title);

		redirect('administrative-reports/sales-report');
	}

	public function close_sales_search()
	{
		$this->session->unset_userdata('sales_report_date');
		$this->session->unset_userdata('sales_report_title');

		redirect('administrative-reports/sales-report');
	}
	
	public function cash_sale_totals()
	{

		$page_title = $this->session->userdata('cash_sale_summary_report_title');
		if(empty($page_title))
		{
			$page_title = 'Cash Sale report for '.date('Y-m-d');
		}

		$v_data['total_amount'] = 0;
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = $page_title;
		
		$data['content'] = $this->load->view('pos/reports/cash_sale_totals_report', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}
	public function get_sales_totals()
	{

		$where = 'pos_payments.pos_order_id = pos_order.pos_order_id AND pos_payments.payment_id = pos_payment_item.payment_id  AND pos_payments.cancel = 0 AND payment_method.payment_method_id = pos_payments.payment_method_id';
		$table = 'pos_order,pos_payments,pos_payment_item,payment_method';

		// var_dump($table);die();

		// $first_day = date('l - Y-m-d', strtotime("this week"));

		$visit_search = $this->session->userdata('cash_sale_summary_report_date');

		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		else
		{
			$where .= ' AND pos_order.order_date = "' .date('Y-m-d') .'"';
		}
	
		$query = $this->pos_reports_model->get_cash_sales_summary($table,$where);
		

		$v_data['total_amount'] = 0;
		$v_data['query'] = $query;
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = ''; 
		
		$items =  $this->load->view('pos/reports/cash_sale_summary', $v_data, true);

		echo json_encode($items);
		
	}
	public function cash_sale_item_list()
	{
 
		$where = 'pos_order_item.pos_order_id = pos_order.pos_order_id AND pos_order.sale_type = 0 AND order_invoice.pos_order_id = pos_order.pos_order_id';
		$table = 'pos_order_item,pos_order,order_invoice';



		$visit_search = $this->session->userdata('cash_sale_item_report_date');

		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		else
		{
			$where .= ' AND pos_order.order_date = "' .date('Y-m-d') .'"';
		}
		
		$segment = 3;

		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'cash-sale/cash-sale-item-list';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->pos_reports_model->get_all_cash_sale_item_list($table, $where, $config["per_page"], $page, 'ASC');

		$v_data['query'] = $query;
		$v_data['page'] = $page;


		$total_amount = 0;//$this->pos_reports_model->get_total_sale($table,$where);


		$page_title = $this->session->userdata('cash_sale_item_report_title');
		if(empty($page_title))
		{
			$page_title = 'Cash Sale item list report for '.date('Y-m-d');
		}

		$v_data['total_amount'] = $total_amount;
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = $page_title;
		
		$data['content'] = $this->load->view('pos/reports/cash_sale_item_list', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}
	public function search_cash_sale_item_list()
	{
		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');

		$search_title = 'Showing reports for: ';

		if(!empty($date_from) && !empty($date_to))
		{
			$date = ' AND pos_order.order_date BETWEEN \''.$date_from.'\' AND \''.$date_to.'\'';
			
			$search_title .= 'Cash Sale Item Report from '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
		}

		else if(!empty($date_from))
		{
			$date = ' AND pos_order.order_date = \''.$date_from.'\'';
			$search_title .= 'Cash Sale Item Report of '.date('jS M Y', strtotime($date_from)).' ';
		}

		else if(!empty($date_to))
		{
			$date = ' AND pos_order.order_date = \''.$date_to.'\'';
			$search_title .= 'Cash Sale Item Report of '.date('jS M Y', strtotime($date_to)).' ';
		}

		else
		{
			$date = '';
		}

		$search = $date;
		

		$this->session->set_userdata('cash_sale_item_report_date', $search);
		$this->session->set_userdata('cash_sale_item_report_title', $search_title);

		redirect('cash-sale/cash-sale-item-list');
	}

	public function search_cash_sale_totals()
	{

		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');

		$search_title = 'Showing reports for: ';

		if(!empty($date_from) && !empty($date_to))
		{
			$date = ' AND pos_order.order_date BETWEEN \''.$date_from.'\' AND \''.$date_to.'\'';
			
			$search_title .= 'Cash Sale Item Report from '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
		}

		else if(!empty($date_from))
		{
			$date = ' AND pos_order.order_date = \''.$date_from.'\'';
			$search_title .= 'Cash Sale Item Report of '.date('jS M Y', strtotime($date_from)).' ';
		}

		else if(!empty($date_to))
		{
			$date = ' AND pos_order.order_date = \''.$date_to.'\'';
			$search_title .= 'Cash Sale Item Report of '.date('jS M Y', strtotime($date_to)).' ';
		}

		else
		{
			$date = '';
		}

		$search = $date;
		

		$this->session->set_userdata('cash_sale_summary_report_date', $search);
		$this->session->set_userdata('cash_sale_summary_report_title', $search_title);

		redirect('cash-sale/cash-sale-totals');
		
	}
	public function credit_sale_totals()
	{
		$page_title = $this->session->userdata('credit_sale_summary_report_title');
		if(empty($page_title))
		{
			$page_title = 'Credit Sale report for '.date('Y-m-d');
		} 

		$v_data['total_amount'] = 0;
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = $page_title;
		
		$data['content'] = $this->load->view('pos/reports/credit_sale_totals_report', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}
	public function get_credit_sales_totals()
	{
		
		$where = 'pos_order_item.pos_order_id = pos_order.pos_order_id AND pos_order.sale_type = 1 AND order_invoice.pos_order_id = pos_order.pos_order_id';
		$table = 'pos_order_item,pos_order,order_invoice';

		// var_dump($table);die();

		// $first_day = date('l - Y-m-d', strtotime("this week"));

		$visit_search = $this->session->userdata('credit_sale_summary_report_date');

		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		else
		{
			$where .= ' AND pos_order.order_date = "' .date('Y-m-d') .'"';
		}
	
		$query = $this->pos_reports_model->get_credit_sales_summary($table,$where);
		

		$v_data['total_amount'] = 0;
		$v_data['query'] = $query;
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = '';
		
		$items =  $this->load->view('pos/reports/credit_sale_summary', $v_data, true);

		echo json_encode($items);
	}
	public function search_credit_sale_totals()
	{

		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');

		$search_title = 'Showing reports for: ';

		if(!empty($date_from) && !empty($date_to))
		{
			$date = ' AND pos_order.order_date BETWEEN \''.$date_from.'\' AND \''.$date_to.'\'';
			
			$search_title .= 'Credit Sale Report from '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
		}

		else if(!empty($date_from))
		{
			$date = ' AND pos_order.order_date = \''.$date_from.'\'';
			$search_title .= 'Credit Sale  Report of '.date('jS M Y', strtotime($date_from)).' ';
		}

		else if(!empty($date_to))
		{
			$date = ' AND pos_order.order_date = \''.$date_to.'\'';
			$search_title .= 'Credit Sale Report of '.date('jS M Y', strtotime($date_to)).' ';
		}

		else
		{
			$date = '';
		}

		$search = $date;
		

		$this->session->set_userdata('credit_sale_summary_report_date', $search);
		$this->session->set_userdata('credit_sale_summary_report_title', $search_title);

		redirect('credit-sale/credit-sale-totals');
		
	}

	public function credit_sale_item_list()
	{
		$where = 'pos_order_item.pos_order_id = pos_order.pos_order_id AND pos_order.sale_type = 1 AND order_invoice.pos_order_id = pos_order.pos_order_id';
		$table = 'pos_order_item,pos_order,order_invoice';



		$visit_search = $this->session->userdata('credit_sale_item_report_date');

		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		else
		{
			$where .= ' AND pos_order.order_date = "' .date('Y-m-d') .'"';
		}
		
		$segment = 3;

		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'credit-sale/credit-sale-item-list';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->pos_reports_model->get_all_cash_sale_item_list($table, $where, $config["per_page"], $page, 'ASC');

		$v_data['query'] = $query;
		$v_data['page'] = $page;


		$total_amount = 0;//$this->pos_reports_model->get_total_sale($table,$where);


		$page_title = $this->session->userdata('credit_sale_item_report_title');
		if(empty($page_title))
		{
			$page_title = 'Credit Sale item list report for '.date('Y-m-d');
		}

		$v_data['total_amount'] = $total_amount;
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = $page_title;
		
		$data['content'] = $this->load->view('pos/reports/credit_sale_item_list', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_credit_sale_item_list()
	{
		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');

		$search_title = 'Showing reports for: ';

		if(!empty($date_from) && !empty($date_to))
		{
			$date = ' AND pos_order.order_date BETWEEN \''.$date_from.'\' AND \''.$date_to.'\'';
			
			$search_title .= 'Credit Sale Item Report from '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
		}

		else if(!empty($date_from))
		{
			$date = ' AND pos_order.order_date = \''.$date_from.'\'';
			$search_title .= 'Credit Sale Item Report of '.date('jS M Y', strtotime($date_from)).' ';
		}

		else if(!empty($date_to))
		{
			$date = ' AND pos_order.order_date = \''.$date_to.'\'';
			$search_title .= 'Credit Sale Item Report of '.date('jS M Y', strtotime($date_to)).' ';
		}

		else
		{
			$date = '';
		}

		$search = $date;
		

		$this->session->set_userdata('credit_sale_item_report_date', $search);
		$this->session->set_userdata('credit_sale_item_report_title', $search_title);

		redirect('credit-sale/credit-sale-item-list');
	}
	public function export_cash_sale_list_items()
	{
		$this->pos_reports_model->export_cash_sale_list_items();
	}
	public function print_cash_sale_list_items()
	{

		$data['contacts'] = $this->site_model->get_contacts();
		$data['page_item'] = 0;
		$where = 'pos_order_item.pos_order_id = pos_order.pos_order_id AND pos_order.sale_type = 0 AND order_invoice.pos_order_id = pos_order.pos_order_id';
	    $table = 'pos_order_item,pos_order,order_invoice';



	    $visit_search = $this->session->userdata('cash_sale_item_report_date');


	    if(!empty($visit_search))
	    {
	        $where .= $visit_search;
	    }
	    else
	    {
	        $where .= ' AND pos_order.order_date = "' .date('Y-m-d') .'"';
	    }



	    $this->db->select('pos_order.*,pos_order_item.*,service_charge.service_charge_name,order_invoice.order_invoice_number');
	    $this->db->where($where);
	    $this->db->join('service_charge', 'pos_order_item.service_charge_id = service_charge.service_charge_id', 'left');
	  
	    $this->db->join('personnel', 'personnel.personnel_id = pos_order.created_by', 'left');
	    $this->db->join('customer', 'customer.customer_id = pos_order.customer_id', 'left');
	    $this->db->group_by('pos_order.pos_order_id');
	    $visits_query = $this->db->get($table);

	    $title = $this->session->userdata('cash_sale_item_report_title');
	    if(empty($title))
	    {
	        $title = 'Cash Sale item list report for '.date('Y-m-d');
	    }
		
		$data['title'] = $title;
		$data['visits_query'] = $visits_query;

		// var_dump($data);die();
		$this->load->view('print_cash_sale_list_items', $data);
	}


	public function print_cash_sale_summary()
	{

		$data['contacts'] = $this->site_model->get_contacts();
		$data['page_item'] = 0;
		$where = 'pos_payments.pos_order_id = pos_order.pos_order_id AND pos_payments.payment_id = pos_payment_item.payment_id  AND pos_payments.cancel = 0 AND payment_method.payment_method_id = pos_payments.payment_method_id';
		$table = 'pos_order,pos_payments,pos_payment_item,payment_method';

		// var_dump($table);die();

		// $first_day = date('l - Y-m-d', strtotime("this week"));

		$visit_search = $this->session->userdata('cash_sale_summary_report_date');

		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		else
		{
			$where .= ' AND pos_order.order_date = "' .date('Y-m-d') .'"';
		}
	
		$query = $this->pos_reports_model->get_cash_sales_summary($table,$where);

	    $title = $this->session->userdata('cash_sale_summary_report_title');
	    if(empty($title))
	    {
	        $title = 'Cash Sale Summary report for '.date('Y-m-d');
	    }
		
		$data['title'] = $title;
		$data['query'] = $query;

		// var_dump($data);die();
		$this->load->view('print_cash_sale_summary_report', $data);
	}

	public function export_cash_sale_summary()
	{
		$this->pos_reports_model->export_cash_sale_summary();
	}


	public function print_credit_sale_summary(){
		$data['contacts'] = $this->site_model->get_contacts();
		$data['page_item'] = 0;
		$where = 'pos_order_item.pos_order_id = pos_order.pos_order_id AND pos_order.sale_type = 1 AND order_invoice.pos_order_id = pos_order.pos_order_id';
		$table = 'pos_order_item,pos_order,order_invoice';

		// var_dump($table);die();

		// $first_day = date('l - Y-m-d', strtotime("this week"));

		$visit_search = $this->session->userdata('credit_sale_summary_report_date');

		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		else
		{
			$where .= ' AND pos_order.order_date = "' .date('Y-m-d') .'"';
		}
	
		$query = $this->pos_reports_model->get_credit_sales_summary($table,$where);


	    $title = $this->session->userdata('credit_sale_summary_report_title');
	    if(empty($title))
	    {
	        $title = 'Credit Sale Summary report for '.date('Y-m-d');
	    }
		
		$data['title'] = $title;
		$data['query'] = $query;

		// var_dump($data);die();
		$this->load->view('print_credit_sale_summary', $data);
	}

	public function export_credit_sale_summary(){
		$this->pos_reports_model->export_credit_sale_summary();
	}

	public function print_credit_sale_list_items(){
		$data['contacts'] = $this->site_model->get_contacts();
		$data['page_item'] = 0;
		$where = 'pos_order_item.pos_order_id = pos_order.pos_order_id AND pos_order.sale_type = 1 AND order_invoice.pos_order_id = pos_order.pos_order_id';
		$table = 'pos_order_item,pos_order,order_invoice';
		// var_dump($table);die();

		// $first_day = date('l - Y-m-d', strtotime("this week"));
		$segment = 3;


		$visit_search = $this->session->userdata('credit_sale_summary_report_date');

		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		else
		{
			$where .= ' AND pos_order.order_date = "' .date('Y-m-d') .'"';
		}

		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'credit-sale/credit-sale-item-list';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
	
		$query = $this->pos_reports_model->get_all_cash_sale_item_list($table, $where, $config["per_page"], $page, 'ASC');


	    $title = $this->session->userdata('credit_sale_list_item_report_title');
	    if(empty($title))
	    {
	        $title = 'Credit Sale List Item report for '.date('Y-m-d');
	    }
		
		$data['title'] = $title;
		$data['query'] = $query;
		$data['page'] = $page;

		// var_dump($data);die();
		$this->load->view('print_credit_sale_list_items', $data);

	}

	public function export_credit_sale_list_items(){
		$this->pos_reports_model->export_credit_sale_list_items();

	}


	public function export_cash_sale()
	{
		$this->pos_reports_model->export_cash_sale();
	}
	public function print_cash_sale()
	{
		$data['contacts'] = $this->site_model->get_contacts();
		$data['page_item'] = 0;


		$where = 'pos_order_item.pos_order_id = pos_order.pos_order_id AND pos_order.sale_type = 1 AND order_invoice.pos_order_id = pos_order.pos_order_id';
		$table = 'pos_order_item,pos_order,order_invoice';

		// var_dump($table);die();

		// $first_day = date('l - Y-m-d', strtotime("this week"));

		$visit_search = $this->session->userdata('credit_sale_summary_report_date');

		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		else
		{
			$where .= ' AND pos_order.order_date = "' .date('Y-m-d') .'"';
		}
	
		$query = $this->pos_reports_model->get_credit_sales_summary($table,$where);


	    $title = $this->session->userdata('credit_sale_summary_report_title');
	    if(empty($title))
	    {
	        $title = 'Credit Sale Report for '.date('Y-m-d');
	    }
		
		$data['title'] = $title;
		$data['query'] = $query;

		// var_dump($data);die();
		$this->load->view('print_credit_sale_summary', $data);
	}

	public function export_credit_sale()
	{
		$this->pos_reports_model->export_credit_sale();
	}

	public function sales_status_report()
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('reports/sales_status_report', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}
	public function get_product_list_items_inquiry()
	{

		$description = $this->input->post('description');
		$part_no = $this->input->post('part_no');
		$vehicle_model = $this->input->post('vehicle_model');
		$vehicle_name = $this->input->post('vehicle_name');
		$valley_no = $this->input->post('valley_no');
		$query = null;
		
		$lab_test_where = 'service_charge.service_charge_delete = 0';
		$lab_test_table = 'service_charge';

		if(!empty($description))
		{
			$lab_test_where .= ' AND (service_charge.service_charge_name LIKE \'%'.$description.'%\')';
		}
		



		$this->db->where($lab_test_where);
		$this->db->limit(1000);
		$this->db->join('product','product.product_id = service_charge.product_id','left');
		$this->db->join('brand','product.brand_id = brand.brand_id','left');
		$this->db->join('service','service_charge.service_id = service.service_id','left');
		$query = $this->db->get($lab_test_table);

			
		$data['query'] = $query;
		$data['visit_id'] = NULL;
		$data['patient_id'] = NULL;
		$data['visit_type_id'] = 1;
		$data['visit_invoice_id'] = NULL;
		$page = $this->load->view('reports/products_list_inquery',$data);

		echo $page;
	}


	public function get_all_unavailable_products()
	{

		$lab_test_where = 'service_charge_statuses.service_charge_status_delete = 0 AND service_charge.service_charge_id = service_charge_statuses.service_charge_id AND service_charge_statuses.status_created = "'.date('Y-m-d').'" AND service.service_id = service_charge.service_id';
		$lab_test_table = 'service_charge_statuses,service_charge,service';


		$this->db->where($lab_test_where);
		$this->db->limit(1000);
		$query = $this->db->get($lab_test_table);

			
		$data['query'] = $query;
		$page = $this->load->view('reports/products_statuses',$data);

		echo $page;
	}

	public function mark_not_available($service_charge_id)
	{

		$this->db->where('service_charge_statuses.service_charge_status_delete = 0 AND service_charge_statuses.status_created = "'.date('Y-m-d').'"  AND service_charge_statuses.service_charge_id = '.$service_charge_id);
		$query = $this->db->get('service_charge_statuses');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$service_charge_statuses_id = $value->service_charge_statuses_id;
				$array['service_charge_status_delete'] = 1;
				$this->db->where('service_charge_statuses_id',$service_charge_statuses_id);
				$this->db->update('service_charge_statuses',$array);

			}
			$array['service_charge_id'] = $service_charge_id;
			$array['status_created'] = date('Y-m-d');
			$array['service_charge_status_delete'] = 0;
			$this->db->insert('service_charge_statuses',$array);
		}
		else
		{
			$array['service_charge_id'] = $service_charge_id;
			$array['status_created'] = date('Y-m-d');
			$array['service_charge_status_delete'] = 0;
			$this->db->insert('service_charge_statuses',$array);
		}


		$response['status'] = 'success';
		$response['message'] = 'success';

		echo json_encode($response);

	}

	public function unmark_not_available($service_charge_id)
	{

		$array['service_charge_status_delete'] = 1;
		$this->db->where('status_created = "'.date('Y-m-d').'" AND service_charge_id ='.$service_charge_id);
		$this->db->update('service_charge_statuses',$array);


		$response['status'] = 'success';
		$response['message'] = 'success';

		echo json_encode($response);
	}
	public function my_sales()
	{

	
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = 'MY SALES';


		$v_data['sale_type'] = 0;
		
		$response['message'] = 'success';
		$response['result'] = $this->load->view('sidebar/my_sales', $v_data,true);

		echo json_encode($response);
	}

	public function view_my_sales()
	{
		$personnel_id = $this->session->userdata('personnel_id');
		$where = 'pos_order_item.pos_order_id = pos_order.pos_order_id AND pos_order.pos_order_status >= 3 AND pos_order.created_by ='.$personnel_id;
		$table = 'pos_order_item,pos_order';
				// var_dump($table);die();
		$visit_search = $this->session->userdata('my_cash_sale_search');

		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		else
		{
			$where .= ' AND pos_order.order_date = "'.date('Y-m-d').'"';
		}
		

		$list_status = $this->input->post('list_status');


		$weekfirstday = date('Y-m-d', strtotime("this week"));
		$yesterday = date('Y-m-d', strtotime("yesterday"));

		$firstdayofthemonth = date('Y-m-01');

		$last_month_first_day =  date('Y-m-d', strtotime('first day of last month'));


		$last_month_end_day =  date('Y-m-d', strtotime('last day of last month'));

		if($list_status == 0){
			$title = 'Today '.date('Y-m-d');
			$where  .= ' AND pos_order.order_date = "'.date('Y-m-d').'"';
		}
		else if($list_status == 1){
			$title = 'Today '.date('Y-m-d');
			$where  .= ' AND pos_order.order_date = "'.date('Y-m-d').'"';
		}
		else if($list_status == 2){
			$title = 'Today '.$yesterday;
			$where  .= ' AND pos_order.order_date = "'.$yesterday.'"';
		}
		else if($list_status == 3){
				$title = 'This Week ';
			$where  .= ' AND pos_order.order_date BETWEEN "'.$weekfirstday.'" AND "'.date('Y-m-d').'"';
		}
		else if($list_status == 4){


			$title = 'This Month ';
			$where  .= ' AND pos_order.order_date BETWEEN "'.$firstdayofthemonth.'" AND "'.date('Y-m-d').'"';
		}
		else if($list_status == 5){
			$title = 'Last Month ';
			$where  .= ' AND pos_order.order_date BETWEEN "'.$last_month_first_day.'" AND "'.$last_month_end_day.'"';
		}
		else if($list_status == 6){
			$title = 'This Year ';
			$where  .= ' AND YEAR(pos_order.order_date) = '.date('Y');
		}
		else{
			$title = 'Today ';
			$where  .= ' AND pos_order.order_date = "'.date('Y-m-d').'"';
		}


		$segment = 3;

		//pagination
		// $this->load->library('pagination');
		$query = $this->pos_reports_model->get_my_all_order_sales($table, $where);

		$v_data['query'] = $query;
		$v_data['page'] = 0;
		// var_dump($query);die();

		$total_amount = 0;//$this->pos_reports_model->get_total_sale($table,$where);


		$page_title = $this->session->userdata('cash_sale_title');
		if(empty($page_title))
		{
			$page_title = 'All cash sales for '.date('Y-m-d');
		}

		$v_data['total_amount'] = $total_amount;
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = $page_title;


		$v_data['sale_type'] = 0;
		
		$response['message'] = 'success';
		$response['title'] = '<div class="alert alert-sm alert-info"> Report For '.$title.'</div>';
		$response['result'] = $this->load->view('reports/my_sales_list', $v_data,true);

		echo json_encode($response);

	}

	public function personnel_report()
	{




		$query = $this->pos_reports_model->get_personnel_report(1);
		
		$v_data['query'] = $query;


		$total_amount = 0;//$this->pos_reports_model->get_total_sale($table,$where);


		$page_title = $this->session->userdata('staff_sales_report_title');
		if(empty($page_title))
		{
			$page_title = 'Product report for '.date('Y-m-d');
		}


		$data['title'] = $v_data['title'] = $page_title;
		
		$data['content'] = $this->load->view('pos/reports/staff_sales', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);

	}

	public function staff_sales_report()
	{

		$query = $this->pos_reports_model->get_personnel_report();
		
		$v_data['query'] = $query;

		$page_title = $this->session->userdata('staff_sales_report_title');
		if(empty($page_title))
		{
			$page_title = 'Sales report for '.date('Y-m-d');
		}


		$data['title'] = $v_data['title'] = $page_title;
		
		$response['content'] = $this->load->view('pos/reports/staff_sales_list', $v_data,true);
		$response['message'] = 'success'; 
		echo json_encode($response);


	}


	public function search_personnel_report()
	{
		
		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');

		$search_title = 'Showing reports for: ';

		if(!empty($date_from) && !empty($date_to))
		{
			$date = ' AND pos_order.order_date BETWEEN \''.$date_from.'\' AND \''.$date_to.'\'';
			
			$search_title .= 'Staff Sales from '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
		}

		else if(!empty($date_from))
		{
			$date = ' AND pos_order.order_date = \''.$date_from.'\'';
			$search_title .= 'Staff Sales of '.date('jS M Y', strtotime($date_from)).' ';
		}

		else if(!empty($date_to))
		{
			$date = ' AND pos_order.order_date = \''.$date_to.'\'';
			$search_title .= 'Staff Sales of '.date('jS M Y', strtotime($date_to)).' ';
		}

		else
		{
			$date = '';
		}

		$search = $date;
		

		$this->session->set_userdata('staff_sales_report_date', $search);
		$this->session->set_userdata('staff_sales_report_title', $search_title);

		redirect('administrative-reports/staff-sales-report');
	}

	public function close_staff_sales_search()
	{
		$this->session->unset_userdata('staff_sales_report_date');
		$this->session->unset_userdata('staff_sales_report_title');
		redirect('administrative-reports/staff-sales-report');
	}
	
}
?>
