<?php



$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y');

//doctor
// $doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
$served_by = $this->pos_model->get_personnel($this->session->userdata('personnel_id'));
// var_dump($served_by);die();
if(empty($served_by))
{
    $served_by = '&nbsp;';
}
// $credit_note_amount = $this->accounts_model->get_sum_credit_notes($visit_id);
// $debit_note_amount = $this->accounts_model->get_sum_debit_notes($visit_id);


$visit_rs = $this->pos_model->get_order_details($pos_order_id);

$sale_type = 0;
$pos_order_status = 1;
$vat_status = 0;
if($visit_rs->num_rows() > 0)
{
    foreach ($visit_rs->result() as $key => $value) {
        # code...
        $sale_type = $value->sale_type;
        $pos_order_status = $value->pos_order_status;
        $vat_status = $value->vat_status;
    }
}



?>

  <table class="table table-hover table-bordered table-striped" style="font-size: 20px !important">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Item</th>
                    <th>Units</th>
                    <th>Unit Price</th>
                    <th>Total</th>
                  </tr>
                  </thead>
                  <tbody>

                    <?php


                    $order_invoice_number ='';
                    $preauth_date = date('Y-m-d');
                    $preauth_amount = '';
                    
                    $visit__rs1 = $this->pos_model->get_visit_charges_charged($pos_order_id); 
                    
                    // var_dump($pos_order_id);die();
                    // $visit_rs = $this->accounts_model->get_visit_details($visit_id);
                    $visit_type_id = 1;
                    $close_card = 3;
                    $total_amount= 0; 
                    $days = 0;
                    $count = 0;
                    $item_list = '';
                    $description_list = "";
                    $quantity_list = "";
                    $rate_list = "";
                    $amount_list = "";
                    $total= 0;  
                    $total_units= 0;
                    $number = 0;
                    $vat_charged = 0;

                    $result ='';
                    if($visit__rs1->num_rows() > 0)
                    {                       
                        foreach ($visit__rs1->result() as $key1 => $value) :
                            $v_procedure_id = $value->pos_order_item_id;
                            $procedure_id = $value->service_charge_id;
                            $product_code = $value->product_code;
                            $pos_order_item_amount = $value->pos_order_item_amount;
                            $units = $value->pos_order_item_quantity;
                            $procedure_name = $value->service_charge_name;
                            $service_id = $value->service_id;
                            $vatable = $value->vatable;
                            $product_id = $value->product_id;
                            // $order_invoice_id = $value->order_invoice_id;
                            // $visit_type_id = 1;
                            $total= $total +($units * $pos_order_item_amount);

                            if($order_invoice_id > 0)
                            {
                                $text_color = "success";
                            }
                            else
                            {
                                $text_color = 'default';
                            }

                        
                            $checked="";
                            $number++;

                            if($vatable)
                            {
                                $vat_charged += ($units * $pos_order_item_amount) * 0.16;
                            }

                            $total_units += $units;
                            $personnel_check = TRUE;
                            if($personnel_check AND $close_card == 3)
                            {
                                $checked = "<td>
                                        
                                                <a class='btn btn-xs btn-danger' href='#' onclick='delete_procedure(".$v_procedure_id.", ".$pos_order_id.")'><i class='fa fa-trash'></i></a>
                                            </td>";
                            }

                            $result .= '<tr>
                                            <td>'.$number.'</td>
                                            <td>'.$procedure_name.'</td>
                                            <td>'.$units.'</td>
                                            <td>'.number_format($pos_order_item_amount,2).'</td>
                                            <td>'.number_format($units * $pos_order_item_amount,2).'</td>
                                         </tr>';

                            endforeach;

                    }
                    else
                    {
                        
                    }
                    
                    $preauth_amount = $total;


                    $payments_value = $total_payments;
                    $balance = $preauth_amount - $total_payments;

                    ?>
                        <?php echo $result?>

                          <?php

                            if($vat_status)
                            {
                                ?>
                                <tr class="receipt_bottom_border_double" style="margin-bottom: 20px; font-weight: bold;">
                                    <td colspan="2"></td>
                                    <td colspan="2">SUBTOTAL</td>
                                    <td><?php echo number_format($preauth_amount - ($preauth_amount*0.16), 2);?></td>
                                </tr>
                                 <tr class="receipt_bottom_border_double" style="margin-bottom: 20px; font-weight: bold;">
                                    <td colspan="2"></td>
                                    <td colspan="2">VAT (16%)</td>
                                    <td><?php echo number_format(($preauth_amount*0.16), 2);?></td>
                                </tr>

                                <?php
                            }
                          ?>
                           <!--  <tr class="receipt_bottom_border"  style="margin-bottom: 20px; font-weight: bold;">
                                <td colspan="2"></td>
                                <td colspan="2">Total Paid</td>
                                <td><?php echo number_format($total_payments, 2);?></td>
                            </tr> -->
                            <tr class="receipt_bottom_border_double" style="margin-bottom: 20px; font-weight: bold;">
                                <td colspan="2"></td>
                                <td colspan="2">TOTAL BILL</td>
                                <td><?php echo number_format($preauth_amount, 2);?></td>
                            </tr>
                                
                       </tbody>

                        
                    </table>