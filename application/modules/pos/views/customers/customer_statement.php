<!-- search -->
<?php //echo $this->load->view('search_creditor_account', '', TRUE);?>
<!-- end search -->

<div class="row">
    <div class="col-md-12">

        <section class="panel">
            <header class="panel-heading">
                
                <h2 class="panel-title"><?php echo $title;?></h2>
                <a href="<?php echo site_url();?>customers" class="btn btn-sm btn-warning pull-right" style="margin-top: -25px; "><i class="fa fa-arrow-left"></i> Back to customers</a>
                <a href="<?php echo base_url().'print-customer-statement/'.$customer_id?>" class="btn btn-sm btn-success pull-right"  style="margin-top: -25px;margin-right: 5px;" target="_blank"><i class="fa fa-print"></i> Print</a>
                <button type="button" class="btn btn-sm btn-primary pull-right"  data-toggle="modal" data-target="#record_creditor_account" style="margin-top: -25px; margin-right: 5px;"><i class="fa fa-plus"></i> Record</button>
              

                	
            </header>
            
            <div class="panel-body">
              
                <div class="modal fade" id="record_creditor_account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Record Transaction</h4>
                            </div>
                            <div class="modal-body">
                                <?php echo form_open("pos/record_customer_account/".$customer_id, array("class" => "form-horizontal"));?>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Transaction date: </label>
                                    
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <input data-format="yyyy-MM-dd" type="date" data-plugin-datepicker class="form-control" name="transaction_date" placeholder="Transaction date" value="<?php echo date('Y-m-d')?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Account*</label>
                                    
                                    <div class="col-md-8">
                                         <select  class="form-control custom-select" name="account_from_id" id='account_from_id' required>
                                            <option value="">-- Select account --</option>
                                            <?php
                                            if($accounts->num_rows() > 0)
											{
												foreach($accounts->result() as $res)
												{
													$account_id = $res->account_id;
													$account_name = $res->account_name;
													?>
                                                    <option value="<?php echo $account_id;?>"><?php echo $account_name;?></option>
                                                    <?php
												}
											}
											?>
                                        </select>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Transaction Code *</label>
                                    
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="transaction_code" placeholder="Code e.g cheque number/MPESA code" autocomplete="off" required="required" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Description *</label>
                                    
                                    <div class="col-md-8">
                                        <textarea class="form-control" name="transaction_description"></textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Amount *</label>
                                    
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="amount_paid" placeholder="Amount" autocomplete="off" required="required" />
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-4">
                                        <div class="center-align">
                                            <button type="submit" class="btn btn-primary">Save record</button>
                                        </div>
                                    </div>
                                </div>
                                <?php echo form_close();?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                 <div class="modal fade" id="import_creditor_account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Import Invoices</h4>
                            </div>
                            <div class="modal-body">
                                <?php echo form_open_multipart('creditors/validate-import/'.$customer_id, array("class" => "form-horizontal", "role" => "form"));?>            
                                    <div class="row">
                                        <div class="col-md-12">
                                            <ul>
                                                <li>Download the import template <a href="<?php echo site_url().'creditors/import-template';?>">here.</a></li>
                                                
                                                <li>Save your file as a <strong>csv</strong> file before importing</li>
                                                <li>After adding your patients to the import template please import them using the button below</li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Account*</label>
                                                
                                                <div class="col-md-8">
                                                     <select  class="form-control custom-select" name="billed_account_id" id='billed_supplier_id'>
                                                        <option value="">-- Select account --</option>
                                                        <?php
                                                        if($accounts->num_rows() > 0)
                                                        {
                                                            foreach($accounts->result() as $res)
                                                            {
                                                                $account_id = $res->account_id;
                                                                $account_name = $res->account_name;
                                                                ?>
                                                                <option value="<?php echo $account_id;?>"><?php echo $account_name;?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div> 
                                            <div class="fileUpload btn btn-primary">
                                                <span>Import patients</span>
                                                <input type="file" class="upload" onChange="this.form.submit();" name="import_csv" />
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>


                
                
			<?php
			$error = $this->session->userdata('error_message');
			$success = $this->session->userdata('success_message');
			
			if(!empty($error))
			{
				echo '<div class="alert alert-danger">'.$error.'</div>';
				$this->session->unset_userdata('error_message');
			}
			
			if(!empty($success))
			{
				echo '<div class="alert alert-success">'.$success.'</div>';
				$this->session->unset_userdata('success_message');
			}
					
			$search = $this->session->userdata('creditor_payment_search');
			$search_title = $this->session->userdata('creditor_search_title');
		
			if(!empty($search))
			{
				echo '
				<a href="'.site_url().'accounting/creditors/close_creditor_search/'.$customer_id.'" class="btn btn-warning btn-sm ">Close Search</a>
				';
				echo $search_title;
			}	

				$creditor_result = $this->pos_model->get_customer_statement($customer_id,$start_date);
			?>

				<table class="table table-hover table-bordered ">
				 	<thead>
						<tr>
						  <th>#</th>	
						  <th>Transaction Date</th>						  
						  <th>Document number</th>
						  <th>Description</th>
						  <th>Debit</th>
						  <th>Credit</th>	
                          <th>Balance</th>   					
						</tr>
					 </thead>
				  	<tbody>
				  		<?php 
				  		$result ='';
				  		$total_arrears = 0;
				  		if($creditor_result->num_rows() > 0)
				  		{
				  			$x=0;

				  			foreach ($creditor_result->result() as $key => $value) {
				  				# code...
				  				$transactionDate = $value->transactionDate;
				  				$referenceCode = $value->referenceCode;
				  				$transactionCategory = $value->transactionCategory;
				  				$dr_amount = $value->dr_amount;
				  				$cr_amount = $value->cr_amount;

				  				

				  				$balance = $dr_amount - $cr_amount;

				  				$total_arrears += $balance; 

				  				$total_debits += $dr_amount;
				  				$total_credits += $cr_amount;

				  				$x++;

				  				$result .= '<tr>
				  								<td>'.$x.'</td>
				  								<td>'.$transactionDate.'</td>
				  								<td>'.strtoupper(strtolower($referenceCode)).'</td>
				  								<td>'.$transactionCategory.'</td>
				  								<td>'.number_format($dr_amount,2).'</td>
				  								<td>'.number_format($cr_amount,2).'</td>
				  								<td>'.number_format($total_arrears,2).'</td>
				  						   </tr>';
				  			}
				  			$result .= '<tr>
				  								<th></th>
				  								<th></th>
				  								<th></th>
				  								<th>Totals</th>
				  								<th>'.number_format($total_debits,2).'</th>
				  								<th>'.number_format($total_credits,2).'</th>
				  								<th>'.number_format($total_debits- $total_credits,2).'</th>
				  						   </tr>';
				  		}
				  		echo $result;
				  		?>
					</tbody>
				</table>
          	</div>
		</section>
    </div>
</div>
<script type="text/javascript">
    $(function() {
       $("#billed_account_id").customselect();
       $("#billed_supplier_id").customselect();
    });
</script>