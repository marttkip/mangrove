<div class="row" id="dashboard-disengaged" style="display: block;">
	
	<div class="col-md-12">
		<div class="col-print-2">
			<a onclick="close_account() "  class="btn btn-sm btn-danger">
            <i class="fa fa-lock" ></i>
           
            LOGOUT
        </a>
		</div>
		<div class="col-print-3">
			<div class="form-group">
		        <div class="center-align">
		       <input type="text" class="form-control" name="order_name" id="order_name" placeholder="SEARCH ORDER NUMBER HERE" onkeyup="search_name()" placeholder="" autocomplete="off"  style="padding: 15px 5px !important;font-size: 12px marigin-top: !important;text-align: center;"  onfocus="this.removeAttribute('readonly');" readonly>
		        </div>
		    </div>
		</div>
		<div class="col-print-7 pull-right">

			 <a class='btn btn-sm btn-success'    onclick="create_new_visit(0)"> <i class="fa fa-plus"></i> Add Table Order Order </a> 
			 <a class='btn btn-sm btn-warning'    onclick="create_new_visit_old(1)"> <i class="fa fa-plus"></i> Add take away sale </a>

			 <a class='btn btn-sm btn-danger'    href="<?php echo site_url().'dashboard'?>"> <i class="fa fa-dashboard"></i> Dashboard </a> 
			  <a class='btn btn-sm btn-info'    onclick="my_sales()"> <i class="fa fa-dashboard"></i> My Sales </a>   
		</div>
	</div>

	<div class="col-md-12">
		<div class="col-print-1">
				<h5 class="center-align">TABLES</h5>
				<a  class="btn btn-lg btn-info col-md-12 link-item-checked" id="link-item0" onclick="search_name(0)"> ALL </a>
				<a  class="btn btn-lg btn-primary col-md-12 link-item-checked" id="link-item1" onclick="search_name(1)"> 1 </a>
				<a  class="btn btn-lg btn-primary col-md-12 link-item-checked" id="link-item2" onclick="search_name(2)"> 2 </a>
				<a  class="btn btn-lg btn-primary col-md-12 link-item-checked" id="link-item3" onclick="search_name(3)"> 3 </a>
				<a  class="btn btn-lg btn-primary col-md-12 link-item-checked" id="link-item4" onclick="search_name(4)"> 4 </a>
				<a  class="btn btn-lg btn-primary col-md-12 link-item-checked" id="link-item5" onclick="search_name(5)"> 5 </a>
				<a  class="btn btn-lg btn-primary col-md-12 link-item-checked" id="link-item6" onclick="search_name(6)"> 6 </a>
				<a  class="btn btn-lg btn-primary col-md-12 link-item-checked" id="link-item7" onclick="search_name(7)"> 7 </a>
				<a  class="btn btn-lg btn-primary col-md-12 link-item-checked" id="link-item8" onclick="search_name(8)"> 8 </a>
				<a  class="btn btn-lg btn-primary col-md-12 link-item-checked" id="link-item9" onclick="search_name(9)"> 9 </a>
				<a  class="btn btn-lg btn-primary col-md-12 link-item-checked" id="link-item10" onclick="search_name(10)"> 10 </a>
				<a  class="btn btn-lg btn-primary col-md-12 link-item-checked" id="link-item11" onclick="search_name(11)"> 11 </a>
				<a  class="btn btn-lg btn-primary col-md-12 link-item-checked" id="link-item12" onclick="search_name(12)"> 12 </a>
				<a  class="btn btn-lg btn-primary col-md-12 link-item-checked" id="link-item13" onclick="search_name(13)"> 13 </a>
				<a  class="btn btn-lg btn-primary col-md-12 link-item-checked" id="link-item14" onclick="search_name(14)"> 14 </a>
				<?php
				$waiters_accounts = $this->pos_model->get_all_waiters();

				if($waiters_accounts->num_rows() > 0)
				{
					foreach ($waiters_accounts->result() as $key => $value) {
						// code...
						$personnel_fname = $value->personnel_fname;
						$personnel_id = $value->personnel_id;



						?>

						<a class="btn btn-lg btn-primary col-md-12 link-item-checked" id="link-item1-<?php echo $personnel_id ?>" 
   onclick="search_name(<?php echo $personnel_id ?>, 1, '<?php echo addslashes($personnel_fname) ?>')"> 
   <?php echo $personnel_fname ?> 
</a>

						<?php
					}
				}
				?>

				
		</div>
		<div class="col-print-11">
			<div id="orders-div"></div>
		</div>

		
	</div>
	
	
</div>
<div class="row" id="dashboard-engaged" style="display: none;">
	<div style="margin-bottom: 20px;">
		<div class="col-xs-12">
			<div class="col-xs-4">
				 <div class="form-group">
                    <label class="col-xs-4 control-label">Name: </label>
                    
                    <div class="col-xs-8">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Client Name" onkeyup="update_client_name()">
                    </div>
                </div>
			</div>
			<div class="col-xs-5">
				<div class="form-group">
					<label class="col-xs-4 control-label">VAT? </label>
		            <div class="col-xs-4">
		                <div class="radio">
		                    <label>
		                        <input id="optionsRadios2" type="radio" name="vat_status" onclick="update_vat_charged(0)" value="0" checked="checked" >
		                        No
		                    </label>
		                </div>
		            </div>
		            
		            <div class="col-xs-4">
		                <div class="radio">
		                    <label>
		                        <input id="optionsRadios2" type="radio" name="vat_status" onclick="update_vat_charged(1)" value="1">
		                        Yes
		                    </label>
		                </div>
		            </div>
				</div>
				
			</div>
			<div class="col-xs-3">
				 <a class='btn btn-sm btn-warning' onclick="back_to_dashboard()"> <i class="fa fa-arrow-left"></i> Back to Dashboard </a>  
			</div>
		</div>
	</div>
	<br>
	<div class="col-md-12">
		<div class="col-print-6" >
			<div class="panel-body">
				<!-- <div id="patient-statement"></div> -->

				<div class="row" id="invoice-div" style="padding:10px;">
		            <input type="text" name="search_procedures" id="search_procedures" class="form-control" onkeyup="get_procedures_done()" placeholder="Search a product,brand,category">
		                
		        </div>
		        <div class="row" style="padding:10px;">
		        	<div id="categories-list"></div>	
		        </div>
			</div>
			<div class="row" style="padding:10px;">
				<div class="panel-body" style="height: 63vh;overflow-y: scroll;">
					<div id="product-list"></div>
		    	</div>
	        		
	        </div>

		</div>
		<div class="col-print-6">
			<div id="visit-charges"></div>
			
		</div>
	</div>
</div>
<div class="row" id="dashboard-payment" style="display: none;">

	<div style="margin-bottom: 20px;">
		<div class="col-md-12">
			<div class="pull-right">
				 <a class='btn btn-sm btn-warning' onclick="back_to_dashboard()"> <i class="fa fa-arrow-left"></i> Back to Dashboard </a>  

			</div>
		</div>
	</div>
	<div class="col-print-4">
		<div id="visit-charges-closed"></div>
		
	</div>
	<div class="col-print-8" style="margin-left: 3px">
		<div class="panel-body" style="height:60vh">
			<div id="payment-div"></div>
		</div>
	</div>
</div>


  <!-- END OF ROW -->
<script type="text/javascript">


   $(function() {
       $("#service_id_item").customselect();
       $("#provider_id_item").customselect();
       $("#parent_service_id").customselect();

   });
   $(document).ready(function(){
   		// display_patient_bill(<?php echo $visit_id;?>);
   		// display_procedure(<?php echo $visit_id;?>);
   		// get_patient_statement();

   		// get_visit_procedures_done();
   		// get_parent_categories();
   		// get_product_list();
   		// get_patient_incomplete_invoices(<?php echo $patient_id;?>);

   		window.localStorage.setItem('visit_id',null);
			window.localStorage.setItem('order_invoice_id',null);
   		get_days_orders();
   	

   });

   function get_order_items_list()
   {
   		var myTarget2 = document.getElementById("dashboard-disengaged");
        myTarget2.style.display = 'none';


        var order_invoice_id = window.localStorage.getItem('order_invoice_id');
        var pos_order_id = window.localStorage.getItem('pos_order_id');

        // alert(order_invoice_id);
        if(order_invoice_id > 0)
        {
        	var myTarget5 = document.getElementById("dashboard-engaged");
	        myTarget5.style.display = 'none';

	        var myTarget6 = document.getElementById("dashboard-payment");
	    	myTarget6.style.display = 'block';
	    	get_payment_div(pos_order_id,order_invoice_id);
        }
        else
        {
        	var myTarget5 = document.getElementById("dashboard-engaged");
	        myTarget5.style.display = 'block';

	        var myTarget6 = document.getElementById("dashboard-payment");
	    	myTarget6.style.display = 'none';

	    	get_parent_categories();
   			get_product_list();
        }

   		get_visit_procedures_done(pos_order_id);
   		
   }

  function getservices(id){

        var myTarget1 = document.getElementById("service_div");
        var myTarget2 = document.getElementById("username_div");
        var myTarget3 = document.getElementById("password_div");
        var myTarget4 = document.getElementById("service_div2");
        var myTarget5 = document.getElementById("payment_method");

        if(id == 1)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'block';
          myTarget5.style.display = 'block';
        }
        else
        {
          myTarget1.style.display = 'block';
          myTarget2.style.display = 'block';
          myTarget3.style.display = 'block';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }

  }
  function check_payment_type(payment_type_id){


    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check

      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 3)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }
    else if(payment_type_id == 4)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
    }
    else
    {
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }

  }

   function display_patient_bill(visit_id){

      var XMLHttpRequestObject = false;

      if (window.XMLHttpRequest) {

          XMLHttpRequestObject = new XMLHttpRequest();
      }

      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }

      var config_url = document.getElementById("config_url").value;
      var close_page = document.getElementById("close_page").value;
      var url = config_url+"pos/view_patient_bill/"+visit_id+"/1";
      // alert(url);
      if(XMLHttpRequestObject) {

          XMLHttpRequestObject.open("GET", url);

          XMLHttpRequestObject.onreadystatechange = function(){

              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patient_bill").innerHTML=XMLHttpRequestObject.responseText;
              }
          }

          XMLHttpRequestObject.send(null);
      }


  }

	//Calculate procedure total
	function calculatetotal(amount, id, procedure_id, pos_order_id){

	    var units = document.getElementById('units'+id).value;
	    var billed_amount = document.getElementById('billed_amount'+id).value;

	    grand_total(id, units, billed_amount, pos_order_id);

	}

  function grand_total(procedure_id, units, amount, pos_order_id,status = null)
  {
  	// alert(units);

		 var config_url = document.getElementById("config_url").value;
		 var product_id = document.getElementById('product_id'+procedure_id).value;
	     var data_url = config_url+"pos/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+pos_order_id+"/"+status;

	      // var tooth = document.getElementById('tooth'+procedure_id).value;
	     // alert(data_url);
	    $.ajax({
	    type:'POST',
	    url: data_url,
	    data:{procedure_id: procedure_id,product_id: product_id},
	    dataType: 'text',
	    success:function(data){
	     // get_medication(visit_id);
	         // display_patient_bill(v_id);
	        get_visit_procedures_done(pos_order_id);


			



	         // alert('You have successfully updated your billing');
	    //obj.innerHTML = XMLHttpRequestObject.responseText;
	    },
	    error: function(xhr, status, error) {
	    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        get_visit_procedures_done(pos_order_id);


	    	alert(error);
	    }

	    });

	    
	}


	function delete_service(id, visit_id){

		var res = confirm('Do you want to remove this charge ? ');

		if(res)
		{
			var XMLHttpRequestObject = false;

		    if (window.XMLHttpRequest) {

		        XMLHttpRequestObject = new XMLHttpRequest();
		    }

		    else if (window.ActiveXObject) {
		        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		    }
		     var config_url = document.getElementById("config_url").value;
		    var url = config_url+"pos/delete_service_billed/"+id;

		    if(XMLHttpRequestObject) {

		        XMLHttpRequestObject.open("GET", url);

		        XMLHttpRequestObject.onreadystatechange = function(){

		            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

		                // display_patient_bill(visit_id);
		                display_procedure(visit_id);
		            }
		        }

		        XMLHttpRequestObject.send(null);
		    }
		}

	}
	function save_service_items(visit_id)
	{
		var provider_id = $('#provider_id'+visit_id).val();
		var service_id = $('#service_id'+visit_id).val();
		var visit_date = $('#visit_date_date'+visit_id).val();
		var url = "<?php echo base_url();?>pos/add_patient_bill/"+visit_id;

		$.ajax({
		type:'POST',
		url: url,
		data:{provider_id: provider_id, service_charge_id: service_id, visit_date: visit_date},
		dataType: 'text',
		success:function(data){
			alert("You have successfully billed");
			display_patient_bill(visit_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		return false;
	}



	function parse_procedures(visit_id,suck)
    {
      var procedure_id = document.getElementById("service_id_item").value;
       procedures(procedure_id, visit_id, suck);

    }

	function procedures(id, v_id, suck){

        var XMLHttpRequestObject = false;

        if (window.XMLHttpRequest) {

            XMLHttpRequestObject = new XMLHttpRequest();
        }

        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }

        var url = "<?php echo site_url();?>pos/accounts_update_bill/"+id+"/"+v_id+"/"+suck;

         if(XMLHttpRequestObject) {

            XMLHttpRequestObject.open("GET", url);

            XMLHttpRequestObject.onreadystatechange = function(){

                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                    // document.getElementById("procedures").innerHTML=XMLHttpRequestObject.responseText;
                    display_patient_bill(v_id);
                }
            }

            XMLHttpRequestObject.send(null);
        }

    }
    function display_procedure(visit_id){

	    var XMLHttpRequestObject = false;

	    if (window.XMLHttpRequest) {

	        XMLHttpRequestObject = new XMLHttpRequest();
	    }

	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }

	    var config_url = document.getElementById("config_url").value;
	    // var url = config_url+"nurse/view_procedure/"+visit_id;
	    var url = config_url+"pos/view_procedure/"+visit_id;

	    if(XMLHttpRequestObject) {

	        XMLHttpRequestObject.open("GET", url);

	        XMLHttpRequestObject.onreadystatechange = function(){

	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                document.getElementById("procedures").innerHTML=XMLHttpRequestObject.responseText;
	            }
	        }

	        XMLHttpRequestObject.send(null);
	    }
	}
	function delete_procedure(id, visit_id){
	    var XMLHttpRequestObject = false;

	    if (window.XMLHttpRequest) {

	        XMLHttpRequestObject = new XMLHttpRequest();
	    }

	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	     var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/delete_procedure/"+id;

	    if(XMLHttpRequestObject) {

	        XMLHttpRequestObject.open("GET", url);

	        XMLHttpRequestObject.onreadystatechange = function(){

	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {


	            }
	        }

	        XMLHttpRequestObject.send(null);
	    }
      display_patient_bill(visit_id);
	}

	$(document).on("change","select#visit_type_id",function(e)
	{
		var visit_type_id = $(this).val();

		if(visit_type_id != '1')
		{
			$('#insured_company').css('display', 'block');
			// $('#consultation').css('display', 'block');
		}
		else
		{
			$('#insured_company').css('display', 'none');
			// $('#consultation').css('display', 'block');
		}




	});

	function change_payer(visit_charge_id, service_charge_id, v_id)
	{

		var res = confirm('Do you want to change who is being billed ? ');

		if(res)
		{

			var config_url = document.getElementById("config_url").value;
		    var data_url = config_url+"pos/change_payer/"+visit_charge_id+"/"+service_charge_id+"/"+v_id;
		   
		      // var tooth = document.getElementById('tooth'+procedure_id).value;
		     // alert(data_url);
		    $.ajax({
		    type:'POST',
		    url: data_url,
		    data:{visit_charge_id: visit_charge_id},
		    dataType: 'text',
		    success:function(data){
		     // get_medication(visit_id);
		         display_patient_bill(v_id);
		     alert('You have successfully updated your billing');
		    //obj.innerHTML = XMLHttpRequestObject.responseText;
		    },
		    error: function(xhr, status, error) {
		    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		        display_patient_bill(v_id);
		    	alert(error);
		    }

		    });

		}

	}


	function get_patient_statement()
	{

		var config_url = $('#config_url').val();
	 	var url = config_url+"pos/get_patient_statement";
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#patient-statement").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

	}

	function close_side_bar()
	{
		// $('html').removeClass('sidebar-right-opened');
		document.getElementById("sidebar-right").style.display = "none"; 
		document.getElementById("current-sidebar-div").style.display = "none"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 
		tinymce.remove();
	}

	function add_invoice()
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/get_visit_invoices";
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);
			$("#visit-invoice-div").html(data);
			get_visit_procedures_done();
			// get_visit_charges(visit_id,patient_id);
			// tinymce.init({
			//                 selector: ".cleditor",
			//                	height: "200"
			// 	            });
			// alert(data);
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
	}


	function get_visit_invoices(patient_id)
	{
		var myTarget = document.getElementById("visit_id").value;
		var visit_id = myTarget;

		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/get_visit_invoices/"+visit_id+"/"+patient_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			document.getElementById("visit-invoice-div").style.display = "block"; 
			$("#visit-invoice-div").html(data);
			get_visit_charges(visit_id,patient_id);
			tinymce.init({
			                selector: ".cleditor",
			               	height: "200"
				            });
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

	function get_procedures_done(category_id = null)
	{
		var config_url = $('#config_url').val();
		// var visit_type_id = document.getElementById("visit_type_id").value;

		var visit_id = window.localStorage.getItem('visit_id');
		var patient_id = window.localStorage.getItem('patient_id');
		var data_url = config_url+"pos/get_product_list";
		// window.alert(data_url);
		$('#charges-div').css('display', 'block');
		var lab_test = $('#search_procedures').val();
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : lab_test,category_id : category_id},
		dataType: 'text',
		success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#product-list").html(data);
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}
	function get_visit_charges(visit_id,patient_id,order_invoice_id = null)
	{
		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/get_visit_charges/"+visit_id+"/"+patient_id+"/"+order_invoice_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
			$("#visit-charges").html(data);
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}


	function add_payment(patient_id,order_invoice_id)
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/add_payment/"+patient_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			get_visit_payments(patient_id,null,order_invoice_id);
			// alert(data);

			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
		
	}


	function get_visit_payments(patient_id,payment_id=null,order_invoice_id=null)
	{
		// var myTarget = document.getElementById("visit_id").value;
		// var visit_id = myTarget;

		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/get_visit_payments/"+patient_id+"/"+payment_id+"/"+order_invoice_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			// document.getElementById("visit-payments-div").style.display = "block"; 
			$("#visit-payments-div").html(data);
			
			$('.datepicker').datepicker({
					    format: 'yyyy-mm-dd'
					});



			$('.timepicker').timepicker({
			    timeFormat: 'h:mm p',
			    interval: 60,
			    minTime: '10',
			    maxTime: '6:00pm',
			    defaultTime: '11',
			    startTime: '10:00',
			    dynamic: false,
			    dropdown: true,
			    scrollbar: true
			});
		
			
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

	function add_payment_item(patient_id,payment_id= null)
	{
		var config_url = $('#config_url').val();

		 var amount_paid = document.getElementById("amount_paid").value;
		 var invoice_id = document.getElementById("invoice_id").value;

		 // window.alert(amount_paid);
		var data_url = config_url+"pos/add_payment_item/"+patient_id+"/"+payment_id;
		
		// window.alert(invoice_id);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{invoice_detail: invoice_id,amount: amount_paid},
		dataType: 'text',
		success:function(data){
		
			get_visit_payments(patient_id,payment_id);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}


	$(document).on("submit","form#confirm-payment",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);
		// var visit_id = $('#charge_visit_id').val();
		var patient_id = $('#payment_patient_id').val();
		var payment_id = $('#payment_payment_id').val();
		var config_url = $('#config_url').val();	

		var url = config_url+"pos/confirm_payment/"+patient_id+"/"+payment_id;
		// alert(url);
		
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:form_data,
	   dataType: 'text',
	   processData: false,
	   contentType: false,
	   success:function(data){
	      var data = jQuery.parseJSON(data);
	    
	      	if(data.message == "success")
			{
				
				
				close_side_bar();
				get_patient_statement(patient_id);
				
			}
			else
			{
				alert(data.result);
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
		 
		
	   
		
	});
	function get_patient_incomplete_invoices(patient_id)
	{

		var config_url = $('#config_url').val();
	 	var url = config_url+"pos/get_incomplete_invoices/"+patient_id;
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#incomplete-invoices").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

	}



$(document).on("submit","form#confirm-invoice",function(e)
{
	e.preventDefault();
	
	var form_data = new FormData(this);

	// alert(form_data);
	var amount = $('#amount').val();
	var tendered_amount = $('#tendered_amount').val();
	var visit_id = $('#charge_visit_id').val();
	// var patient_id = $('#charge_patient_id').val();
	// var order_invoice_id = $('#order_invoice_id').val();
	var pos_order_id = window.localStorage.getItem('pos_order_id');
	// var patient_id = window.localStorage.getItem('patient_id');
	// var order_invoice_id = window.localStorage.getItem('order_invoice_id');
	// alert(pos_order_id);


	var sale_type = $("input[name='sale_type']:checked").val();

	var proceed = false;
	if(sale_type == 0)
	{
		if(tendered_amount >= amount)
		{
			proceed = true;
		}
		else
		{
			alert('Sorry please check on the amount entered ');
			proceed = false;
		}

	}
	else
	{
		proceed = true;

	}
         
	if(proceed == true)
	{


		var config_url = $('#config_url').val();	

		var url = config_url+"pos/confirm_visit_charge";
		 
		 
	   $.ajax({
		   type:'POST',
		   url: url,
		   data:form_data,
		   dataType: 'text',
		   processData: false,
		   contentType: false,
		   success:function(data){
		      var data = jQuery.parseJSON(data);
		    
		      	if(data.message == "success")
				{
					var order_invoice_id = data.order_invoice_id;
					
					window.localStorage.setItem('order_invoice_id',order_invoice_id);

					var myTarget2 = document.getElementById("dashboard-disengaged");
				    myTarget2.style.display = 'none';


					var myTarget5 = document.getElementById("dashboard-engaged");
				    myTarget5.style.display = 'none';

				    var myTarget6 = document.getElementById("dashboard-payment");
				    myTarget6.style.display = 'block';
					
					get_visit_procedures_done(pos_order_id,order_invoice_id);

					get_payment_div(pos_order_id,order_invoice_id);
					close_side_bar();
					// get_patient_incomplete_invoices(patient_id);
					// get_patient_statement();
					
				}
				else
				{
					alert(data.result);
				}
		   
		   },
		   error: function(xhr, status, error) {
		   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		   
		   }
		   });
	
	
	 }
	
   
	
});

function invoice_details_view(order_invoice_id,visit_id,patient_id)
{

	document.getElementById("sidebar-right").style.display = "block"; 
	document.getElementById("existing-sidebar-div").style.display = "none"; 

	var config_url = $('#config_url').val();
	var data_url = config_url+"pos/get_visit_invoices/"+visit_id+"/"+patient_id+"/"+order_invoice_id;
	//window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{appointment_id: 1},
	dataType: 'text',
	success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
		document.getElementById("current-sidebar-div").style.display = "block"; 
		$("#current-sidebar-div").html(data);
		$("#visit-invoice-div").html(data);
		get_visit_charges(visit_id,patient_id,order_invoice_id);
		tinymce.init({
		                selector: ".cleditor",
		               	height: "200"
			            });
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
	}

	});

}

function add_service_charge_test(service_charge_id,service_charge_amount,order_invoice_id =null)
{
	// var res = confirm('Are you sure you want to charge ?');

	// if(res)
	// {	
		
		var pos_order_id = window.localStorage.getItem('pos_order_id');
		// var patient_id = window.localStorage.getItem('patient_id');
		var visit_type_id=1;

		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/add_visit_charge/"+service_charge_id+"/"+pos_order_id+"/"+visit_type_id+"/"+order_invoice_id;
		// window.alert(product_id);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{service_charge_id: service_charge_id,visit_type_id: visit_type_id,visit_invoice: order_invoice_id, service_charge_amount: service_charge_amount},
		dataType: 'text',
		success:function(data){

			var data = jQuery.parseJSON(data);

			if(data.message == "success")
			{
				document.getElementById('search_procedures').value = '';
				$('#charges-div').css('display', 'none');
				get_visit_procedures_done(pos_order_id);

			}
			else
			{
				alert(data.result);
			}
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	// }

	


}
	
function get_visit_procedures_done(pos_order_id,order_invoice_id = null)
{
	var config_url = $('#config_url').val();

	var order_invoice_id = window.localStorage.getItem('order_invoice_id');


	if(order_invoice_id > 0)
	{
		var data_url = config_url+"pos/visit_procedures_view/"+pos_order_id+"/"+order_invoice_id;
	}
	else
	{
		var data_url = config_url+"pos/visit_procedures_view/"+pos_order_id;
	}
	
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : null},
	dataType: 'text',
	success:function(data){
	//window.alert("You have successfully updated the symptoms");
	//obj.innerHTML = XMLHttpRequestObject.responseText;
		if(order_invoice_id > 0)
		{
			$("#visit-charges-closed").html(data);
		}else
		{
			$("#visit-charges").html(data);
		}
	// alert(data);
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});

	// get_totals_div();
}





// pos functions 

function get_parent_categories()
{
	var config_url = $('#config_url').val();
	var data_url = config_url+"pos/get_parent_categories";
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : null},
	dataType: 'text',
	success:function(data){
	//window.alert("You have successfully updated the symptoms");
	//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#categories-list").html(data);
	// alert(data);
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});
}

function get_product_list()
{
	var config_url = $('#config_url').val();
	var data_url = config_url+"pos/get_product_list";
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : null},
	dataType: 'text',
	success:function(data){
	//window.alert("You have successfully updated the symptoms");
	//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#product-list").html(data);
	// alert(data);
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});
}

function get_totals_div(visit_id,patient_id)
{
	var config_url = $('#config_url').val();
	var data_url = config_url+"pos/get_totals_div/"+visit_id+"/"+patient_id;
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : null},
	dataType: 'text',
	success:function(data){
	//window.alert("You have successfully updated the symptoms");
	//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#totals-div").html(data);
	// alert(data);
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});
}

function create_new_visit(sale_type)
{

	document.getElementById("sidebar-right").style.display = "block"; 
	document.getElementById("existing-sidebar-div").style.display = "none"; 

	var pos_order_id = window.localStorage.getItem('pos_order_id');

	var config_url = $('#config_url').val();

	var data_url = config_url+"pos/open_sale/"+sale_type;
	//window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{appointment_id: 1},
	dataType: 'text',
	success:function(data){
			var data = jQuery.parseJSON(data);
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data.result);
			// $("#payment-div").html(data.result);
			
				
				document.getElementById("payment-complete-off").style.display = "none"; 
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

	});

}
function create_new_visit_old(sale_type)
{

		if(sale_type == 1)
		{
			var sale = 'Take away ';
		}
		else if(sale_type == 0)
		{
			var sale = 'Cash ';
		}
		else if(sale_type == 3)
		{
			var sale = 'Quotation ';
		}
		var res = confirm('Are you sure you want to start a new '+sale+' sale ? ');

		if(res)
		{


			var config_url = $('#config_url').val();
			var data_url = config_url+"pos/create_new_visit/"+sale_type;
			// window.alert(data_url);
			$.ajax({
			type:'POST',
			url: data_url,
			data:{query : null},
			dataType: 'text',
			success:function(data){
				var data = jQuery.parseJSON(data);

				if(data.status == "success")
				{
					// var patient_id = data.patient_id;
					var pos_order_id = data.pos_order_id;
					window.localStorage.setItem('pos_order_id',pos_order_id);
					// window.localStorage.setItem('patient_id',patient_id);
					get_order_items_list();
				}
				$("#totals-div").html(data);
			// alert(data);
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
			}

			});
		}
}

function back_to_dashboard()
{
	var myTarget2 = document.getElementById("dashboard-disengaged");
    myTarget2.style.display = 'block';


	var myTarget5 = document.getElementById("dashboard-engaged");
    myTarget5.style.display = 'none';

    var myTarget6 = document.getElementById("dashboard-payment");
    myTarget6.style.display = 'none';

    window.localStorage.setItem('visit_id',null);
	window.localStorage.setItem('patient_id',null);
	window.localStorage.setItem('pos_order_item_id',null);
	window.localStorage.setItem('order_invoice_id',null);
    get_days_orders();	
}

function get_days_orders()
{

	var config_url = $('#config_url').val();
	var data_url = config_url+"pos/get_todays_orders";
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : null},
	dataType: 'text',
	success:function(data){
	//window.alert("You have successfully updated the symptoms");
	//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#orders-div").html(data);
		window.localStorage.setItem('pos_order_item_id',null);
	// alert(data);
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});

	// get_sales_breakdown();
}

function get_order_detail(pos_order_id,order_invoice_id=null)
{

	// alert(visit_id);
	// alert(patient_id);

	var config_url = $('#config_url').val();

	var data_url = config_url+"pos/get_order_details/"+pos_order_id;
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{name: name},
	dataType: 'text',
	success:function(data){

		var data = jQuery.parseJSON(data);
			document.getElementById("name").value = data.name;
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
	}

	});

	window.localStorage.setItem('pos_order_id',pos_order_id);
	// window.localStorage.setItem('patient_id',patient_id);
	// alert(order_invoice_id);
	if(order_invoice_id > 0)
	{
		window.localStorage.setItem('order_invoice_id',order_invoice_id);
	}
	else
	{
		window.localStorage.setItem('order_invoice_id','');
	}


	get_order_items_list();	
}

function get_change(total_amount)
{ 

	var cash_tendered_amount = parseInt(document.getElementById('tendered_amount').value);
	var mpesa_tendered_amount = parseInt(document.getElementById('mpesa_tendered_amount').value);
	var debit_tendered_amount = parseInt(document.getElementById('debit_tendered_amount').value);

	var config_url = $('#config_url').val();
	var data_url = config_url+"pos/get_balance";
	// window.alert(data_url);


	$.ajax({
	type:'POST',
	url: data_url,
	data:{cash_tendered_amount : cash_tendered_amount,mpesa_tendered_amount : mpesa_tendered_amount, debit_tendered_amount: debit_tendered_amount,total_amount: total_amount},
	dataType: 'text',
	success:function(data){

		var data = jQuery.parseJSON(data);

		var total_amount_paid = data.total_paid_amount;
		// alert(total_amount_paid);

		// var buttons = document.querySelectorAll(".sale_finalize");

	 var buttons = document.getElementsByClassName("sale_finalize");
    console.log(buttons); // Check if buttons are selected correctly

    for (var i = 0; i < buttons.length; i++) {
        if (total_amount > total_amount_paid) {
            buttons[i].disabled = true; // Disable the button
        } else {
            buttons[i].disabled = false; // Enable the button
        }
        console.log("Button " + i + " disabled: " + buttons[i].disabled+" "+total_amount+" "+total_amount_paid); // Check button state
    }
		
		document.getElementById("total_paid_amount").value = data.total_paid_amount;
		document.getElementById("change").value = data.balance;
		var data = '<h2 class="center-align">Ksh.'+data.balance+' </h2>';
		$("#balance-div").html(data);
	// alert(data);
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});
	
}
function get_payment_div(pos_order_id,order_invoice_id)
{
	var config_url = $('#config_url').val();
	var data_url = config_url+"pos/get_payment_div/"+pos_order_id+"/"+order_invoice_id;
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : null},
	dataType: 'text',
	success:function(data){
		var data = jQuery.parseJSON(data);

		if(data.status == "success")
		{
			
		}
		$("#payment-div").html(data.result);
	// alert(data);

	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});
	// get_visit_payments(patient_id,null,order_invoice_id);

}

function proceed_to_complete_sale()
{

	document.getElementById("sidebar-right").style.display = "block"; 
	document.getElementById("existing-sidebar-div").style.display = "none"; 

	var pos_order_id = window.localStorage.getItem('pos_order_id');

	var config_url = $('#config_url').val();

	var data_url = config_url+"pos/get_complete_view/"+pos_order_id;
	//window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{appointment_id: 1},
	dataType: 'text',
	success:function(data){
		var data = jQuery.parseJSON(data);
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
		document.getElementById("current-sidebar-div").style.display = "block"; 
		$("#current-sidebar-div").html(data.result);
		// $("#payment-div").html(data.result);
		var sale_type = data.sale_type;


		// if(sale_type == 1)
		// {
		// 	document.getElementById("credit-sale-div").style.display = "block"; 
		// }
		// else 
		// {
			document.getElementById("cash-sale-div").style.display = "block"; 
		// }
			
			document.getElementById("payment-complete-off").style.display = "none"; 
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
	}

	});

}

function get_invoice_type(pos_order_id,id)
{

        var myTarget1 = document.getElementById("cash-sale-div");
        var myTarget2 = document.getElementById("credit-sale-div");
        var myTarget3 = document.getElementById("new-customer-div");
        var myTarget4 = document.getElementById("searched-div");
        var myTarget5 = document.getElementById("quote-div");
        if(id == 0)
        {
          myTarget1.style.display = 'block';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }
        else if(id == 1)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'block';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }
        else if(id == 3)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'block';
        }

        if(id <= 1)
        {
        	 var config_url = $('#config_url').val();
	  	 
			  var data_url = config_url+"pos/update_sale_type/"+pos_order_id+"/"+id;
			  
			  $.ajax({
			  type:'POST',
			  url: data_url,
			  data:{pos_order_id: pos_order_id},
			  dataType: 'text',
			  success:function(data){
			  	var data = jQuery.parseJSON(data);

			  	if(data.message == "success")
			  	{
			  		
			  	}
			  	else
			  	{

			  	}
			 	
			   		
			    // alert(data);
			  },
			  error: function(xhr, status, error) {
			  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			  alert(error);
			  }

			  });
        }

  }

  function search_customers()
  {
  	  var config_url = $('#config_url').val();
  	  var pos_order_id = window.localStorage.getItem('pos_order_id');
	  var data_url = config_url+"pos/search_customer/"+pos_order_id;
	  //window.alert(data_url);
	  var lab_test = $('#q').val();
	  $.ajax({
	  type:'POST',
	  url: data_url,
	  data:{pos_order_id: pos_order_id, query : lab_test},
	  dataType: 'text',
	  success:function(data){
	  //window.alert("You have successfully updated the symptoms");
	  //obj.innerHTML = XMLHttpRequestObject.responseText;
	   $("#searched-customers").html(data);
	    // alert(data);
	  },
	  error: function(xhr, status, error) {
	  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	  alert(error);
	  }

	  });
  }

  	function add_new_customer()
	{

	        var myTarget1 = document.getElementById("cash-sale-div");
	        var myTarget2 = document.getElementById("credit-sale-div");
	        var myTarget3 = document.getElementById("new-customer-div");
	        var myTarget4 = document.getElementById("searched-div");
	        var myTarget5 = document.getElementById("quote-div");
	       
			myTarget1.style.display = 'none';
			myTarget2.style.display = 'none';
			myTarget3.style.display = 'block';
			myTarget4.style.display = 'none';
			myTarget5.style.display = 'none';
	        

	 }

	$(document).on("submit","form#add-customer",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		
		var config_url = $('#config_url').val();	

		var url = config_url+"pos/add_customer";
		 var pos_order_id = window.localStorage.getItem('pos_order_id');
		 
	   $.ajax({
		   type:'POST',
		   url: url,
		   data:form_data,
		   dataType: 'text',
		   processData: false,
		   contentType: false,
		   success:function(data){
		      var data = jQuery.parseJSON(data);
		    
		      	if(data.message == "success")
				{
					get_invoice_type(pos_order_id,1);
					document.getElementById("add-customer").reset(); 
					
				}
				else
				{
					alert(data.result);
				}
		   
		   },
		   error: function(xhr, status, error) {
		   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		   
		   }
		   });
		
		
	});
	function add_customer_to_order(customer_id,pos_order_id)
	{

		  var config_url = $('#config_url').val();
	  	 
		  var data_url = config_url+"pos/add_customer_to_order/"+pos_order_id+"/"+customer_id;
		  
		  $.ajax({
		  type:'POST',
		  url: data_url,
		  data:{pos_order_id: pos_order_id, customer_id : customer_id},
		  dataType: 'text',
		  success:function(data){
		  	var data = jQuery.parseJSON(data);

		  	if(data.message == "success")
		  	{
		  		var myTarget4 = document.getElementById("searched-div");
		  		myTarget4.style.display = 'block';
		  		$('#q').val('');
		  		$("#sale-detail").html(data.customer_name);
		  	}
		  	else
		  	{

		  	}
		 	
		   		
		    // alert(data);
		  },
		  error: function(xhr, status, error) {
		  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		  alert(error);
		  }

		  });

	}
	function clear_order_invoice(pos_order_id)
	{
		var res = confirm('Are you sure you want to remove the customer from the order ? ');

		if(res)
		{

			 var config_url = $('#config_url').val();
	  	 
			  var data_url = config_url+"pos/remove_customer_from_order/"+pos_order_id;
			  
			  $.ajax({
			  type:'POST',
			  url: data_url,
			  data:{pos_order_id: pos_order_id},
			  dataType: 'text',
			  success:function(data){
			  	var data = jQuery.parseJSON(data);

			  	if(data.message == "success")
			  	{
			  		var myTarget4 = document.getElementById("searched-div");
			  		myTarget4.style.display = 'none';
			  	
			  		// $("#sale-detail").html(data.customer_name);
			  	}
			  	else
			  	{

			  	}
			 	
			   		
			    // alert(data);
			  },
			  error: function(xhr, status, error) {
			  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			  alert(error);
			  }

			  });
		}
	}
	function login_to_sale(pos_order_id,sale_type)
	{

		var username = $('#username').val();
		var password = $('#password').val();
		var config_url = $('#config_url').val();	

		var url = config_url+"pos/confirm_user/"+pos_order_id;
		 
		 
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:{username: username, password: password},
	   dataType: 'text',
	   success:function(data){
	      var data = jQuery.parseJSON(data);

	      // alert(data.result_items);
	    
	      	if(data.message == "success")
			{
				
				window.localStorage.setItem('user_id',data.user_id);
				// alert(sale_type);
				if(sale_type == 1)
				{
					var myTarget1 = document.getElementById("cash-sale-div");
			        var myTarget2 = document.getElementById("credit-sale-div");
			        var myTarget3 = document.getElementById("quote-div");
			       
					myTarget1.style.display = 'none';
					myTarget2.style.display = 'block';
					myTarget3.style.display = 'none';

				}
				else if(sale_type == 0)
				{
					var myTarget1 = document.getElementById("cash-sale-div");
			        var myTarget2 = document.getElementById("credit-sale-div");
			        var myTarget3 = document.getElementById("quote-div");
			       
					myTarget1.style.display = 'block';
					myTarget2.style.display = 'none';
					myTarget3.style.display = 'none';
				}
				else if(sale_type == 3)
				{
					var myTarget1 = document.getElementById("cash-sale-div");
			        var myTarget2 = document.getElementById("credit-sale-div");
			        var myTarget3 = document.getElementById("quote-div");
			       
					myTarget1.style.display = 'none';
					myTarget2.style.display = 'none';
					myTarget3.style.display = 'block';
				}
				document.getElementById("username-div").style.display = "none"; 
				document.getElementById("payment-complete-off").style.display = "block"; 
			}
			else
			{
				document.getElementById("username-div").style.display = "block"; 
				document.getElementById("payment-complete-off").style.display = "none"; 
				alert(data.result_items);
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });

	}
	function set_procedure_status(pos_order_item_id,service_charge_id)
	{
		// alert();
		var pos_order_id = window.localStorage.getItem('pos_order_id');
	
		window.localStorage.setItem('pos_order_item_id',pos_order_item_id);
		window.localStorage.setItem('service_charge_id',service_charge_id);
		// document.getElementById("procedure_id").style.display = "color: grey"; 
	


		var config_url = $('#config_url').val();

		var data_url = config_url+"pos/check_status/"+pos_order_item_id;

		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){

			var data = jQuery.parseJSON(data);
			// alert(data);
				if(data == true)
				{
						// get_visit_procedures_done(pos_order_id);
						$( "#procedure_id"+pos_order_item_id).addClass( "success" );

						$('.down-arrow').css('display', 'block');
						// $('.down-arrow').style.display = "block"; 
						// $('.btn-delete').style.display = "block"; 
						$('.btn-delete').css('display', 'block');
				}

					
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});


	}

	function add_items(pos_order_id)
	{
		
		var id = window.localStorage.getItem('pos_order_item_id');
		var procedure_id = window.localStorage.getItem('service_charge_id');
	    var units = document.getElementById('units'+id).value;

	    // alert(units);
	    var billed_amount = document.getElementById('billed_amount'+id).value;
	    var pos_order_id = window.localStorage.getItem('pos_order_id');
	    
	    grand_total(id, units, billed_amount, pos_order_id,1);

	    var pos_order_item_id = window.localStorage.getItem('pos_order_item_id');

		
		if(pos_order_item_id > 0)
		{
			// alert(pos_order_item_id);
			$( "#procedure_id"+pos_order_item_id).addClass( "success" );
		}

		


	}

	function reduce_items(pos_order_id)
	{
		
		var id = window.localStorage.getItem('pos_order_item_id');
		var procedure_id = window.localStorage.getItem('service_charge_id');
	    var units = document.getElementById('units'+id).value;
	    var billed_amount = document.getElementById('billed_amount'+id).value;
	    var pos_order_id = window.localStorage.getItem('pos_order_id');

	    grand_total(id, units, billed_amount, pos_order_id,2);

	    var pos_order_item_id = window.localStorage.getItem('pos_order_item_id');

		// alert(pos_order_item_id);
		if(pos_order_item_id > 0)
		{
			$( "#procedure_id"+pos_order_item_id).addClass( "success" );
		}




	}


	function post_bill(print_status=0)
	{


		var pos_order_id = window.localStorage.getItem('pos_order_id');

		var config_url = $('#config_url').val();

		var data_url = config_url+"pos/post_bill/"+pos_order_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){

			var data = jQuery.parseJSON(data);
			
				if(print_status == 1)
				{
					print_orders_department(pos_order_id,0);
				}
				
				back_to_dashboard();
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});

	}

	function remove_items()
	{
		var pos_order_item_id = window.localStorage.getItem('pos_order_item_id');
	    var pos_order_id = window.localStorage.getItem('pos_order_id');


		var config_url = $('#config_url').val();

		var data_url = config_url+"pos/remove_item/"+pos_order_id+"/"+pos_order_item_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){

			var data = jQuery.parseJSON(data);
				 get_visit_procedures_done(pos_order_id);
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});


	}

	function open_sale(sale_type)
	{
		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 


		var config_url = $('#config_url').val();

		var data_url = config_url+"pos/open_sale/"+sale_type;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			var data = jQuery.parseJSON(data);
			
			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data.result);



		}

		});
	}

	$(document).on("submit","form#create-order",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);
		
		var sale_type = $('#sale_type').val();

		


		var config_url = $('#config_url').val();	

		var data_url = config_url+"pos/create_new_visit/"+sale_type;
			 
			 
	   $.ajax({
		   type:'POST',
		   url: data_url,
		   data:form_data,
		   dataType: 'text',
		   processData: false,
		   contentType: false,
		   success:function(data){
		      var data = jQuery.parseJSON(data);
		    
		      	if(data.status == "success")
				{

					close_side_bar();
					
					// var patient_id = data.patient_id;
					var pos_order_id = data.pos_order_id;
					window.localStorage.setItem('pos_order_id',pos_order_id);
					// window.localStorage.setItem('patient_id',patient_id);
					get_order_items_list();

				}
				else
				{
					alert(data.result);
				}
		   
		   },
		   error: function(xhr, status, error) {
		   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		   
		   }
		   });
		
		
		 
	

		
	});

	function verify_bill(pos_order_id)
	{
		var res = confirm('Are you sure you want to verify this bill ? ');

		if(res)
		{

			var pos_order_id = window.localStorage.getItem('pos_order_id');


			var config_url = $('#config_url').val();

			var data_url = config_url+"pos/verify_order/"+pos_order_id;
			//window.alert(data_url);
			$.ajax({
			type:'POST',
			url: data_url,
			data:{appointment_id: 1},
			dataType: 'text',
			success:function(data){

				var data = jQuery.parseJSON(data);
					back_to_dashboard();
					get_days_orders();	
				},
				error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

			});

		}
	}

	function get_table_side(position)
	{
		if(position == 0)
		{
			// this is not set yest
  			$('#top-view').css('display', 'block');
  			$('#bottom-view').css('display', 'none');

		}
		else if(position == 1) 
		{
			$('#bottom-view').css('display', 'block');
			$('#top-view').css('display', 'none');
		}
		
	}


	function get_waiters(table_id)
	{


		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/get_all_waiters/"+table_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){

			var data = jQuery.parseJSON(data);
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;

			$("#table-details").html(data.table_details);
			$("#waiters-div").html(data.result);

			document.getElementById("table_id").value = table_id;
			
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});

	}


	function finalize_bill(total_amount)
	{


		var res = confirm('Are you sure you want to finalize this bill ?');

		if(res)
		{

			var pos_order_id = window.localStorage.getItem('pos_order_id');
			
			var config_url = $('#config_url').val();

			var data_url = config_url+"pos/finalize_bill/"+pos_order_id+"/"+total_amount;
			// window.alert(data_url);
			$.ajax({
			type:'POST',
			url: data_url,
			data:{appointment_id: 1},
			dataType: 'text',
			success:function(data){

				var data = jQuery.parseJSON(data);
					back_to_dashboard();
				},
				error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

			});
		}

		

	}
	function update_client_name()
	{
		var pos_order_id = window.localStorage.getItem('pos_order_id');
		var name = $('#name').val();
		var config_url = $('#config_url').val();

		var data_url = config_url+"pos/update_client_details/"+pos_order_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{name: name},
		dataType: 'text',
		success:function(data){

			var data = jQuery.parseJSON(data);
				// back_to_dashboard();
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
	}

	function delete_pos_order(pos_order_id)
	{
		var res = confirm('Are you sure you want to delete this order ?');

		if(res)
		{

			// var pos_order_id = window.localStorage.getItem('pos_order_id');
			// var name = $('#name').val();
			var config_url = $('#config_url').val();

			var data_url = config_url+"pos/delete_pos_order/"+pos_order_id;
			// window.alert(data_url);
			$.ajax({
			type:'POST',
			url: data_url,
			data:{name: name},
			dataType: 'text',
			success:function(data){

				var data = jQuery.parseJSON(data);
					// back_to_dashboard();
					back_to_dashboard();
				},
				error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

			});

		}
	}

	function update_vat_charged(vat_status)
	{
		var pos_order_id = window.localStorage.getItem('pos_order_id');

		var config_url = $('#config_url').val();

		var data_url = config_url+"pos/update_vat_status/"+pos_order_id+"/"+vat_status;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{vat_status: vat_status},
		dataType: 'text',
		success:function(data){

			var data = jQuery.parseJSON(data);
				// back_to_dashboard();
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
	}

	function print_order(pos_order_id)
	{
		var config_url = $('#config_url').val();

		var data_url = config_url+"print-bill/"+pos_order_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{vat_status: 1},
		dataType: 'text',
		success:function(data){

			var data = jQuery.parseJSON(data);
				// back_to_dashboard();
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
	}

	function print_orders_department(pos_order_id,print_status)
	{
			var config_url = $('#config_url').val();

			var data_url = config_url+"pos/print_order_printer/"+pos_order_id+"/"+print_status;
			// window.alert(data_url);
			$.ajax({
			type:'POST',
			url: data_url,
			data:{vat_status: 1},
			dataType: 'text',
			success:function(data){

				var data = jQuery.parseJSON(data);
					// back_to_dashboard();
				},
				error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

			});
	}

	function close_account()
	{
		// alert("dadada");
			var config_url = $('#config_url').val();
			
	    window.location.href = config_url+"logout-admin";
	}

	function create_new_table_order(table_id)
	{
		var sale_type = $('#sale_type').val();

		var config_url = $('#config_url').val();	

		var data_url = config_url+"pos/create_new_visit/"+sale_type;
			 
			 
	   $.ajax({
		   type:'POST',
		   url: data_url,
		   data:{table_id: table_id},
		   dataType: 'text',
		   success:function(data){
		      var data = jQuery.parseJSON(data);
		    
		    if(data.status == "success")
				{

					close_side_bar();
					
					var pos_order_id = data.pos_order_id;
					window.localStorage.setItem('pos_order_id',pos_order_id);
					get_order_items_list();

				}
				else
				{
					alert(data.result);
				}
		   
		   },
		   error: function(xhr, status, error) {
		   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		   
		   }
		   });
	}


	function search_name(table_no = 0, type = null,name=null) { // Default type to null for static buttons
    var order_name = $('#order_name').val();
    var config_url = $('#config_url').val();
    
    var data_url = config_url + "pos/get_todays_orders";
    
    $.ajax({
        type: 'POST',
        url: data_url,
        data: { query: order_name, table_no: table_no ,type:type},
        dataType: 'text',
        success: function(data) {
            $("#orders-div").html(data);
            window.localStorage.setItem('pos_order_item_id', null);

            // Reset all buttons to default state
            const buttons = document.querySelectorAll('.link-item-checked');
            buttons.forEach(button => {
                button.classList.remove('btn-warning');
                button.classList.add('btn-primary');
            });
            console.log(name);

            // Update the order_name input
            if (table_no >= 0) { // Include 'ALL' option (table_no = 0)
                document.getElementById("order_name").value = (table_no > 0 && type == null) ? "TABLE " + table_no : (table_no > 0 && type == 1) ?  ""+name  : " ALL TABLE ";

                // Highlight the selected button
                const selectedButton = document.getElementById("link-item" + (type ? "1-" + table_no : table_no));
                selectedButton.classList.remove('btn-primary');
                selectedButton.classList.add('btn-warning');
            }
        },
        error: function(xhr, status, error) {
            alert(error);
        }
    });
}

	function search_name_old(table_no=0,type)
	{

			var order_name = $('#order_name').val();
			var config_url = $('#config_url').val();

			var data_url = config_url+"pos/get_todays_orders";
			// window.alert(data_url);
			$.ajax({
			type:'POST',
			url: data_url,
			data:{query: order_name,table_no: table_no},
			dataType: 'text',
			success:function(data){

				$("#orders-div").html(data);
				window.localStorage.setItem('pos_order_item_id',null);

					document.getElementById("link-item1").classList.remove('btn-warning');
					document.getElementById("link-item2").classList.remove('btn-warning');
					document.getElementById("link-item3").classList.remove('btn-warning');
					document.getElementById("link-item4").classList.remove('btn-warning');
					document.getElementById("link-item5").classList.remove('btn-warning');
					document.getElementById("link-item6").classList.remove('btn-warning');
					document.getElementById("link-item7").classList.remove('btn-warning');
					document.getElementById("link-item8").classList.remove('btn-warning');
					document.getElementById("link-item9").classList.remove('btn-warning');
					document.getElementById("link-item10").classList.remove('btn-warning');
					document.getElementById("link-item11").classList.remove('btn-warning');
					document.getElementById("link-item12").classList.remove('btn-warning');
					document.getElementById("link-item13").classList.remove('btn-warning');
					document.getElementById("link-item14").classList.remove('btn-warning');


					document.getElementById("link-item1").classList.add('btn-primary');
					document.getElementById("link-item2").classList.add('btn-primary');
					document.getElementById("link-item3").classList.add('btn-primary');
					document.getElementById("link-item4").classList.add('btn-primary');
					document.getElementById("link-item5").classList.add('btn-primary');
					document.getElementById("link-item6").classList.add('btn-primary');
					document.getElementById("link-item7").classList.add('btn-primary');
					document.getElementById("link-item8").classList.add('btn-primary');
					document.getElementById("link-item9").classList.add('btn-primary');
					document.getElementById("link-item10").classList.add('btn-primary');
					document.getElementById("link-item11").classList.add('btn-primary');
					document.getElementById("link-item12").classList.add('btn-primary');
					document.getElementById("link-item13").classList.add('btn-primary');
					document.getElementById("link-item14").classList.add('btn-primary');
					document.getElementById("order_name").value = " ALL TABLE ";

					if(table_no > 0)
					{
						document.getElementById("order_name").value = "TABLE "+table_no;

					
						document.getElementById("link-item"+table_no).classList.remove('btn-primary');
						document.getElementById("link-item"+table_no).classList.add('btn-warning');
					}
				
				},
				error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

			});

	}

	function my_sales()
	{
			document.getElementById("sidebar-right").style.display = "block"; 
			document.getElementById("existing-sidebar-div").style.display = "none"; 


			var config_url = $('#config_url').val();

			var data_url = config_url+"pos/pos_reports/my_sales";
			//window.alert(data_url);
			$.ajax({
			type:'POST',
			url: data_url,
			data:{appointment_id: 1},
			dataType: 'text',
			success:function(data){
					var data = jQuery.parseJSON(data);
					//window.alert("You have successfully updated the symptoms");
					//obj.innerHTML = XMLHttpRequestObject.responseText;
					document.getElementById("current-sidebar-div").style.display = "block"; 
					$("#current-sidebar-div").html(data.result);
					// $("#payment-div").html(data.result);
						get_period_sales();
						
						// document.getElementById("payment-complete-off").style.display = "none"; 
					},
					error: function(xhr, status, error) {
					//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
					alert(error);
				}

			});
	}

	
	function get_my_order_detail(pos_order_id,order_invoice_id)
	{

		var config_url = $('#config_url').val();

		var data_url = config_url+"pos/pos_reports/view_sold_items/"+pos_order_id+"/"+order_invoice_id;
		
		
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){
			$("#sale-detail-list").html(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});


	}

	function get_period_sales(list_status = 0)
	{
		$("#sales-items-list").html('');
		var config_url = $('#config_url').val();

		var data_url = config_url+"pos/pos_reports/view_my_sales";
		
		
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{list_status : list_status},
		dataType: 'text',
		success:function(data){

			var data = jQuery.parseJSON(data);

			$("#sales-title").html(data.title);
			$("#sales-items-list").html(data.result);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}




</script>