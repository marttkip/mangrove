<?php



$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('d/m/Y');

//doctor
// var_dump($pos_order_id);die();
//served by
$served_by = $this->pos_model->get_personnel($this->session->userdata('personnel_id'));


$customer_details = $this->pos_model->get_order_details_invoice($order_invoice_id);

// var_dump($customer_details);die();

$customer_name = '';
$customer_number ='';
if($customer_details->num_rows() > 0)
{
	foreach ($customer_details->result() as $key => $value) {
		# code...
		$customer_name = $value->customer_name;
		$customer_number = $value->customer_number;
		$order_date = $value->order_date;
	}
}

$visit_date = date('d/m/Y',strtotime($order_date));

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Invoice</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
        	body
        	{
        		font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
        	}
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			
			.col-md-6 {
			    width: 50%;
			 }
		
			h3
			{
				font-size: 30px;
			}
			p
			{
				margin: 0 0 0px !important;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{ margin:0 auto;}
			.col-print-1 {width:8%;  float:left;}
			.col-print-2 {width:16%; float:left;}
			.col-print-3 {width:25%; float:left;}
			.col-print-4 {width:33%; float:left;}
			.col-print-5 {width:42%; float:left;}
			.col-print-6 {width:50%; float:left;}
			.col-print-7 {width:58%; float:left;}
			.col-print-8 {width:66%; float:left;}
			.col-print-9 {width:75%; float:left;}
			.col-print-10{width:83%; float:left;}
			.col-print-11{width:92%; float:left;}
			.col-print-12{width:100%; float:left;}

			table.table{
			    border-spacing: 0 !important;
			    font-size:12px;
				margin-top:0px;
				margin-bottom: 10px !important;
				border-collapse: inherit !important;
				font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
			}
			th, td {
			    border: 1px solid #000 !important;
			    padding: 0.5em 1em !important;
			}
			/* the first 'th' within the first 'tr' of the 'thead': */
			thead tr:first-child th:first-child {
			    border-radius: 10px 0 0 0 !important;
			}
			/* the last 'th' within the first 'tr' of the 'thead': */
			thead tr:first-child th:last-child {
			    border-radius: 0 10px 0 0 !important;
			}
			/* the first 'td' within the last 'tr' of the 'tbody': */
			tbody tr:last-child td:first-child {
			    border-radius: 0 0 0 10px !important;
			}
			/* the last 'td' within the last 'tr' of the 'tbody': */
			tbody tr:last-child td:last-child {
			    border-radius: 0 0 10px 10px !important;
			}
			tbody tr:first-child td:first-child {
			    border-radius: 10px 10px 0 0 !important;
			    border-bottom: 0px solid #000 !important;
			}
			.padd
			{
				padding:10px;
			}
			
		</style>
    </head>
    <!-- <body onLoad="window.print();return false;"> -->
    <body >	
 		

    	<div class="padd " >
    		<div class="col-md-18">
                <div class="col-print-4 pull-left">
                    <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive"/>
                </div>
                <div class="col-print-4 center-align">
                    <strong>
                       <h4> <?php echo $contacts['company_name'];?></h4><br/>
                        P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                        MOB No: <?php echo $contacts['phone'];?><br/>
                        E-Mail:<?php echo $contacts['email'];?>.<br> 
                        <?php echo $contacts['location'];?><br/>
                   
                    </strong>
                </div>
                 <div class="col-print-2 pull-right">
                    <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive"/>
                </div>
                
            </div>            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12 center-align">
                    <h3>INVOICE</h3>
                </div>
            </div>
    
        
	      	
	        
	        <div class="row" >
	      		<div class="col-md-12">
	      			<div class="col-print-5 left-align">
		            	<table class="table">
		            		<tr>
		            			<td>
			            			<span class="pull-left"><strong>To :</strong> </span> 
			            			<span class="pull-right" ><strong><?php echo $customer_name; ?> </strong> </span>
			            		</td>
		            		</tr>
		            		<tr>
		            			
		            			<td>
		            				<span class="pull-left"><strong>Customer No:</strong> </span> 
		            				<span class="pull-right" ><strong><?php echo $customer_number; ?> </strong> </span>

		            			</td>
		            		</tr>
		            	</table>
		            </div>
		            <div class="col-print-2">
		            	&nbsp;
		            </div>
		            <div class="col-print-5">
		            	<table class="table">
		            		<tr>
		            			<td style="border-radius: 10px 0px 0 0 !important;border-right: 0px solid #000 !important;">
			            			<span class="pull-left">Doc No </span> 
			            		</td>
			            		<td style="border-radius: 0px 10px 0 0 !important;border-bottom: 0px solid #000 !important;">
			            			<span class="pull-right" ><?php echo $customer_number; ?>  </span>
			            		</td>
		            		</tr>
		            		<tr>
		            			<td style="border-radius: 10px 0px 0 0 !important;border-right: 0px solid #000 !important;">
			            			<span class="pull-left">Date </span> 
			            		</td>
			            		<td style="border-radius: 0px 10px 0 0 !important;border-bottom: 0px solid #000 !important;">
			            			<span class="pull-right" ><?php echo $visit_date; ?>  </span>
			            		</td>
		            		</tr>
		            			<tr>
		            			<td style="border-radius: 10px 0px 0 0 !important;border-right: 0px solid #000 !important;">
			            			<span class="pull-left">Order No./QTN No </span> 
			            		</td>
			            		<td style="border-radius: 0px 10px 0 0 !important;border-bottom: 0px solid #000 !important;">
			            			<span class="pull-right" ><?php echo $customer_number; ?>  </span>
			            		</td>
		            		</tr>
		            		
		            		<tr>
		            			<td style="border-right: 0px solid #000 !important;">
			            			<span class="pull-left">Sales Person </span> 
			            		</td>
			            		<td style="border-radius: 0px 0px 10px 0 !important;">
			            			<span class="pull-right" ><?php echo $served_by; ?>  </span>
			            		</td>
		            		</tr>
		            		
		            	</table>
		            	
		            </div>
	      		</div>
	      	</div>
		     
		     
		   
	      	<div class="row">
	      		<div class="col-md-12">
	      			<table class="table">
		            		<tr>
		            			<td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important; width: 10%;">
			            			<span class="pull-left">Item </span> 
			            		
			            		</td>
			            		<td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important; width: 60%;">
			            			<span class="pull-left">Description </span> 
			            		
			            		</td>
			            		<td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important;  width: 10%;">
			            			<span class="pull-left">Qty </span> 
			            		
			            		</td>
			            		<td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important;  width: 10%;">
			            			<span class="pull-left">Rate </span> 
			            		
			            		</td>
			            		<td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important;  width: 10%;">
			            			<span class="pull-left">Amount </span> 
			            		
			            		</td>
		            		</tr>

		            		<?php


		            	$order_invoice_number ='';
						$preauth_date = date('Y-m-d');
						$preauth_amount = '';
						
						$visit__rs1 = $this->pos_model->get_visit_charges_charged($pos_order_id,$order_invoice_id);	
						
						// var_dump($pos_order_id);die();
						// $visit_rs = $this->accounts_model->get_visit_details($visit_id);
						$visit_type_id = 1;
						$close_card = 3;
						$total_amount= 0; 
						$days = 0;
						$count = 0;
						$item_list = '';
						$description_list = "";
						$quantity_list = "";
						$rate_list = "";
						$amount_list = "";
						$total= 0;  
						$total_units= 0;
						$number = 0;
						$vat_charged = 0;
						if($visit__rs1->num_rows() > 0)
						{						
							foreach ($visit__rs1->result() as $key1 => $value) :
								$v_procedure_id = $value->pos_order_item_id;
								$procedure_id = $value->service_charge_id;
								$product_code = $value->product_code;
								$pos_order_item_amount = $value->pos_order_item_amount;
								$units = $value->pos_order_item_quantity;
								$procedure_name = $value->service_charge_name;
								$service_id = $value->service_id;
								$vatable = $value->vatable;
								$product_id = $value->product_id;
								// $order_invoice_id = $value->order_invoice_id;
								// $visit_type_id = 1;
								$total= $total +($units * $pos_order_item_amount);

								if($order_invoice_id > 0)
								{
									$text_color = "success";
								}
								else
								{
									$text_color = 'default';
								}

							
								$checked="";
								$number++;

								if($vatable)
								{
									$vat_charged += ($units * $pos_order_item_amount) * 0.16;
								}

								$total_units += $units;
								$personnel_check = TRUE;
								if($personnel_check AND $close_card == 3)
								{
									$checked = "<td>
											
													<a class='btn btn-xs btn-danger' href='#' onclick='delete_procedure(".$v_procedure_id.", ".$pos_order_id.")'><i class='fa fa-trash'></i></a>
												</td>";
								}

								$item_list .= $product_code.'<br> ';
								$description_list .= $procedure_name."<br>";
								$quantity_list .= $units."<br>";
								$rate_list .= number_format($pos_order_item_amount,2)."<br>";
								$amount_list .= number_format($units * $pos_order_item_amount,2)."<br>";
	
								endforeach;

						}
						else
						{
							
						}
						
						$preauth_amount = $total;


							  
                        $balance = $preauth_amount - $payments_value;

                        ?>
		            		<tr>
		            			<td  style="border-radius: 10px 10px 0px 10px !important;border-bottom: 1px solid #000 !important;border-right: 0px solid #000 !important; height: 200px !important;">
			            			<span class="pull-left"> <?php echo $item_list;?> </span> 
			            		
			            		</td>
			            		<td  style="border-radius: 10px 10px 0px 0px !important;border-bottom: 1px solid #000 !important;border-right: 0px solid #000 !important;height: 200px !important;">
			            			<span class="pull-left">  <?php echo $description_list;?>  </span> 
			            		
			            		</td>
			            		<td  style="border-radius: 10px 10px 0px 0px !important;border-bottom: 1px solid #000 !important;border-right: 0px solid #000 !important;height: 200px !important;">
			            			<span class="pull-left"> <?php echo $quantity_list;?>  </span> 
			            		
			            		</td>
			            		<td  style="border-radius: 10px 10px 0px 0px !important;border-bottom: 1px solid #000 !important;border-right: 0px solid #000 !important;height: 200px !important;">
			            			<span class="pull-left"> <?php echo $rate_list;?> </span> 
			            		
			            		</td>
			            		<td  style="border-radius: 10px 10px 10px 0px !important;border-bottom: 1px solid #000 !important; height: 200px !important;">
			            			<span class="pull-left"> <?php echo $amount_list;?> </span> 
			            		
			            		</td>
		            		</tr>

		            		  

		            		<tr>
		            			<td style="border:none !important;"></td>
		            			<td  style="border:none !important;"></td>
		            			<td colspan="3"  style="border:none !important; padding:0px !important;">
		            				<table class="table" style="margin:none !important; padding: none !important;margin-top:0px !important;">
					            		<tr>
					            			<td style="border-radius: 10px 0px 0 0 !important;border-right: 0px solid #000 !important;border-top: 0px solid #000 !important;">
						            			<span class="pull-left"><strong>Sub Total</strong> </span> 
						            		</td>
						            		<td style="border-radius: 0px 10px 0 0 !important;border-bottom: 0px solid #000 !important;border-top: 0px solid #000 !important;">
						            			<span class="pull-right" >KES <?php echo number_format($preauth_amount,2); ?>  </span>
						            		</td>
					            		</tr>
					            		<tr>
					            			<td style="border-radius: 0px 0px 0 0 !important;border-right: 0px solid #000 !important; border-bottom: 0px solid #000 !important;">
						            			<span class="pull-left"><strong>VAT (14%)</strong> </span> 
						            		</td>
						            		<td style="border-radius: 0px 0px 0 0 !important;border-bottom: 0px solid #000 !important;">
						            			<span class="pull-right" >KES <?php echo number_format($vat_charged,2); ?> </span>
						            		</td>
					            		</tr>
					            		
					            <!-- 		<tr>
					            			<td style="border-radius: 0px 0px 0 0 !important;border-right: 0px solid #000 !important; border-bottom: 0px solid #000 !important;">
						            			<span class="pull-left"><strong>Payments</strong> </span> 
						            		</td>
						            		<td style="border-radius: 0px 0px 0 0 !important;border-bottom: 0px solid #000 !important;">
						            			<span class="pull-right" >KES <?php echo number_format($payments_value,2); ?> </span>
						            		</td>
					            		</tr> -->
					            		<tr>
					            			<td style="border-right: 0px solid #000 !important;">
						            			<span class="pull-left"><strong>Net Amount</strong> </span> 
						            		</td>
						            		<td style="border-radius: 0px 0px 10px 0 !important;">
						            			<span class="pull-right" >KES <?php echo number_format($balance,2); ?> </span>
						            		</td>
					            		</tr>
					            		
					            	</table>

		            			</td>
		            		</tr>
		            		

		            		
		            	</table>
	      		</div>
	      	</div>
	      	<div class="row">
	      		<div class="col-md-12" style="margin-bottom: 0px !important;font-size: 12px !important;">
	      			<!-- <p>Bill Payable to : <strong>Upper Hill Dental Centre</strong></p>
	      			<p>Modes of Payment; Cash,Cheque,EFT, Visa card,M-pesa (Pay bill No. 967966. Account No. UHDC)</p>
	      			
	      			<div >
	      				<div class="col-print-4">
	      					For Self Paying Patients
	      					<div class="col-print-12">
	      						<div class="col-print-4">
	      							Bank Name
	      						</div>
	      						<div class="col-print-1" style="width:2% !important;">
	      							:
	      						</div>
	      						<div class="col-print-7 pull-left">
	      							Stanbic Bank
	      						</div>
	      					</div>
	      					<div class="col-print-12">
	      						<div class="col-print-4">
	      							Bank Bank
	      						</div>
	      						<div class="col-print-1" style="width:2% !important;">
	      							:
	      						</div>
	      						<div class="col-print-7 pull-left">
	      							Upper Hill
	      						</div>
	      					</div>
	      					<div class="col-print-12">
	      						<div class="col-print-4">
	      							Account Number
	      						</div>
	      						<div class="col-print-1" style="width:2% !important;">
	      							:
	      						</div>
	      						<div class="col-print-7 pull-left">
	      							0100002224008
	      						</div>
	      					</div>
	      				</div>
	      				<div class="col-print-4">
	      					For Insurance Patients
	      					<div class="col-print-12">
	      						<div class="col-print-4">
	      							Bank Name
	      						</div>
	      						<div class="col-print-1" style="width:2% !important;">
	      							:
	      						</div>
	      						<div class="col-print-7">
	      							Stanbic Bank
	      						</div>
	      					</div>
	      					<div class="col-print-12">
	      						<div class="col-print-4">
	      							Bank Bank
	      						</div>
	      						<div class="col-print-1" style="width:2% !important;">
	      							:
	      						</div>
	      						<div class="col-print-7">
	      							Upper Hill
	      						</div>
	      					</div>
	      					<div class="col-print-12">
	      						<div class="col-print-4">
	      							Account Number
	      						</div>
	      						<div class="col-print-1" style="width:2% !important;">
	      							:
	      						</div>
	      						<div class="col-print-7">
	      							0100005465944
	      						</div>
	      					</div>
	      				</div>
	      				<div class="col-print-4">
	      					<div class="col-print-12">
	      						<div class="col-print-4">
	      							Swift Code
	      						</div>
	      						<div class="col-print-1" style="width:2% !important;">
	      							:
	      						</div>
	      						<div class="col-print-7">
	      							SBICKENX
	      						</div>
	      					</div>
	      					<div class="col-print-12">
	      						<div class="col-print-4">
	      							Bank Code
	      						</div>
	      						<div class="col-print-1" style="width:2% !important;">
	      							:
	      						</div>
	      						<div class="col-print-7">
	      							31
	      						</div>
	      					</div>
	      					<div class="col-print-12">
	      						<div class="col-print-4">
	      							Branch Code
	      						</div>
	      						<div class="col-print-1" style="width:2% !important;">
	      							:
	      						</div>
	      						<div class="col-print-7">
	      							010
	      						</div>
	      					</div>
	      				</div>
	      			</div> -->
	      			<div class="col-print-12">
	      				
	      			<!-- <strong>TERMS AND CONDITIONS:</strong>
	      			<br>
	      			<p style="font-size: 10px !important;">All accounts are due for payment strictly 30 days from the date of invoice or on demand. Interest of 3% per month will be surcharged on all overdue debts.</p> -->
	      			<!-- <p style="font-size: 10px !important; text-align: center;">Professor Nelson Awori Centre, Ground Floor - Suite A3 | Ralph Bunche Rd No. 7 (Next to The Nairobi Hospital) | P.O. Box 19986 00202 KNH Nairobi, Kenya</p>
	      			<p style="font-size: 10px !important;text-align: center;">info@upperhilldentalcentre.com | +254 736 579375 +254 20 2430110 +254 710 569959 | Admin/Accounts +254 706 706000 admin@upperhilldentalcentre.com</p> -->
	      			</div>
	      		</div>
	      	</div>
	    </div>


    </body>
    
</html>