<?php



$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y');

//doctor
// $doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
$served_by = $this->pos_model->get_personnel($this->session->userdata('personnel_id'));
// var_dump($served_by);die();
if(empty($served_by))
{
    $served_by = '&nbsp;';
}
// $credit_note_amount = $this->accounts_model->get_sum_credit_notes($visit_id);
// $debit_note_amount = $this->accounts_model->get_sum_debit_notes($visit_id);


$today = date('d/m/Y',strtotime($payment_date));


$grouped_service_rs = $this->pos_model->get_grouped_services($pos_order_id);
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <title><?php echo $contacts['company_name'];?> | Receipt</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
         /*body{font-family:"Courier New", Courier, monospace}*/
        .receipt_spacing{letter-spacing:0px; font-size: 12px;}
        .center-align{margin:0 auto; text-align:center;}
        
        .receipt_bottom_border{border-bottom: #888888 medium solid;}
        .row .col-md-12 table {
            border:solid #000 !important;
            border-width:1px 0 0 1px !important;
            font-size:20px;
        }
        .row .col-md-12 th, .row .col-md-12 td {
            border:solid #000 !important;
            border-width:0 1px 1px 0 !important;
        }
        .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
        {
             padding: 2px;
             font-size: 12px;
        }
        .col-print-1 {width:8%;  float:left;}
        .col-print-2 {width:16%; float:left;}
        .col-print-3 {width:25%; float:left;}
        .col-print-4 {width:33%; float:left;}
        .col-print-5 {width:42%; float:left;}
        .col-print-6 {width:50%; float:left;}
        .col-print-7 {width:58%; float:left;}
        .col-print-8 {width:66%; float:left;}
        .col-print-9 {width:75%; float:left;}
        .col-print-10{width:83%; float:left;}
        .col-print-11{width:92%; float:left;}
        .col-print-12{width:100%; float:left;}

        .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
        .title-img{float:left; padding-left:30px;}
        img.logo{max-height:70px; margin:0 auto;}
    </style>
    </head>
    <body class="receipt_spacing" onLoad="window.print();return false;">
        
      		<div class="col-md-12">
            	<?php
            	
            	 if($print_order_type < 2)
            	 {
            		// var_dump($grouped_service_rs->num_rows());die();
            		if($grouped_service_rs->num_rows() > 0)
            		{
            			foreach ($grouped_service_rs->result() as $key => $value) {
            				# code...
            				$service_name = $value->service_name;
            				$service_id = $value->service_id;

            				?>
            				<div class="row">
				                <div class="col-print-12 center-align">
				                    <strong>
				                       <h4> <?php echo $contacts['company_name'];?></h4><br/>
				                        P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
				                        MOB No: <?php echo $contacts['phone'];?><br/>
				                        E-Mail:<?php echo $contacts['email'];?>.<br> 
				                        <?php echo $contacts['location'];?><br/>
				                   
				                    </strong>
				                </div>
					                
			                    <div class="row" style="margin-bottom: 5px;">
			                        
			                        <div class="col-print-12 center-align">
			                            <h4><?php echo $service_name;?> </h4>
			                        </div>
			                         
			                    </div>

				            	<div class="row" >
				                
					             	<div class="col-md-12">
				                        <table class="table table-hover table-bordered table-striped" style="font-size: 20px !important">
				                          <thead>
				                          <tr>
				                            <th>Item</th>
				                            <th>Units</th>
				                          </tr>
				                          </thead>
				                          <tbody>

				                            <?php


				                            $order_invoice_number ='';
				                            $preauth_date = date('Y-m-d');
				                            $preauth_amount = '';
				                            
				                            $visit__rs1 = $this->pos_model->get_visit_charges_charged($pos_order_id,NULL,$service_id); 
				                            
				                            // var_dump($pos_order_id);die();
				                            // $visit_rs = $this->accounts_model->get_visit_details($visit_id);
				                            $visit_type_id = 1;
				                            $close_card = 3;
				                            $total_amount= 0; 
				                            $days = 0;
				                            $count = 0;
				                            $item_list = '';
				                            $description_list = "";
				                            $quantity_list = "";
				                            $rate_list = "";
				                            $amount_list = "";
				                            $total= 0;  
				                            $total_units= 0;
				                            $number = 0;
				                            $vat_charged = 0;
				                            $result ='';
				                            if($visit__rs1->num_rows() > 0)
				                            {                       
				                                foreach ($visit__rs1->result() as $key1 => $value) :
				                                    $v_procedure_id = $value->pos_order_item_id;
				                                    $procedure_id = $value->service_charge_id;
				                                    $product_code = $value->product_code;
				                                    $pos_order_item_amount = $value->pos_order_item_amount;
				                                    $units = $value->pos_order_item_quantity;
				                                    $procedure_name = $value->service_charge_name;
				                                    $service_id = $value->service_id;
				                                    $vatable = $value->vatable;
				                                    $product_id = $value->product_id;
				                                    // $order_invoice_id = $value->order_invoice_id;
				                                    // $visit_type_id = 1;
				                                    $total= $total +($units * $pos_order_item_amount);

				                                    if($order_invoice_id > 0)
				                                    {
				                                        $text_color = "success";
				                                    }
				                                    else
				                                    {
				                                        $text_color = 'default';
				                                    }

				                                
				                                    $checked="";
				                                    $number++;

				                                    if($vatable)
				                                    {
				                                        $vat_charged += ($units * $pos_order_item_amount) * 0.16;
				                                    }

				                                    $total_units += $units;
				                                    $personnel_check = TRUE;
				                                    if($personnel_check AND $close_card == 3)
				                                    {
				                                        $checked = "<td>
				                                                
				                                                        <a class='btn btn-xs btn-danger' href='#' onclick='delete_procedure(".$v_procedure_id.", ".$pos_order_id.")'><i class='fa fa-trash'></i></a>
				                                                    </td>";
				                                    }

				                                    $result .= '<tr>
				                                                    <td>'.$procedure_name.'</td>
				                                                    <td>'.$units.'</td>
				                                                 </tr>';
				        
				                                    endforeach;

				                            }
				                            else
				                            {
				                                
				                            }
				                            
				                            $preauth_amount = $total;


				                            $payments_value = $total_payments;
				                            $balance = $preauth_amount - $total_payments;

				                            ?>
				                                <?php echo $result?>

				                                  
				                                        
				                               </tbody>

				                        </table>
				                    </div>
				            
				            	</div>
				        	</div>

				        	<!-- <div class="col-md-12" >
					            <div class="center-align">
					                <h4>FISCAL RECEIPT</h4>
					                <div class="row" style="font-style:italic; font-size:10px;">
					                    <div >
					                        Served by: <?php echo $served_by; ?>
					                    </div>
					                    <div >
					                        <?php echo $today; ?> Thank you
					                    </div>
					                </div>
					            </div>
					            
					        </div> -->
				        	<div class="pagebreak"> </div>

            				<?php

            			}
            		}
            	}

            	else if($print_order_type == 2)
            	{
            	?>
            

	            	<div class="row">
		                <div class="col-print-12 center-align">
		                    <strong>
		                       <h4> <?php echo $contacts['company_name'];?></h4><br/>
		                        P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
		                        MOB No: <?php echo $contacts['phone'];?><br/>
		                        E-Mail:<?php echo $contacts['email'];?>.<br> 
		                        <?php echo $contacts['location'];?><br/>
		                   
		                    </strong>
		                </div>
			                
			           
	                    <div class="row" style="margin-bottom: 10px;">
	                        
	                        <div class="col-print-12 center-align">
	                            <h4>Customer Order </h4>
	                        </div>
	                    </div>

		            	<div class="row" >
		                	<div class="col-md-12">
		                        <table class="table table-hover table-bordered table-striped" style="font-size: 20px !important">
		                          <thead>
		                          <tr>
		                            <th>Item</th>
		                            <th>Units</th>
		                          </tr>
		                          </thead>
		                          <tbody>

		                            <?php


		                            $order_invoice_number ='';
		                            $preauth_date = date('Y-m-d');
		                            $preauth_amount = '';
		                            
		                            $visit__rs1 = $this->pos_model->get_visit_charges_charged($pos_order_id,NULL,NULL); 
		                            
		                            // var_dump($pos_order_id);die();
		                            // $visit_rs = $this->accounts_model->get_visit_details($visit_id);
		                            $visit_type_id = 1;
		                            $close_card = 3;
		                            $total_amount= 0; 
		                            $days = 0;
		                            $count = 0;
		                            $item_list = '';
		                            $description_list = "";
		                            $quantity_list = "";
		                            $rate_list = "";
		                            $amount_list = "";
		                            $total= 0;  
		                            $total_units= 0;
		                            $number = 0;
		                            $vat_charged = 0;
		                            $result ='';
		                            if($visit__rs1->num_rows() > 0)
		                            {                       
		                                foreach ($visit__rs1->result() as $key1 => $value) :
		                                    $v_procedure_id = $value->pos_order_item_id;
		                                    $procedure_id = $value->service_charge_id;
		                                    $product_code = $value->product_code;
		                                    $pos_order_item_amount = $value->pos_order_item_amount;
		                                    $units = $value->pos_order_item_quantity;
		                                    $procedure_name = $value->service_charge_name;
		                                    $service_id = $value->service_id;
		                                    $vatable = $value->vatable;
		                                    $product_id = $value->product_id;
		                                    // $order_invoice_id = $value->order_invoice_id;
		                                    // $visit_type_id = 1;
		                                    $total= $total +($units * $pos_order_item_amount);

		                                    if($order_invoice_id > 0)
		                                    {
		                                        $text_color = "success";
		                                    }
		                                    else
		                                    {
		                                        $text_color = 'default';
		                                    }

		                                
		                                    $checked="";
		                                    $number++;

		                                    if($vatable)
		                                    {
		                                        $vat_charged += ($units * $pos_order_item_amount) * 0.16;
		                                    }

		                                    $total_units += $units;
		                                    $personnel_check = TRUE;
		                                    if($personnel_check AND $close_card == 3)
		                                    {
		                                        $checked = "<td>
		                                                
		                                                        <a class='btn btn-xs btn-danger' href='#' onclick='delete_procedure(".$v_procedure_id.", ".$pos_order_id.")'><i class='fa fa-trash'></i></a>
		                                                    </td>";
		                                    }

		                                    $result .= '<tr>
		                                                    <td>'.$procedure_name.'</td>
		                                                    <td>'.$units.'</td>
		                                                 </tr>';
		        
		                                    endforeach;

		                            }
		                            else
		                            {
		                                
		                            }
		                            
		                            $preauth_amount = $total;


		                            $payments_value = $total_payments;
		                            $balance = $preauth_amount - $total_payments;

		                            ?>
		                                <?php echo $result?>

		                                  
		                                        
		                               </tbody>

		                                
		                            </table>
		                    </div>
		            
		            	</div>
		        	</div>
		        	<!-- <div class="col-md-12" >
			            <div class="center-align">
			                <h4>FISCAL RECEIPT</h4>
			                <div class="row" style="font-style:italic; font-size:10px;">
			                    <div >
			                        Served by: <?php echo $served_by; ?>
			                    </div>
			                    <div >
			                        <?php echo $today; ?> Thank you
			                    </div>
			                </div>
			            </div>
			            
			        </div> -->
		        	<div class="pagebreak"> </div>

			        <div class="row">
			                
		                <div class="col-print-12 center-align">
		                    <strong>
		                       <h4> <?php echo $contacts['company_name'];?></h4><br/>
		                        P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
		                        MOB No: <?php echo $contacts['phone'];?><br/>
		                        E-Mail:<?php echo $contacts['email'];?>.<br> 
		                        <?php echo $contacts['location'];?><br/>
		                   
		                    </strong>
		                </div>
			                
			           
	                    <div class="row" style="margin-bottom: 10px;">
	                       
	                        <div class="col-print-4 center-align">
	                            <h4>Customer Order </h4>
	                        </div>
	                    </div>

		            	<div class="row" >
		                
			             	<div class="col-md-12">
			                        <table class="table table-hover table-bordered table-striped" style="font-size: 20px !important">
			                          <thead>
			                          <tr>
			                            <th>Item</th>
			                            <th>Units</th>
			                          </tr>
			                          </thead>
			                          <tbody>

			                            <?php


			                            $order_invoice_number ='';
			                            $preauth_date = date('Y-m-d');
			                            $preauth_amount = '';
			                            
			                            $visit__rs1 = $this->pos_model->get_visit_charges_charged($pos_order_id,NULL,NULL); 
			                            
			                            // var_dump($pos_order_id);die();
			                            // $visit_rs = $this->accounts_model->get_visit_details($visit_id);
			                            $visit_type_id = 1;
			                            $close_card = 3;
			                            $total_amount= 0; 
			                            $days = 0;
			                            $count = 0;
			                            $item_list = '';
			                            $description_list = "";
			                            $quantity_list = "";
			                            $rate_list = "";
			                            $amount_list = "";
			                            $total= 0;  
			                            $total_units= 0;
			                            $number = 0;
			                            $vat_charged = 0;
			                            $result ='';
			                            if($visit__rs1->num_rows() > 0)
			                            {                       
			                                foreach ($visit__rs1->result() as $key1 => $value) :
			                                    $v_procedure_id = $value->pos_order_item_id;
			                                    $procedure_id = $value->service_charge_id;
			                                    $product_code = $value->product_code;
			                                    $pos_order_item_amount = $value->pos_order_item_amount;
			                                    $units = $value->pos_order_item_quantity;
			                                    $procedure_name = $value->service_charge_name;
			                                    $service_id = $value->service_id;
			                                    $vatable = $value->vatable;
			                                    $product_id = $value->product_id;
			                                    // $order_invoice_id = $value->order_invoice_id;
			                                    // $visit_type_id = 1;
			                                    $total= $total +($units * $pos_order_item_amount);

			                                    if($order_invoice_id > 0)
			                                    {
			                                        $text_color = "success";
			                                    }
			                                    else
			                                    {
			                                        $text_color = 'default';
			                                    }

			                                
			                                    $checked="";
			                                    $number++;

			                                    if($vatable)
			                                    {
			                                        $vat_charged += ($units * $pos_order_item_amount) * 0.16;
			                                    }

			                                    $total_units += $units;
			                                    $personnel_check = TRUE;
			                                    if($personnel_check AND $close_card == 3)
			                                    {
			                                        $checked = "<td>
			                                                
			                                                        <a class='btn btn-xs btn-danger' href='#' onclick='delete_procedure(".$v_procedure_id.", ".$pos_order_id.")'><i class='fa fa-trash'></i></a>
			                                                    </td>";
			                                    }

			                                    $result .= '<tr>
			                                                    <td>'.$procedure_name.'</td>
			                                                    <td>'.$units.'</td>
			                                                 </tr>';
			        
			                                    endforeach;

			                            }
			                            else
			                            {
			                                
			                            }
			                            
			                            $preauth_amount = $total;


			                            $payments_value = $total_payments;
			                            $balance = $preauth_amount - $total_payments;

			                            ?>
			                                <?php echo $result?>

			                                  
			                                        
			                               </tbody>

			                                
			                            </table>
			                    </div>
		            
		            	</div>
		        	</div>
		        	<!-- <div class="col-md-12" >
			            <div class="center-align">
			                <h4>FISCAL RECEIPT</h4>
			                <div class="row" style="font-style:italic; font-size:10px;">
			                    <div >
			                        Served by: <?php echo $served_by; ?>
			                    </div>
			                    <div >
			                        <?php echo $today; ?> Thank you
			                    </div>
			                </div>
			            </div>
			            
			        </div> -->
		        <?php
		    	}
		        ?>
	            
	       
		</div>
	    
        <br>
       
    </body>
    
</html>