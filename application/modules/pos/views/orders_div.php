<div class="col-md-12" style="margin-top: 10px">
	<?php
	$job_title_id = $this->session->userdata('job_title_id');
	$name = $this->session->userdata('personnel_fname');
	if($job_title_id == 4)
	{
		?>
		<div class="col-md-4">
			<!-- <h4>Sales Summary</h4> -->
			<div style="height:80vh;overflow-y: scroll;" class="orders-div-item">

				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-defailt center-align"><?php echo date('jS M Y');?> SALES</div>
					</div>
					<div class="col-md-12">
						<div class="alert alert-danger center-align">COMPLETED : <span id="completed_orders"></span>  </div>
					</div>
					<div class="col-md-12">
						<div class="alert alert-warning center-align">PENDING : <span id="pending_orders"></span> </div>
					</div>
					
				</div>
				<div class="center-align">
	                <a onclick="close_account() "  >
	                    <i class="fa fa-lock" style="font-size: 250px !important;"></i>
	                    <br>
	                    <?php echo $name;?> LOCK
	                </a>
	            </div>
			</div>
		</div>
		<div class="col-md-4">
			<h4>New Orders</h4>
			<div style="height:80vh;overflow-y: scroll;" class="orders-div-item">
			

				<?php
				$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
				if(empty($authorize_invoice_changes))
				{
					$authorize_invoice_changes	 = 0;
				}
				$incomplete_orders_rs = $this->pos_model->get_order_lists(1,$authorize_invoice_changes);
				// var_dump($incomplete_orders_rs);die();
				$incomplete_result ='';

				if($incomplete_orders_rs->num_rows() > 0)
				{
					$x=0;
					foreach ($incomplete_orders_rs->result() as $key => $value) {
						# code...
						$pos_order_id = $value->pos_order_id;
						$customer_id = $value->customer_id;
						$order_date = $value->order_date;
						$sale_type = $value->sale_type;
						$name = $value->name;
						$pos_order_number = $value->pos_order_number;
						$personnel_fname = $value->personnel_fname;
						$table_id = $value->table_id;
						$count = $value->total_items;
						$order_invoice_id = $value->order_invoice_id;
						if($sale_type == 0)
						{
							$sale = 'Cash Sale';
						}
						else if($sale_type == 1)
						{
							$sale = 'Take Away';
						}
						else if($sale_type == 3)
						{
							$sale = 'Quotation';
						}

						if($table_id > 0)
						{
							$sale = 'Table :'.$table_id;
						}
						$order_details_rs = $this->pos_model->get_visit_charges_charged($pos_order_id);
						$items_list = '';
						$total_amount =0;
						if($order_details_rs->num_rows() > 0)
						{						
							foreach ($order_details_rs->result() as $key1 => $value2) :
								$v_procedure_id = $value2->pos_order_item_id;
								$procedure_id = $value2->service_charge_id;
								$product_code = $value2->product_code;
								$pos_order_item_amount = $value2->pos_order_item_amount;
								$units = $value2->pos_order_item_quantity;
								$procedure_name = $value2->service_charge_name;
								$service_id = $value2->service_id;
								$vatable = $value2->vatable;
								$product_id = $value2->product_id;

								$items_list .= $units.' X '.$procedure_name.' <span class="pull-right">'.number_format(($pos_order_item_amount*$units),2).'</span><br>';

								$total_amount += $pos_order_item_amount*$units;
							endforeach;
							$items_list .= '<br><strong>Total <span class="pull-right">'.number_format(($total_amount),2).'</span></strong><br>';
						}
						// if($order_invoice_id > 0)
						// {
							$color = '';
						// }
						// else
						// {
						// 	$color = 'warning';
						// }

						$total_units = $order_details_rs->num_rows();

						if($total_units == 0)
						{
							$button = '<a onclick="delete_pos_order('.$pos_order_id.')" class="btn btn-xs btn-danger pull-right"><i class="fa fa-trash"></i></a>';
						}
						else
						{
							$button = '';
						}
						$x++;

						$incomplete_result .='<table class="table table-condensed table-bordered '.$color.'" onclick="get_order_detail('.$pos_order_id.','.$order_invoice_id.')">
										<thead>
											<tr class="'.$color.'">
												<th>Title</th>
												<th>Description : '.$name.' '.$button.'</th>
											</tr>
										</thead>
										<tbody>';
						$incomplete_result .='
											<tr >

												<th >Date</th>
												<td >'.date('jS M Y',strtotime($order_date)).' - '.$pos_order_number.'</td>
												
											</tr>';
					
						$incomplete_result .='
											<tr >

												<td colspan="2">'.$items_list.'</td>
											</tr>';
						$incomplete_result .='
											<tr >

												<th >Waiter</th>
												<td >'.$personnel_fname.'</td>
											</tr>';
						$incomplete_result .='
											<tr >

												<th >Type</th>
												<td >'.$sale.'</td>
											</tr>';
						$incomplete_result .='
											<tr >

												<th >Customer</th>
												<td >'.strtoupper($name).'</td>
											</tr>';
						$incomplete_result .='</tbody>
								</table>';


					}
				}
				

				echo $incomplete_result;
				?>

			</div>
			
			
			
		</div>
		<div class="col-md-4" >
			<h4>Billed Orders</h4>
			<div style="height:80vh;overflow-y: scroll;" class="orders-div-item">
			

				<?php


				$incomplete_orders_rs = $this->pos_model->get_order_lists(2,$authorize_invoice_changes);

				$incomplete_result ='';

				if($incomplete_orders_rs->num_rows() > 0)
				{
					$x=0;
					foreach ($incomplete_orders_rs->result() as $key => $value) {
						# code...
						$pos_order_id = $value->pos_order_id;
						$customer_id = $value->customer_id;
						$order_date = $value->order_date;
						$sale_type = $value->sale_type;
						$name = $value->name;
						$pos_order_number = $value->pos_order_number;
						$personnel_fname = $value->personnel_fname;
						$order_invoice_id = $value->order_invoice_id;
						$table_id = $value->table_id;
						$count = $value->total_items;

						$order_details_rs = $this->pos_model->get_visit_charges_charged($pos_order_id);
						$items_list = '';
						$total_amount = 0;
						if($order_details_rs->num_rows() > 0)
						{						
							foreach ($order_details_rs->result() as $key1 => $value2) :
								$v_procedure_id = $value2->pos_order_item_id;
								$procedure_id = $value2->service_charge_id;
								$product_code = $value2->product_code;
								$pos_order_item_amount = $value2->pos_order_item_amount;
								$units = $value2->pos_order_item_quantity;
								$procedure_name = $value2->service_charge_name;
								$service_id = $value2->service_id;
								$vatable = $value2->vatable;
								$product_id = $value2->product_id;

								$items_list .= $units.' X '.$procedure_name.' <span class="pull-right">'.number_format(($pos_order_item_amount*$units),2).'</span><br>';

								$total_amount += $pos_order_item_amount*$units;
							endforeach;

							$items_list .= '<br><strong>Total <span class="pull-right">'.number_format(($total_amount),2).'</span></strong><br>';
						}
						if($sale_type == 0)
						{
							$sale = 'Cash Sale';
						}
						else if($sale_type == 1)
						{
							$sale = 'Take Away';
						}
						else if($sale_type == 3)
						{
							$sale = 'Quotation';
						}


						if($table_id > 0)
						{
							$sale = 'Table :'.$table_id;
						}

						$total_units = $order_details_rs->num_rows();

						if($total_units == 0)
						{
							$button = '<a onclick="delete_pos_order('.$pos_order_id.')" class="btn btn-xs btn-danger pull-right"><i class="fa fa-trash"></i></a>';
						}
						else
						{
							$button = '';
						}

						// if($order_invoice_id > 0)
						// {
						// 	$color = 'success';
						// }
						// else
						// {
							$color = 'warning';
						// }
						$x++;
						$incomplete_result .='<table class="table table-condensed table-bordered '.$color.'" onclick="get_order_detail('.$pos_order_id.','.$order_invoice_id.')" >
										<thead>
											<tr class="'.$color.'">
												<th>Title</th>
												<th>Description : '.$name.' '.$button.' </th>
											</tr>
										</thead>
										<tbody>';
						$incomplete_result .='
											<tr >

												<th >Date</th>
												<td >'.date('jS M Y',strtotime($order_date)).' # '.$pos_order_number.'</td>
												
											</tr>';
				
						$incomplete_result .='
											<tr >
												<td colspan="2">'.$items_list.'</td>
											</tr>';
						$incomplete_result .='
											<tr >

												<th >Waiter</th>
												<td >'.$personnel_fname.'</td>
											</tr>';
						$incomplete_result .='
											<tr >

												<th >Type</th>
												<td >'.$sale.'</td>
											</tr>';
						$incomplete_result .='
											<tr >

												<th >Customer</th>
												<td >'.strtoupper($name).'</td>
											</tr>';
						$incomplete_result .='</tbody>
								</table>';

					}
				}

				echo $incomplete_result;
				?>

			</div>
			
			
			
		</div>
		<?php
	}
	else
	{


	?>
		<div class="col-md-4">
			<h4>New Orders</h4>
			<div style="height:80vh;overflow-y: scroll;" class="orders-div-item">
			

				<?php
				$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
				if(empty($authorize_invoice_changes))
				{
					$authorize_invoice_changes	 = 0;
				}
				$incomplete_orders_rs = $this->pos_model->get_order_lists(1,$authorize_invoice_changes);
				// var_dump($incomplete_orders_rs);die();
				$incomplete_result ='';

				if($incomplete_orders_rs->num_rows() > 0)
				{
					$x=0;
					foreach ($incomplete_orders_rs->result() as $key => $value) {
						# code...
						$pos_order_id = $value->pos_order_id;
						$customer_id = $value->customer_id;
						$order_date = $value->order_date;
						$sale_type = $value->sale_type;
						$name = $value->name;
						$pos_order_number = $value->pos_order_number;
						$personnel_fname = $value->personnel_fname;
						$table_id = $value->table_id;
						$count = $value->total_items;
						$order_invoice_id = $value->order_invoice_id;
						if($sale_type == 0)
						{
							$sale = 'Cash Sale';
						}
						else if($sale_type == 1)
						{
							$sale = 'Take Away';
						}
						else if($sale_type == 3)
						{
							$sale = 'Quotation';
						}

						if($table_id > 0)
						{
							$sale = 'Table :'.$table_id;
						}
						$order_details_rs = $this->pos_model->get_visit_charges_charged($pos_order_id);
						$items_list = '';
						$total_amount =0;
						if($order_details_rs->num_rows() > 0)
						{						
							foreach ($order_details_rs->result() as $key1 => $value2) :
								$v_procedure_id = $value2->pos_order_item_id;
								$procedure_id = $value2->service_charge_id;
								$product_code = $value2->product_code;
								$pos_order_item_amount = $value2->pos_order_item_amount;
								$units = $value2->pos_order_item_quantity;
								$procedure_name = $value2->service_charge_name;
								$service_id = $value2->service_id;
								$vatable = $value2->vatable;
								$product_id = $value2->product_id;

								$items_list .= $units.' X '.$procedure_name.' <span class="pull-right">'.number_format(($pos_order_item_amount*$units),2).'</span><br>';

								$total_amount += $pos_order_item_amount*$units;
							endforeach;
							$items_list .= '<br><strong>Total <span class="pull-right">'.number_format(($total_amount),2).'</span></strong><br>';
						}
						// if($order_invoice_id > 0)
						// {
							$color = '';
						// }
						// else
						// {
						// 	$color = 'warning';
						// }

						$total_units = $order_details_rs->num_rows();

						if($total_units == 0)
						{
							$button = '<a onclick="delete_pos_order('.$pos_order_id.')" class="btn btn-xs btn-danger pull-right"><i class="fa fa-trash"></i></a>';
						}
						else
						{
							$button = '';
						}
						$x++;

						$incomplete_result .='<table class="table table-condensed table-bordered '.$color.'" onclick="get_order_detail('.$pos_order_id.','.$order_invoice_id.')">
										<thead>
											<tr class="'.$color.'">
												<th>Title</th>
												<th>Description : '.$name.' '.$button.'</th>
											</tr>
										</thead>
										<tbody>';
						$incomplete_result .='
											<tr >

												<th >Date</th>
												<td >'.date('jS M Y',strtotime($order_date)).' - '.$pos_order_number.'</td>
												
											</tr>';
					
						$incomplete_result .='
											<tr >

												<td colspan="2">'.$items_list.'</td>
											</tr>';
						$incomplete_result .='
											<tr >

												<th >Waiter</th>
												<td >'.$personnel_fname.'</td>
											</tr>';
						$incomplete_result .='
											<tr >

												<th >Type</th>
												<td >'.$sale.'</td>
											</tr>';
						$incomplete_result .='
											<tr >

												<th >Customer</th>
												<td >'.strtoupper($name).'</td>
											</tr>';
						$incomplete_result .='</tbody>
								</table>';


					}
				}
				

				echo $incomplete_result;
				?>

			</div>
			
			
			
		</div>
		<div class="col-md-4" >
			<h4>Billed Orders</h4>
			<div style="height:80vh;overflow-y: scroll;" class="orders-div-item">
			

				<?php


				$incomplete_orders_rs = $this->pos_model->get_order_lists(2,$authorize_invoice_changes);

				$incomplete_result ='';

				if($incomplete_orders_rs->num_rows() > 0)
				{
					$x=0;
					foreach ($incomplete_orders_rs->result() as $key => $value) {
						# code...
						$pos_order_id = $value->pos_order_id;
						$customer_id = $value->customer_id;
						$order_date = $value->order_date;
						$sale_type = $value->sale_type;
						$name = $value->name;
						$pos_order_number = $value->pos_order_number;
						$personnel_fname = $value->personnel_fname;
						$order_invoice_id = $value->order_invoice_id;
						$table_id = $value->table_id;
						$count = $value->total_items;

						$order_details_rs = $this->pos_model->get_visit_charges_charged($pos_order_id);
						$items_list = '';
						$total_amount = 0;
						if($order_details_rs->num_rows() > 0)
						{						
							foreach ($order_details_rs->result() as $key1 => $value2) :
								$v_procedure_id = $value2->pos_order_item_id;
								$procedure_id = $value2->service_charge_id;
								$product_code = $value2->product_code;
								$pos_order_item_amount = $value2->pos_order_item_amount;
								$units = $value2->pos_order_item_quantity;
								$procedure_name = $value2->service_charge_name;
								$service_id = $value2->service_id;
								$vatable = $value2->vatable;
								$product_id = $value2->product_id;

								$items_list .= $units.' X '.$procedure_name.' <span class="pull-right">'.number_format(($pos_order_item_amount*$units),2).'</span><br>';

								$total_amount += $pos_order_item_amount*$units;
							endforeach;

							$items_list .= '<br><strong>Total <span class="pull-right">'.number_format(($total_amount),2).'</span></strong><br>';
						}
						if($sale_type == 0)
						{
							$sale = 'Cash Sale';
							$color='warning';
						}
						else if($sale_type == 1)
						{
							$sale = 'Take Away';
							$color='primary';
						}
						else if($sale_type == 3)
						{
							$sale = 'Quotation';
							$color='danger';
						}


						if($table_id > 0)
						{
							$sale = 'Table :'.$table_id;
						}

						$total_units = $order_details_rs->num_rows();

						if($total_units == 0)
						{
							$button = '<a onclick="delete_pos_order('.$pos_order_id.')" class="btn btn-xs btn-danger pull-right"><i class="fa fa-trash"></i></a>';
						}
						else
						{
							$button = '';
						}

						// if($order_invoice_id > 0)
						// {
						// 	$color = 'success';
						// }
						// else
						// {
							// $color = 'warning';
						// }
						$x++;
						$incomplete_result .='<table class="table table-condensed table-bordered '.$color.'" onclick="get_order_detail('.$pos_order_id.','.$order_invoice_id.')" >
										<thead>
											<tr class="'.$color.'">
												<th>Title</th>
												<th>Description : '.$name.' '.$button.' </th>
											</tr>
										</thead>
										<tbody>';
						$incomplete_result .='
											<tr >

												<th >Date</th>
												<td >'.date('jS M Y',strtotime($order_date)).' # '.$pos_order_number.'</td>
												
											</tr>';
				
						$incomplete_result .='
											<tr >
												<td colspan="2">'.$items_list.'</td>
											</tr>';
						$incomplete_result .='
											<tr >

												<th >Waiter</th>
												<td >'.$personnel_fname.'</td>
											</tr>';
						$incomplete_result .='
											<tr >

												<th >Type</th>
												<td >'.$sale.'</td>
											</tr>';
						$incomplete_result .='
											<tr >

												<th >Customer</th>
												<td >'.strtoupper($name).'</td>
											</tr>';
						$incomplete_result .='</tbody>
								</table>';

					}
				}

				echo $incomplete_result;
				?>

			</div>
			
			
			
		</div>
		<div class="col-md-4">
		<h4>Completed Sales</h4>
		<div style="height:80vh;overflow-y: scroll;" class="orders-div-item">
			


			<?php


			$complete_orders_rs = $this->pos_model->get_order_lists(3,$authorize_invoice_changes);

			// var_dump($complete_orders_rs);die();

			$complete_result ='';

			if($complete_orders_rs->num_rows() > 0)
			{
				$x=0;
				foreach ($complete_orders_rs->result() as $key => $value) {
					# code...
					# code...
					$pos_order_id = $value->pos_order_id;
					$order_invoice_id = $value->order_invoice_id;
					// $customer_id = $value->customer_id;
					$order_date = $value->order_date;
					$name = $value->name;
					$pos_order_number = $value->pos_order_number;
					$personnel_fname = $value->personnel_fname;
					$count = $value->total_items;
					$sale_type = $value->sale_type;
					$table_id = $value->table_id;

					if($sale_type == 0)
					{
						$sale = 'Cash Sale';
						$color='warning';
					}
					else if($sale_type == 1)
					{
						$sale = 'Take Away';
						$color='primary';
					}
					else if($sale_type == 3)
					{
						$sale = 'Quotation';
						$color='danger';
					}
					// var_dump($pos_order_id);die();
					$order_details_rs = $this->pos_model->get_visit_charges_charged($pos_order_id);
					$items_list = '';
					$total_amount = 0;
					if($order_details_rs->num_rows() > 0)
					{						
						foreach ($order_details_rs->result() as $key1 => $value2) :
							$v_procedure_id = $value2->pos_order_item_id;
							$procedure_id = $value2->service_charge_id;
							$product_code = $value2->product_code;
							$pos_order_item_amount = $value2->pos_order_item_amount;
							$units = $value2->pos_order_item_quantity;
							$procedure_name = $value2->service_charge_name;
							$service_id = $value2->service_id;
							$vatable = $value2->vatable;
							$product_id = $value2->product_id;
							$items_list .= $units.' X '.$procedure_name.' <span class="pull-right">'.number_format(($pos_order_item_amount*$units),2).'</span><br>';

							$total_amount += $pos_order_item_amount*$units;
						endforeach;
						$items_list .= '<br><strong>Total <span class="pull-right">'.number_format(($total_amount),2).'</span></strong><br>';
					}
					// if($order_invoice_id > 0)
					// {
						// $color = 'success';
					// }
					// else
					// {
					// 	$color = 'warning';
					// }

					if($table_id > 0)
					{
						$sale = 'Table :'.$table_id;
					}
					$x++;
					$complete_result .='<table class="table table-condensed table-bordered" onclick="get_order_detail('.$pos_order_id.','.$order_invoice_id.')">
									<thead>
										<tr class="'.$color.'">
											<th>Title</th>
											<th>Description : '.$name.'</th>
										</tr>
									</thead>
									<tbody>';
					$complete_result .='
										<tr >

											<th >Date</th>
											<td >'.date('jS M Y',strtotime($order_date)).' # '.$pos_order_number.'</td>
											
										</tr>';
			
					$complete_result .='
										<tr >
											<td colspan="2">'.$items_list.'</td>
										</tr>';
					$complete_result .='
										<tr >

											<th >Waiter</th>
											<td >'.$personnel_fname.'</td>
										</tr>';
					$complete_result .='
										<tr >

											<th >Type</th>
											<td >'.$sale.'</td>
										</tr>';
					$complete_result .='
										<tr >

											<th >Customer</th>
											<td >'.strtoupper($name).'</td>
										</tr>';
					$complete_result .='</tbody>
							</table>';

				}
			}

			echo $complete_result;
			?>
		</div>
		
	</div>
	<?php
	}
	

	?>
	
	
	
</div>


<script type="text/javascript">
	$(document).ready(function(){
   		

   		get_sales_breakdown();
   	

   });
	function get_sales_breakdown()
	{
			var config_url = $('#config_url').val();

			var data_url = config_url+"pos/get_sales_breakdown";
			// window.alert(data_url);
			$.ajax({
			type:'POST',
			url: data_url,
			data:{vat_status: 1},
			dataType: 'text',
			success:function(data){

				var data = jQuery.parseJSON(data);
					
						$("#pending_orders").html(data.total_pending);
						$("#completed_orders").html(data.total_completed);



				},
				error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

			});
	}
</script>

