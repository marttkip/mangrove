<?php
$categories_rs = $this->pos_model->get_product_categories();

$categories_result ='';
if($categories_rs->num_rows() > 0)
{
	foreach ($categories_rs->result() as $key => $value) {
		# code...

		$service_name = $value->service_name;
		$service_id = $value->service_id;

		$categories_result .= '<div class="col-print-3" onclick="get_procedures_done('.$service_id.')">
				        			<a  class="btn btn-lg btn-success"> '.strtoupper($service_name).'</a>
				        		</div>';
	}
}

echo $categories_result;
?>