<?php
$categories_result ='';

// $categories_result ='
// 						<table class="table table-bordered ">
// 							<thead>
// 								<th>#</th>
// 								<th>Name</th>
// 								<th>Category</th>
// 								<th>Code</th>
// 								<th>Vehicle Name</th>
// 								<th>Vehicle Model</th>
// 								<th>Part No.</th>
// 								<th>Price</th>
// 							</thead>
// 							<tbody>';
if($query->num_rows() > 0)
{
	$x=0;
	foreach ($query->result() as $key => $value) {
		# code...

		$service_charge_name = $value->service_charge_name;
		$service_charge_amount = $value->service_charge_amount;
		$service_charge_id = $value->service_charge_id;
		$product_code = $value->product_code;
		$category_name = $value->category_name;
		$part_no = $value->part_no;
		$vehicle_name = $value->vehicle_name;
		$vehicle_model = $value->vehicle_model;
		$product_id = $value->product_id;
		$service_charge_statuses_id = $value->service_charge_statuses_id;

		$checked = '';
		$type = 'danger';
		if(!empty($service_charge_statuses_id))
		{
			$type = 'danger';
			$checked = '';
			$categories_result .= '<div class="col-print-3" >
				        			<a  class="btn btn-lg btn-'.$type.'" '.$checked.' style="height: 80px !important;"> 
				        				'.strtoupper($service_charge_name).'<br>
				        				KES. '.number_format($service_charge_amount,2).'</a>
				        		</div>';
		}
		else
		{
			$type = 'primary';
			$checked = 'onclick="add_service_charge_test('.$service_charge_id.','.$service_charge_amount.')"';
			$categories_result .= '<div class="col-print-3" >
				        			<a  class="btn btn-lg btn-'.$type.'" '.$checked.' style="height: 80px !important;"> 
				        				'.strtoupper($service_charge_name).'<br>
				        				KES. '.number_format($service_charge_amount,2).'</a>
				        		</div>';
		}

		$x++;
		// $categories_result .= '<tr onclick="add_service_charge_test('.$service_charge_id.','.$product_id.','.$service_charge_amount.');">
		// 							<td>'.$x.'</td>
		// 		        			<td>'.$service_charge_name.'</td>
		// 		        			<td>'.$category_name.'</td>
		// 		        			<td>'.$product_code.' </td>
		// 		        			<td>'.ucfirst($vehicle_name).'</td>
		// 		        			<td>'.ucfirst($vehicle_model).'</td>
		// 		        			<td>'.$part_no.' </td>
		// 		        			<td>Kes. '.number_format($service_charge_amount,2).'</td>
		// 		        		</tr>';

		
	}
}
// $categories_result .='</tbody>
// 				</table>';
echo $categories_result;
?>