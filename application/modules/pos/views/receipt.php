<?php



$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y');

//doctor
// $doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
$served_by = $this->pos_model->get_personnel($this->session->userdata('personnel_id'));
// var_dump($served_by);die();
if(empty($served_by))
{
    $served_by = '&nbsp;';
}
// $credit_note_amount = $this->accounts_model->get_sum_credit_notes($visit_id);
// $debit_note_amount = $this->accounts_model->get_sum_debit_notes($visit_id);

$payments_rs = $this->pos_model->get_pos_order_payment_details($order_invoice_id);

// var_dump($payments_rs);die();
$total_payments = 0;
$invoices = '';
$confirm_number = '';
if($payments_rs->num_rows() > 0){
    $x = $s;
    foreach ($payments_rs->result() as $key_items):
        
        $payment_method = $key_items->payment_method;
        $amount_paid = $key_items->amount_paid;
        $payment_item_id = $key_items->payment_item_id;
        $time = $key_items->time;
        $order_invoice_id = $key_items->order_invoice_id;
        $pos_order_id = $key_items->pos_order_id;
        $payment_date = $key_items->payment_date;
        $payment_method = $key_items->payment_method;
        $transaction_code = $key_items->transaction_code;
        $payment_item_amount = $key_items->payment_item_amount;
        $order_invoice_number = $key_items->order_invoice_number;
        $amount_paidd = number_format($amount_paid,2);
        $payment_service_id = $key_items->payment_service_id;
        $confirm_number = $key_items->confirm_number;
        $change = $key_items->change;

        $total_payments += $payment_item_amount;

        if(!empty($order_invoice_id))
        {
            $invoices .= $order_invoice_number.' ';
        }
        // var_dump($payment_item_amount);die();
      
    endforeach;
}
$today = date('d/m/Y',strtotime($payment_date));

$visit_rs = $this->pos_model->get_order_details($pos_order_id);

$sale_type = 0;
$pos_order_status = 1;
$vat_status = 0;
if($visit_rs->num_rows() > 0)
{
    foreach ($visit_rs->result() as $key => $value) {
        # code...
        $sale_type = $value->sale_type;
        $pos_order_status = $value->pos_order_status;
        $vat_status = $value->vat_status;
    }
}




?>


<!DOCTYPE html>
<html lang="en">

    <head>
        <title><?php echo $contacts['company_name'];?> | Receipt</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
         /*body{font-family:"Courier New", Courier, monospace}*/
        .receipt_spacing{letter-spacing:0px; font-size: 12px;}
        .center-align{margin:0 auto; text-align:center;}
        
        .receipt_bottom_border{border-bottom: #888888 medium solid;}
        .receipt_double_bottom_border{border-bottom: #888888 medium solid;border-top: #888888 medium solid;}
        .row .col-md-12 table {
            border:solid #000 !important;
            border-width:1px 0 0 1px !important;
            font-size:20px;
        }
        .row .col-md-12 th, .row .col-md-12 td {
            border:solid #000 !important;
            border-width:0 1px 1px 0 !important;
        }
        .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
        {
             padding: 2px;
             font-size: 12px;
        }
        .col-print-1 {width:8%;  float:left;}
        .col-print-2 {width:16%; float:left;}
        .col-print-3 {width:25%; float:left;}
        .col-print-4 {width:33%; float:left;}
        .col-print-5 {width:42%; float:left;}
        .col-print-6 {width:50%; float:left;}
        .col-print-7 {width:58%; float:left;}
        .col-print-8 {width:66%; float:left;}
        .col-print-9 {width:75%; float:left;}
        .col-print-10{width:83%; float:left;}
        .col-print-11{width:92%; float:left;}
        .col-print-12{width:100%; float:left;}

        .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
        .title-img{float:left; padding-left:30px;}
        img.logo{max-height:70px; margin:0 auto;}
        .pagebreak
        {
            page-break-after: always;
        }
    </style>
    </head>
    <body class="receipt_spacing" onLoad="window.print();return false;">
        <!-- <div class="row"> -->
            <div class="col-md-12 center-align receipt_bottom_border">
                <strong>
                    <h4><?php echo strtoupper($contacts['company_name']);?></h4><br/>
                  <!--   P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-Mail:<?php echo $contacts['email'];?>.<br> Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?><br/> -->
                </strong>
            </div>
        <!-- </div> -->
        
         <div class="col-md-12 center-align">
               <h4><strong>RECEIPT</strong></h4> 
            </div>

         <div class="col-md-12">
                <table class="table table-hover table-bordered table-striped" style="font-size: 20px !important">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Item</th>
                    <th>Units</th>
                    <th>Unit Price</th>
                    <th>Total</th>
                  </tr>
                  </thead>
                  <tbody>

                    <?php


                    $order_invoice_number ='';
                    $preauth_date = date('Y-m-d');
                    $preauth_amount = '';
                    
                    $visit__rs1 = $this->pos_model->get_visit_charges_charged($pos_order_id,$order_invoice_id); 
                    
                    // var_dump($pos_order_id);die();
                    // $visit_rs = $this->accounts_model->get_visit_details($visit_id);
                    $visit_type_id = 1;
                    $close_card = 3;
                    $total_amount= 0; 
                    $days = 0;
                    $count = 0;
                    $item_list = '';
                    $description_list = "";
                    $quantity_list = "";
                    $rate_list = "";
                    $amount_list = "";
                    $total= 0;  
                    $total_units= 0;
                    $number = 0;
                    $vat_charged = 0;
                    $result ='';
                    if($visit__rs1->num_rows() > 0)
                    {                       
                        foreach ($visit__rs1->result() as $key1 => $value) :
                            $v_procedure_id = $value->pos_order_item_id;
                            $procedure_id = $value->service_charge_id;
                            $product_code = $value->product_code;
                            $pos_order_item_amount = $value->pos_order_item_amount;
                            $units = $value->pos_order_item_quantity;
                            $procedure_name = $value->service_charge_name;
                            $service_id = $value->service_id;
                            $vatable = $value->vatable;
                            $product_id = $value->product_id;
                            // $order_invoice_id = $value->order_invoice_id;
                            // $visit_type_id = 1;
                            $total= $total +($units * $pos_order_item_amount);

                            if($order_invoice_id > 0)
                            {
                                $text_color = "success";
                            }
                            else
                            {
                                $text_color = 'default';
                            }

                        
                            $checked="";
                            $number++;

                            if($vatable)
                            {
                                $vat_charged += ($units * $pos_order_item_amount) * 0.16;
                            }

                            $total_units += $units;
                            $personnel_check = TRUE;
                            if($personnel_check AND $close_card == 3)
                            {
                                $checked = "<td>
                                        
                                                <a class='btn btn-xs btn-danger' href='#' onclick='delete_procedure(".$v_procedure_id.", ".$pos_order_id.")'><i class='fa fa-trash'></i></a>
                                            </td>";
                            }

                            $result .= '<tr>
                                            <td></td>
                                            <td>'.$procedure_name.'</td>
                                            <td>'.$units.'</td>
                                            <td>'.number_format($pos_order_item_amount,2).'</td>
                                            <td>'.number_format($units * $pos_order_item_amount,2).'</td>
                                         </tr>';

                            endforeach;

                    }
                    else
                    {
                        
                    }
                    
                    $preauth_amount = $total;


                    $payments_value = $total_payments;
                    $balance = $preauth_amount - $total_payments;

                    $change = $preauth_amount - $total_payments;


                    if($change < 0)
                    {
                        $balance = 0;
                        $change = -$change;
                    }
                    else
                    {
                        $balance = $change;
                    }

                    ?>
                        <?php echo $result?>

                            
                                
                       </tbody>

                        
                    </table>
            <div class="col-md-12">
                <!-- <div class="col-print-12"> -->
                    <table class="table table-bordered table-condensed">
                         <?php

                            if($vat_status)
                            {
                                ?>
                                <tr class="receipt_double_bottom_border"  style="margin-bottom: 20px; font-weight: bold;">
                         
                                    <td colspan="2">SUBTOTAL</td>
                                    <td><?php echo number_format($preauth_amount - ($preauth_amount*0.16), 2);?></td>
                                </tr>
                                <tr class="receipt_double_bottom_border"  style="margin-bottom: 20px; font-weight: bold;">
                                 
                                    <td colspan="2">VAT (16%)</td>
                                    <td><?php echo number_format(($preauth_amount*0.16), 2);?></td>
                                </tr>
                        <?php

                            }
                        ?>


                         <tr class="receipt_double_bottom_border"  style="margin-bottom: 20px; font-weight: bold;">
                         
                            <td colspan="2">TOTAL BILL</td>
                            <td><?php echo number_format($preauth_amount, 2);?></td>
                        </tr>
                        <tr class="receipt_bottom_border"  style="margin-bottom: 20px; font-weight: bold;">
                          
                            <td colspan="2">TOTAL PAYMENT(S)</td>
                            <td>(<?php echo number_format($total_payments, 2);?>)</td>
                        </tr>
                        <tr class="receipt_bottom_border" style="margin-bottom: 20px; font-weight: bold;">
                          
                            <td colspan="2">BALANCE</td>
                            <td><?php echo number_format($balance, 2);?></td>
                        </tr>
                        <tr class="receipt_bottom_border" style="margin-bottom: 20px; font-weight: bold;">
                          
                            <td colspan="2">CHANGE</td>
                            <td><?php echo number_format($change, 2);?></td>
                        </tr>
                    </table>
                <!-- </div> -->
            </div>
            
        </div>


         <div class="col-md-12" style="margin-top: 10px;">
            <div class="center-align">
                <h3>FISCAL RECEIPT</h3>
                <div class="row" style="font-style:italic; font-size:10px;">
                    <div >
                        Served by: <?php echo $served_by; ?>
                    </div>
                    <div >
                        <?php echo $today; ?> Thank you
                    </div>
                </div>
            </div>
            
        </div>
    </body>
</html>
