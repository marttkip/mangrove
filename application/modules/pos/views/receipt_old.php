<?php



$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y');

//doctor
// $doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
$served_by = $this->pos_model->get_personnel($this->session->userdata('personnel_id'));
// var_dump($served_by);die();
if(empty($served_by))
{
    $served_by = '&nbsp;';
}
// $credit_note_amount = $this->accounts_model->get_sum_credit_notes($visit_id);
// $debit_note_amount = $this->accounts_model->get_sum_debit_notes($visit_id);

$payments_rs = $this->pos_model->get_payment_details($payment_id);

// var_dump($payments_rs);die();
$total_payments = 0;
$invoices = '';
$confirm_number = '';
if($payments_rs->num_rows() > 0){
    $x = $s;
    foreach ($payments_rs->result() as $key_items):
        
        $payment_method = $key_items->payment_method;
        $amount_paid = $key_items->amount_paid;
        $payment_item_id = $key_items->payment_item_id;
        $time = $key_items->time;
        $order_invoice_id = $key_items->order_invoice_id;
        $payment_date = $key_items->payment_date;
        $payment_method = $key_items->payment_method;
        $transaction_code = $key_items->transaction_code;
        $payment_item_amount = $key_items->payment_item_amount;
        $order_invoice_number = $key_items->order_invoice_number;
        $amount_paidd = number_format($amount_paid,2);
        $payment_service_id = $key_items->payment_service_id;
        $confirm_number = $key_items->confirm_number;

        $total_payments += $payment_item_amount;

        if(!empty($order_invoice_id))
        {
            $invoices .= $order_invoice_number.' ';
        }
        // var_dump($payment_item_amount);die();
      
    endforeach;
}
$today = date('d/m/Y',strtotime($payment_date));
?>

<!DOCTYPE html>
<html lang="en">

    <head>
          <title><?php echo $contacts['company_name'];?> | Invoice</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
            body
            {
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
                font-size:15px;                
            }
            .receipt_spacing{letter-spacing:0px; font-size: 13px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            
            .col-md-6 {
                width: 50%;
             }
        
            h3
            {
                font-size: 30px;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{ margin:0 auto;}
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {width:16%; float:left;}
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}

            table.table{
                border-spacing: 0 !important;
                font-size:15px;
                margin-top:10px;
                border-collapse: inherit !important;
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
            }
            th, td {
                border: 2px solid #000 !important;
                padding: 0.5em 1em !important;
            }
            /* the first 'th' within the first 'tr' of the 'thead': */
            thead tr:first-child th:first-child {
                border-radius: 20px 0 0 0 !important;
            }
            /* the last 'th' within the first 'tr' of the 'thead': */
            thead tr:first-child th:last-child {
                border-radius: 0 20px 0 0 !important;
            }
            /* the first 'td' within the last 'tr' of the 'tbody': */
            tbody tr:last-child td:first-child {
                border-radius: 0 0 0 20px !important;
            }
            /* the last 'td' within the last 'tr' of the 'tbody': */
            tbody tr:last-child td:last-child {
                border-radius: 0 0 20px 20px !important;
            }
            tbody tr:first-child td:first-child {
                border-radius: 20px 20px 0 0 !important;
                border-bottom: 0px solid #000 !important;
            }
            .padd
            {
                padding:10px;
            }
            
        </style>
    </head>
    <body class="receipt_spacing">
        <div class="padd">
             <div class="row">
                <div class="col-md-12">
                    <div class="col-print-6" style="text-align: left;">
                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive"/>
                    </div>
                    <div class="col-print-6 " style="text-align: right;">
                        <strong>
                            <?php echo $contacts['company_name'];?><br/>
                            P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                            E-Mail:<?php echo $contacts['email'];?>.<br> Tel : <?php echo $contacts['phone'];?><br/>
                            <?php echo $contacts['location'];?><br/>
                        </strong>
                    </div>
                    
                </div>
                <div class="row" style="margin-bottom: 20px;">
                    <div class="col-md-12 center-align">
                        <h3>RECEIPT</h3>
                        FOR PROFESSIONAL SERVICES RENDERED
                    </div>
                </div>
                <div class="col-md-12" style="margin-bottom: 10px;">
                    <div class="col-print-9" style="text-align: left;">
                       &nbsp;

                    </div>
                     <div class="col-print-3" style="text-align: left;">
                           <div class="form-group">
                            <label class="col-print-2 control-label">Date: </label>
                            <div class="col-print-10" style="border-bottom: 1px solid #000 !important;">
                                 <?php echo $today; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="col-md-12" style="margin-bottom: 10px;">
                    <div class="col-print-9" style="text-align: left;">
                        <div class="form-group">
                            <label class="col-print-2 control-label">Received For: </label>
                            <div class="col-print-10" style="border-bottom: 1px solid #000 !important;">
                                  <?php echo $order_invoice_number; ?>
                            </div>
                        </div>

                    </div>
                     <div class="col-print-3" style="text-align: left;">
                           <div class="form-group">
                            <label class="col-print-2 control-label">File No: </label>
                            <div class="col-print-10" style="border-bottom: 1px solid #000 !important;">
                                 <?php echo $patient_number; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="col-md-12" style="margin-bottom: 10px;">
                    <div class="col-print-12" style="text-align: left;">
                        <div class="form-group">
                            <label class="col-print-2 control-label">The sum of Kenya Shillings  </label>
                            <div class="col-print-10" style="border-bottom: 1px solid #000 !important;">
                                 <?php echo $this->accounts_model->convert_number($total_payments);?>
                            </div>
                        </div>

                    </div>
                </div>
                 <br/>
                <div class="col-md-12" style="margin-bottom: 10px;">
                    <div class="col-print-8" style="text-align: left;">
                        <div class="form-group">
                            <label class="col-print-2 control-label">being payment for   </label>
                            <div class="col-print-10" style="border-bottom: 1px solid #000 !important;text-align: center">
                                 <?php echo 'Dental Services'?>
                            </div>
                        </div>

                    </div>
                    <div class="col-print-4" style="text-align: left;">
                        <div class="form-group">
                            <label class="col-print-2 control-label">Invoice No.   </label>
                            <div class="col-print-10" style="border-bottom: 1px solid #000 !important;text-align: center">
                                 <?php echo $invoices?>
                            </div>
                        </div>

                    </div>
                </div>
                
            </div>
            
            <div class="row" style="margin-top: 10px">
                <div class="col-md-12">
                    <div class="col-print-4">
                         <div class="form-group">
                            <label class="col-print-2 control-label">KShs.   </label>
                            <div class="col-print-10" style="border-bottom: 1px solid #000 !important;text-align: center">
                                 <?php echo number_format($total_payments,2);?>

                            </div>
                            <div style="text-align: center">
                                <?php echo $payment_method.' '.$transaction_code;?>
                            </div>
                            <div style="text-align: lefts">
                                No. <span style="font-size: 20px;"><strong><?php echo $confirm_number;?></strong> </span>
                            </div>
                        </div>
                    </div>
                     <div class="col-print-4" style="text-align: center">

                        <div class="form-group">
                           <div> WITH THANKS.  </div>
                            <div class="col-print-12" style="border-bottom: 1px solid #000 !important;text-align: center;margin-top: 20px;">
                                 <?php echo $served_by; ?>
                            </div>
                        </div>
                        
                        
                    </div>
                     <div class="col-print-4">
                        
                    </div>
                </div>
                
            </div>
            
        
        </div>
       
    </body>
    
</html>