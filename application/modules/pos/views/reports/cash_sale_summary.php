<div class="row">
	<div class="col-md-12" style="margin-bottom: 10px;">
		<a href="<?php echo site_url().'print-cash-sale-summary'?>" target="_blank"  class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print List</a>
		<a href="<?php echo site_url().'export-cash-sale-summary'?>" target="_blank" class="btn btn-sm btn-success"><i class="fa fa-export"></i> Export List</a>
	</div>
	<div class="col-md-12">
		
		<?php

		$incomplete_result ='<table class="table table-condensed table-bordered table-hover">
								<thead>
									<th>#</th>
									<th>Date</th>
									<th>Amount</th>

								</thead>
								<tbody>';

		if($query->num_rows() > 0)
		{
			$x=0;
			foreach ($query->result() as $key => $value) {
				# code...
				$order_date = $value->order_date;
				$total_amount = $value->total_amount;


				$x++;
				$incomplete_result .='
									<tr>
										<td>'.$x.'</td>
										<td>'.date('jS M Y',strtotime($order_date)).'</td>
										<td>'.number_format($total_amount,2).'</td>
									</tr>';

			}
			$incomplete_result .='
									<tr>
										<th></th>
										<th>Total</th>
										<th>'.number_format($total_amount,2).'</th>
									</tr>';
		}

		$incomplete_result .='</tbody>
						</table>';

		echo $incomplete_result;
						
					

		?>
	
		
	</div>
</div>