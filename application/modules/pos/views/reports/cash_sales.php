<div style="padding:0px">
<div class="col-md-12">
	<div class="col-md-2">
		<div class="col-md-12" style="margin-bottom: 10px;">
			<div class="panel-body" style="height: 80vh;">

				<?php echo $this->load->view("search/cash_sale_search", '', TRUE);?>
			</div>
		</div>

		<div class="col-md-12" style="display: none;">
			<div class="panel-body panel-success" style="height: 15vh;margin:0 auto;">
				<div id="sales-totals" class="alert alert-info center-align"> 
					<h3>Total Sales</h3>

					<h4>Kes. <?php echo number_format($total_amount,2);?></h4>
				</div>
			</div>
		</div>
		
		
	</div>
	<div class="col-md-6" >
		<div class="col-md-12" style="margin-top: 10px;margin-bottom: 10px;">
			<div class="col-md-4" >
				<!-- <a href="<?php echo site_url().'print-cash-sale'?>" target="_blank"  class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print List</a> -->
				<a href="<?php echo site_url().'export-cash-sale'?>" target="_blank"  class="btn btn-sm btn-success"><i class="fa fa-export"></i> Export List</a>
			</div>
			<div class="col-md-8">
				<h4 class="center-align"><?php echo $title?></h4>
			</div>
			
		</div>
		<div class="col-md-12">
			<div class="panel-body" style="height: 85vh;overflow-y: scroll;padding: 0px;">
				<?php
					$incomplete_result ='<table class="table table-condensed table-bordered table-hover table-linked">
											<thead>
												<th>#</th>
												<th>Date</th>
												<th>Order Number</th>
												<th>Items</th>
												<th>Total Amount</th>
												<th>Created By</th>

											</thead>
											<tbody>';
					$grand_amount = 0;
					if($query->num_rows() > 0)
					{
						$x=$page;
						foreach ($query->result() as $key => $value) {
							# code...
							$pos_order_id = $value->pos_order_id;
							$customer_id = $value->customer_id;
							$order_date = $value->order_date;
							$pos_order_number = $value->pos_order_number;
							$personnel_fname = $value->personnel_fname;
							$total_amount = $value->total_amount;
							$order_invoice_id = $value->order_invoice_id;
							$count = $value->total_items;

							if(empty($order_invoice_id))
								$order_invoice_id = 0;
							$x++;
							$incomplete_result .='
												<tr onclick="get_order_detail('.$pos_order_id.','.$order_invoice_id.')">
													<td>'.$x.'</td>
													<td>'.date('jS M Y',strtotime($order_date)).'</td>
													<td>'.$pos_order_number.'</td>
													<td>'.$count.'</td>
													<td>'.number_format($total_amount,2).'</td>
													<td>'.$personnel_fname.'</td>
												</tr>';

							$grand_amount += $total_amount;

						}
					}
					$incomplete_result .= '</tbody>
											<tfoot>
												<tr>
													<th colspan="4">TOTAL</th>
													
													<th>'.number_format($grand_amount,2).'</th>
													<th></th>
												</tr>
											</tfoot>';
					$incomplete_result .='
									</table>';

					echo $incomplete_result;
					
				?>
			</div>
			<div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
		</div>
		
	</div>
	<div class="col-md-4">
		<div class="row">
			<div class="panel-body" style="height: 90vh;margin:0 auto;">
				<div id="sale-detail"></div>
			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
	
	function get_order_detail(pos_order_id,order_invoice_id)
	{

		var config_url = $('#config_url').val();

		var data_url = config_url+"pos/pos_reports/view_sold_items/"+pos_order_id+"/"+order_invoice_id;
		
		
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){
			$("#sale-detail").html(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});


	}

</script>