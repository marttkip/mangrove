<div class="row">
	<div class="col-md-2">
		<div class="row" style="margin-bottom: 10px;">
			<div class="panel-body" style="height: 68vh;">

				<?php echo $this->load->view("search/credit_sale_item_list_search", '', TRUE);?>
			</div>
		</div>

		
		
	</div>
	<div class="col-md-10" >
		<div class="col-md-12" style="margin-top: 10px;margin-bottom: 10px;">
			<div class="col-md-4" >
				<a href="<?php echo site_url().'print-credit-sale-list-items'?>" target="_blank"  class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print List</a>
				<a href="<?php echo site_url().'export-credit-sale-list-items'?>" target="_blank"  class="btn btn-sm btn-success"><i class="fa fa-export"></i> Export List</a>
			</div>
			<div class="col-md-8">
				<h4 class="center-align"><?php echo $title?></h4>
			</div>
			
		</div>
		
		<div class="col-md-12">
			<div class="panel-body" >
				<?php
					$incomplete_result ='<table class="table table-condensed table-bordered table-hover">
											<thead>
												<th>#</th>
												<th>Invoice Number</th>
												<th>Item</th>
												<th>Units</th>
												<th>Unit Charge</th>
												<th>Amount Charged</th>

											</thead>
											<tbody>';

					if($query->num_rows() > 0)
					{
						$x=$page;
						foreach ($query->result() as $key => $value) {
							# code...
							$order_invoice_number = $value->order_invoice_number;
							$units = $value->pos_order_item_quantity;
							$procedure_name = $value->service_charge_name;
							$amount = $value->pos_order_item_amount;
							// $visit_invoice_id = $value->visit_invoice_id;
							// $visit_type_id = 1;
							

							$total_amount = $units * $amount;
							// $total= $total +($units * $amount);

							$x++;
							$incomplete_result .='
												<tr >
													<td>'.$x.'</td>
													<td>'.$order_invoice_number.'</td>
													<td>'.$procedure_name.'</td>
													<td>'.$units.'</td>
													<td>'.number_format($amount,2).'</td>
													<td>'.number_format($total_amount,2).'</td>
												</tr>';

						}
					}
					$incomplete_result .='</tbody>
									</table>';

					echo $incomplete_result;
					
				?>
			</div>
			<div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
		</div>
		
	</div>
	
</div>
<script type="text/javascript">
	
	function get_order_detail(pos_order_id,order_invoice_id)
	{

		var config_url = $('#config_url').val();

		var data_url = config_url+"pos/pos_reports/view_sold_items/"+pos_order_id+"/"+order_invoice_id;
		
		
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){
			$("#sale-detail").html(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});


	}

</script>