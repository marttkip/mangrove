<?php
	$incomplete_result ='<table class="table table-condensed table-bordered table-hover table-linked">
							<thead>
								<th>#</th>
								<th>Date</th>
								<th>Order Number</th>
								<th>Items</th>
								<th>Total Amount</th>
								<th>Created By</th>

							</thead>
							<tbody>';

	if($query->num_rows() > 0)
	{
		$x=0;
		foreach ($query->result() as $key => $value) {
			# code...
			$pos_order_id = $value->pos_order_id;
			$customer_id = $value->customer_id;
			$order_date = $value->order_date;
			$pos_order_number = $value->pos_order_number;
			$personnel_fname = $value->personnel_fname;
			$total_amount = $value->total_amount;
			$order_invoice_id = $value->order_invoice_id;
			$count = $value->total_items;


			$x++;
			$incomplete_result .='
								<tr onclick="get_my_order_detail('.$pos_order_id.','.$order_invoice_id.')">
									<td>'.$x.'</td>
									<td>'.date('jS M Y',strtotime($order_date)).'</td>
									<td>'.$pos_order_number.'</td>
									<td>'.$count.'</td>
									<td>'.number_format($total_amount,2).'</td>
									<td>'.$personnel_fname.'</td>
								</tr>';

		}
	}
	$incomplete_result .='</tbody>
					</table>';

	echo $incomplete_result;
	
?>