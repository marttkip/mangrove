<!-- search -->

<!-- end search -->
<?php //echo $this->load->view('payments_statistics', '', TRUE);?>

<?php

		$result = '';
		
		$restaurant_total = 0;
		$grills_total = 0;
		$coffee_total = 0;
		$glovo_total = 0;
		$packaging_total = 0;
		$soft_drinks_total = 0;
		$other_amount_total = 0;



		$restaurant_count = 0;
		$grills_count = 0;
		$coffee_count = 0;
		$glovo_count = 0;
		$packaging_count = 0;
		$soft_drinks_count = 0;
		$other_amount_count = 0;
//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
				'
					<table class="table table-hover table-bordered table-striped table-responsive col-md-12 table-linked">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Department</th>
						  <th>Product Name</th>
						  <th>Units Sold</th>
						  <th>Unit Price</th>
						  <th>Sold Amount</th>
						  
						</tr>
					  </thead>
					  <tbody>
			';
			$total_invoiced = 0;
			$total_paid = 0;
			$total_balance =0;
			$total_revenue = 0;
			foreach ($query->result() as $value)
			{
				$count++;
				
				$order_date = date('jS M Y',strtotime($value->order_date));
				
				$pos_order_id = $value->pos_order_id;
				$customer_id = $value->customer_id;
				$order_date = $value->order_date;
				$pos_order_number = $value->pos_order_number;
				$personnel_fname = $value->personnel_fname;
				$order_invoice_id = $value->order_invoice_id;
				$service_name = $value->service_name;
				$pos_order_item_quantity = $value->total_items;
				$pos_order_item_amount = $value->pos_order_item_amount;
				// $discount = $value->discount;
				$service_charge_name = $value->service_charge_name;
				$service_charge_id = $value->service_charge_id;
				$service_charge_amount = $value->service_charge_amount;
				$total_amount = $pos_order_item_amount * $pos_order_item_quantity;


				$minimum_amount = $service_charge_amount*$pos_order_item_quantity;

				$profit = $total_amount - $minimum_amount;
				$total_revenue += $total_amount;

				if($service_name == "Kitchen")
				{
					$restaurant_total += $total_amount;
					$restaurant_count += $pos_order_item_quantity;
				}
				else if($service_name == "Glovo")
				{
					$glovo_total += $total_amount;
					$glovo_count += $pos_order_item_quantity;
				}
				
				else if($service_name == "Grills")
				{
					$grills_total += $total_amount;
					$grills_count += $pos_order_item_quantity;
				}

				else if($service_name == "Coffee")
				{
					$coffee_total += $total_amount;
					$coffee_count += $pos_order_item_quantity;
				}
				else if($service_name == "Packaging")
				{
					$packaging_total += $total_amount;
					$packaging_count += $pos_order_item_quantity;
				}
				else if($service_name == "Soft Drinks")
				{
					$soft_drinks_total += $total_amount;
					$soft_drinks_count += $pos_order_item_quantity;
				}
				else
				{
					$other_amount_total += $total_amount;
					$other_amount_count += $pos_order_item_quantity;
				}



				$result .= 
						'
							<tr>
								<td>'.$count.'</td>
								<td>'.$service_name.'</td>
								<td>'.ucfirst(strtolower($service_charge_name)).'</td>
								<td>'.$pos_order_item_quantity.'</td>
								<td>'.number_format($pos_order_item_amount, 2).'</td>
								<td>'.number_format($total_amount, 2).'</td>
								
							</tr> 
					';
			}
			$result .= 
						'  </tbody>
							<tfoot>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							
								<th>Total Revenue</th>
								<th>'.number_format($total_revenue, 2).'</th>
								
							</tfoot> 
					';
			$result .= 
			'
						
						</table>
			';
		}
		
		else
		{
			$result .= "There are no payments";
		}
?>
 
<div class="col-md-12">
	<div class="col-md-2">
		<div class="panel-body" style="">
			<div class="col-md-12">
			<?php echo $this->load->view('search/sales', '', TRUE);?>
				
			</div>
		</div>
		<br>
		<div class="center-align">
			<?php
				$search = $this->session->userdata('sales_report_date');
				if(!empty($search))
				{
					echo '<a href="'.site_url().'pos/pos_reports/close_sales_search" class="btn btn-sm btn-warning">Close Search</a>';
				}

				?>
		</div>
		<br>

        <div class="panel-body" style="padding:0px;overflow-y: scroll; height: 50vh;">

			

			<div class="col-md-12">
				<table class="table table-hover table-condensed table-stripped table-linked">
					<thead>
						<th>Departmnt</th>
						<th>Items Sold</th>
						<th>Revenue</th>
					</thead>
					<tbody>
						<tr>
							<th>Kitchen</th>
							<th><?php echo $restaurant_count?></th>
							<td><?php echo number_format($restaurant_total,2)?></td>
						</tr>

						<tr>
							<th>Grills</th>
							<th><?php echo $grills_count?></th>
							<td><?php echo number_format($grills_total,2)?></td>
						</tr>
						<tr>
							<th>Soft Drinks</th>
							<th><?php echo $soft_drinks_count?></th>
							<td><?php echo number_format($soft_drinks_total,2)?></td>
						</tr>
						<tr>
							<th>Packaging</th>
							<th><?php echo $packaging_count?></th>
							<td><?php echo number_format($packaging_total,2)?></td>
						</tr>
						<tr>
							<th>Coffee Zone</th>
							<th><?php echo $coffee_count?></th>
							<td><?php echo number_format($coffee_total,2)?></td>
						</tr>
						<tr>
							<th>Glovo</th>
							<th><?php echo $glovo_count?></th>
							<td><?php echo number_format($glovo_total,2)?></td>
						</tr>
						<tr>
							<th>Others</th>
							<th><?php echo $other_amount_count?></th>
							<td><?php echo number_format($other_amount_total,2)?></td>
						</tr>

						<tr>
							<th>Total</th>
							<th><?php echo ($restaurant_count+$grills_count+$packaging_count+$coffee_count+$glovo_count+$soft_drinks_count)?></th>
							<th><?php echo number_format($restaurant_total+$grills_total+$packaging_total+$coffee_total+$glovo_total+$soft_drinks_total+$other_amount_total,2)?></th>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
			
	</div>

    <div class="col-md-10">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            </header>             

          <!-- Widget content -->
          <div class="panel-body" style="padding:0px;overflow-y: scroll; height: 80vh;">
<?php
		
		
		
		echo $result;
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
    </div>
  </div>