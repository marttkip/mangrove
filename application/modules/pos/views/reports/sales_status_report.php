<section class="" style="margin-top:1% !important;">
	<div class="col-md-6">
		<div class="col-md-12">
    		<input type="text" class="form-control" name="product_name" id="product_name" onkeyup="search_products()" placeholder="" autocomplete="off"  style="padding: 15px 5px !important;font-size: 12px marigin-top: !important;text-align: center;"  onfocus="this.removeAttribute('readonly');">
    	</div>
    	<div class="col-md-12 panel-body" style="height: 80vh !important;overflow-y: scroll;margin-top: 5px;">
    		<div id="product-list-item"></div>
    	</div>

	</div>
    <div class="col-md-6">
    	<div class="col-md-12 panel-body" style="height: 80vh !important;overflow-y: scroll;margin-top: 5px;">
			<div id="product-unavailable-list-item"></div>
		</div>
	</div>
</section>
<!-- end: page -->
<script type="text/javascript">
	$(document).ready(function(){
   		get_not_available_products();
  	});


	function search_products()
	{
			var config_url = $('#config_url').val();

			var product_name = $('#product_name').val();

			var data_url = config_url+"pos/pos_reports/get_product_list_items_inquiry";
			// window.alert(data_url);

			$.ajax({
			type:'POST',
			url: data_url,
			data:{description: product_name},
			dataType: 'text',
		   success:function(data){
				// alert(data);
				//window.alert("You have successfully updated the symptoms");
				//obj.innerHTML = XMLHttpRequestObject.responseText;
				$("#product-list-item").html(data);
				// alert(data);
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
			}

			});
	}

	function mark_not_available(service_charge_id)
	{

		var config_url = $('#config_url').val();


			var data_url = config_url+"pos/pos_reports/mark_not_available/"+service_charge_id;
			// window.alert(data_url);

			$.ajax({
			type:'POST',
			url: data_url,
			data:{description: 1},
			dataType: 'text',
		   success:function(data){
		   	get_not_available_products();
				
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
			}

			});

	}

	function unmark_not_available(service_charge_id)
	{

		var config_url = $('#config_url').val();


			var data_url = config_url+"pos/pos_reports/unmark_not_available/"+service_charge_id;
			// window.alert(data_url);

			$.ajax({
			type:'POST',
			url: data_url,
			data:{description: 1},
			dataType: 'text',
		   success:function(data){
				get_not_available_products();
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
			}

			});

	}

	function get_not_available_products()
	{
			var config_url = $('#config_url').val();


			var data_url = config_url+"pos/pos_reports/get_all_unavailable_products";
			// window.alert(data_url);

			$.ajax({
			type:'POST',
			url: data_url,
			data:{description: 1},
			dataType: 'text',
		   success:function(data){
				// alert(data);
				//window.alert("You have successfully updated the symptoms");
				//obj.innerHTML = XMLHttpRequestObject.responseText;
				$("#product-unavailable-list-item").html(data);
				// alert(data);
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
			}

			});
	}
</script>