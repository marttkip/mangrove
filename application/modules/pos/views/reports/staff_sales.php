
 
<div class="col-md-12">
	<div class="col-md-3">
		<div class="panel-body" style="">
			<div class="col-md-12">
			<?php echo $this->load->view('search/staff_sales', '', TRUE);?>
				
			</div>
		</div>
		<br>
		<div class="center-align">
			<?php
				$search = $this->session->userdata('staff_sales_report_date');
				if(!empty($search))
				{
					echo '<a href="'.site_url().'pos/pos_reports/close_staff_sales_search" class="btn btn-sm btn-warning">Close Search</a>';
				}

				?>
		</div>
		<br>

        <div class="panel-body" style="padding:0px;overflow-y: scroll; height: 50vh;">
				<table class="table table-hover table-condensed table-stripped table-linked">
					<thead>
						<th>COUNT</th>
						<th>STAFF NAME</th>
						<th>TOTAL ITEMS SOLD</th>
						<th>REVENUE</th>
					</thead>
					<tbody>
						<?php
						$count = 0;
						$total_items_number = 0;
						$total_revenue_number = 0;
						$result = '';
						if($query->num_rows() > 0)
						{
							foreach ($query->result() as $key => $value) {
								// code...
								$personnel_id = $value->personnel_id;
								$personnel_fname = $value->personnel_fname;
								$personnel_onames = $value->personnel_onames;
								$total_revenue = $value->total_revenue;
								$total_items = $value->total_items;

								$total_items_number += $total_items;
								$total_revenue_number += $total_revenue;
								$count++;
								$result .= '<tr>
												<td onclick="get_staff_sales('.$personnel_id.')">'.$count.'</td>
												<td onclick="get_staff_sales('.$personnel_id.')">'.$personnel_fname.'</td>
												<td onclick="get_staff_sales('.$personnel_id.')">'.$total_items.'</td>
												<td onclick="get_staff_sales('.$personnel_id.')">'.number_format($total_revenue,2).'</td>
											</tr>';
							}
						}
							$result .= '	</tbody>
										<tfoot>
											<tr>
												<th>TOTAL</th>
												<th></th>
												<th>'.$total_items_number.'</th>
												<th>'.number_format($total_revenue_number,2).'</th>
											</tr>
										</tfoot>';


						echo $result;
						?>
				
				</table>
		</div>
			
	</div>

    <div class="col-md-9">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            </header>             

          <!-- Widget content -->
          <div class="panel-body" style="padding:0px;overflow-y: scroll; height: 80vh;">
			<div id="staff-sales-items"></div>
          </div>
          
         
        
		</section>
    </div>
  </div>

  <script type="text/javascript">
  	function get_staff_sales(staff_id)
  	{
		var config_url = $('#config_url').val();

		var data_url = config_url+"pos/pos_reports/staff_sales_report";
		
		
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{personnel_id : staff_id},
		dataType: 'text',
		success:function(data){

			var data = jQuery.parseJSON(data);

			$("#staff-sales-items").html(data.content);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	

  	}
  </script>