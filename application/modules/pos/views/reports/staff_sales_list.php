<?php
$result = '';
		
$restaurant_total = 0;
$grills_total = 0;
$coffee_total = 0;
$glovo_total = 0;
$packaging_total = 0;
$soft_drinks_total = 0;
$other_amount_total = 0;



$restaurant_count = 0;
$grills_count = 0;
$coffee_count = 0;
$glovo_count = 0;
$packaging_count = 0;
$soft_drinks_count = 0;
$other_amount_count = 0;
//if users exist display them
if ($query->num_rows() > 0)
{
	$count = 0;
	
	$result .= 
		'
			<table class="table table-hover table-bordered table-striped table-responsive col-md-12 table-linked">
			  <thead>
				<tr>
				  <th>#</th>
				  <th>Department</th>
				  <th>Product Name</th>
				  <th>Units Sold</th>
				  <th>Unit Price</th>
				  <th>Sold Amount</th>
				  
				</tr>
			  </thead>
			  <tbody>
	';
	$total_invoiced = 0;
	$total_paid = 0;
	$total_balance =0;
	$total_revenue = 0;
	foreach ($query->result() as $value)
	{
		$count++;
		
		$order_date = date('jS M Y',strtotime($value->order_date));
		
		$pos_order_id = $value->pos_order_id;
		$customer_id = $value->customer_id;
		$order_date = $value->order_date;
		$pos_order_number = $value->pos_order_number;
		$personnel_fname = $value->personnel_fname;
		$order_invoice_id = $value->order_invoice_id;
		$service_name = $value->service_name;
		$pos_order_item_quantity = $value->total_items;
		$pos_order_item_amount = $value->pos_order_item_amount;
		// $discount = $value->discount;
		$service_charge_name = $value->service_charge_name;
		$service_charge_id = $value->service_charge_id;
		$service_charge_amount = $value->service_charge_amount;
		$total_amount = $pos_order_item_amount * $pos_order_item_quantity;


		$minimum_amount = $service_charge_amount*$pos_order_item_quantity;

		$profit = $total_amount - $minimum_amount;
		$total_revenue += $total_amount;

		if($service_name == "Kitchen")
		{
			$restaurant_total += $total_amount;
			$restaurant_count += $pos_order_item_quantity;
		}
		else if($service_name == "Glovo")
		{
			$glovo_total += $total_amount;
			$glovo_count += $pos_order_item_quantity;
		}
		
		else if($service_name == "Grills")
		{
			$grills_total += $total_amount;
			$grills_count += $pos_order_item_quantity;
		}

		else if($service_name == "Coffee")
		{
			$coffee_total += $total_amount;
			$coffee_count += $pos_order_item_quantity;
		}
		else if($service_name == "Packaging")
		{
			$packaging_total += $total_amount;
			$packaging_count += $pos_order_item_quantity;
		}
		else if($service_name == "Soft Drinks")
		{
			$soft_drinks_total += $total_amount;
			$soft_drinks_count += $pos_order_item_quantity;
		}
		else
		{
			$other_amount_total += $total_amount;
			$other_amount_count += $pos_order_item_quantity;
		}



		$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$service_name.'</td>
						<td>'.ucfirst(strtolower($service_charge_name)).'</td>
						<td>'.$pos_order_item_quantity.'</td>
						<td>'.number_format($pos_order_item_amount, 2).'</td>
						<td>'.number_format($total_amount, 2).'</td>
						
					</tr> 
			';
	}
	$result .= 
				'  </tbody>
					<tfoot>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					
						<th>Total Revenue</th>
						<th>'.number_format($total_revenue, 2).'</th>
						
					</tfoot> 
			';
	$result .= 
	'
				
				</table>
	';
}

else
{
	$result .= "There are no payments";
}
echo $result
?>