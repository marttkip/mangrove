<?php


$all_tables = $this->pos_model->get_all_tables();

$table_zero_result = $this->pos_model->get_table_occupations(0);
$table_zero_class = $table_zero_result['table_status'];
$table_zero_waiter = $table_zero_result['table_waiter'];

$tables_result = '<div class="col-md-2 btn btn-lg btn-primary" style="height:15vh;margin-bottom:2px;margin-left:0px" onclick="create_new_table_order(0)">
                                TAKE AWAY<br>
                                 - <br>
                               SHKJAHDA
                            </div>';
if($all_tables->num_rows() > 0)
{
    foreach ($all_tables->result() as $key => $value) {
        // code...

        $customer_table_id = $value->customer_table_id;
        $customer_table_number = $value->customer_table_number;
        $customer_table_name = $value->customer_table_name;

        $table_twenty_three_result = $this->pos_model->get_table_occupations($customer_table_id);
        $table_twenty_three_class = $table_twenty_three_result['table_status'];
        $table_twenty_three_waiter = $table_twenty_three_result['table_waiter'];

        $tables_result .= '<div class="col-md-2 btn btn-lg btn-'.$table_twenty_three_class.'" style="height:15vh;margin-bottom:2px;margin-left:0px" onclick="create_new_table_order('.$customer_table_id.')">
                                '.$customer_table_number.'<br>
                                - <br>
                                '.$table_twenty_three_waiter.'
                            </div>';


    }
}
 $tables_result .= '<div class="col-md-3 btn btn-lg btn-danger" style="height:15vh;margin-bottom:2px;margin-left:0px" onclick="close_side_bar()">
                               BACK TO ORDERS
                            </div>';
?>
<div class="row">
<section class="panel">
    <!-- <header class="panel-heading"> -->
            <!-- <h5 class="pull-left"><i class="icon-reorder"></i>NEW SALE</h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div> -->
    <!-- </header> -->
    <div class="panel-body">
        <?php
                echo form_open("finance/creditors/confirm_invoice_note", array("class" => "form-horizontal","id" => "create-order"));
                ?>
        <div class="padd">
        	
            <div  style="padding:10px;height: 80vh;overflow-y: scroll;">
                  <div class="col-md-12">
                        <?php echo $tables_result;?>
                    </div>
            	
                
               <input type="hidden" name="table_id" id="table_id">
                 <input type="hidden" name="sale_type" id="sale_type" value="<?php echo $sale_type?>">
				
            </div>
             <div class="center-align" style="bottom:0px !important;">
                <input type="submit" value="Create Order" class="btn btn-info btn-sm"/>
            </div>

            
        </div>
        </form>
    </div>
</section>
<div class="col-md-12">                             
    <div class="center-align">
        <a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
    </div>  
</div>
</div>