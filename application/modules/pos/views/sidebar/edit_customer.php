<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>
    <div class="panel-body">
    	<div class="row" style="margin-bottom:20px;">
            <div class="col-lg-12">
                <a href="<?php echo site_url();?>customers" class="btn btn-info pull-right">Back to customers</a>
            </div>
        </div>
            
        <!-- Adding Errors -->
        <?php
			$success = $this->session->userdata('success_message');
			$error = $this->session->userdata('error_message');
			
			if(!empty($success))
			{
				echo '
					<div class="alert alert-success">'.$success.'</div>
				';
				
				$this->session->unset_userdata('success_message');
			}
			
			if(!empty($error))
			{
				echo '
					<div class="alert alert-danger">'.$error.'</div>
				';
				
				$this->session->unset_userdata('error_message');
			}

			$validation_errors = validation_errors();
			
			if(!empty($validation_errors))
			{
				echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
			}
        
			$validation_errors = validation_errors();
			
			if(!empty($validation_errors))
			{
				echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
			}


			$row = $customer->row();
			$customer_name = $row->customer_name;
			$opening_balance = $row->opening_balance;
			$start_date = $row->start_date;
			$debit_id = $row->debit_id;
			$contact_person = $row->contact_person;
			$address = $row->address;
			$customer_email = $row->customer_email;
			$customer_phone = $row->customer_phone;
			$pin_number = $row->pin_number;
			$vat_number = $row->vat_number;
			$postal_address = $row->postal_address;
			$debit_id = $row->debit_id;
			$start_date = $row->start_date;

			if($debit_id == 0)
			{
				$prepayment_checked = '';
				$debt_checked = 'checked';
			}
			else
			{
				$prepayment_checked = 'checked';
				$debt_checked = '';

			}
        ?>

		<?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>

			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6">
						<div class="form-group">
		                    <label class="col-md-4 control-label">Customer Name *: </label>
		                    
		                    <div class="col-md-8">
		                        <input type="text" class="form-control" name="customer_name" placeholder="Customer name" value="<?php echo $customer_name;?>" required="required" autocomplete="off">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label class="col-md-4 control-label"> Start Date : </label>
		                    
		                    <div class="col-md-8">
		                       
		                        <div class="input-group">
				                    <span class="input-group-addon">
				                        <i class="fa fa-calendar"></i>
				                    </span>
				                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="start_date" placeholder="Start Date" value="<?php echo $start_date?>">
				                </div>
		                    </div>
		                </div>
		                <div class="form-group">
				            <label class="col-md-4 control-label">Opening Balance: </label>
				            
				            <div class="col-md-8">
				                <input type="text" class="form-control" name="opening_balance" placeholder="Opening Balance" value="<?php echo $opening_balance;?>">
				            </div>
				        </div>
				        <div class="form-group">
							<label class="col-lg-4 control-label">Prepayment ?</label>
							<div class="col-md-4">
								<div class="radio">
									<label>
									<input id="optionsRadios5" type="radio" value="1" name="debit_id" <?php echo $prepayment_checked?>>
									Yes
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="radio">
									<label>
									<input id="optionsRadios6" type="radio" value="0" name="debit_id" <?php echo $debt_checked?>>
									No
									</label>
								</div>
							</div>
						</div>
		               
		                <div class="form-group">
		                    <label class="col-md-4 control-label"> PIN Number *: </label>
		                    
		                    <div class="col-md-8">
		                        <input type="text" class="form-control" name="pin_number" placeholder="PIN number" value="<?php echo $pin_number;?>" required="required" autocomplete="off">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label class="col-md-4 control-label"> VAT Number *: </label>
		                    
		                    <div class="col-md-8">
		                        <input type="text" class="form-control" name="vat_number" placeholder="VAT Number" value="<?php echo $vat_number;?>"  autocomplete="off">
		                    </div>
		                </div>

		               
		                
					</div>
					<div class="col-md-6">
						 <div class="form-group">
		                    <label class="col-md-4 control-label"> Address : </label>
		                    
		                    <div class="col-md-8">
		                        <input type="text" class="form-control" name="address" placeholder="Address" value="<?php echo $address;?>"  autocomplete="off">
		                    </div>
		                </div>
						<div class="form-group">
		                    <label class="col-md-4 control-label"> P.O Box: </label>
		                    
		                    <div class="col-md-8">
		                        <input type="text" class="form-control" name="postal_address" placeholder="Postal Address" value="<?php echo $postal_address;?>"  autocomplete="off">
		                    </div>
		                </div>
						<div class="form-group">
		                    <label class="col-md-4 control-label">Customer Email *: </label>
		                    <div class="col-md-8">
		                        <input type="text" class="form-control" name="customer_email" placeholder="Customer Email" value="<?php echo $customer_email;?>" required="required" autocomplete="off">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label class="col-md-4 control-label"> Telephone Number *: </label>
		                    
		                    <div class="col-md-8">
		                        <input type="text" class="form-control" name="customer_phone" placeholder="Customer Phone" value="<?php echo $customer_phone;?>" required="required" autocomplete="off">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label class="col-md-4 control-label"> Contact Person *: </label>
		                    
		                    <div class="col-md-8">
		                        <input type="text" class="form-control" name="contact_person" placeholder="Customer name" value="<?php echo $contact_person;?>"  autocomplete="off">
		                    </div>
		                </div>

						
					</div>
				</div>
				<div class="col-md-12" style="margin-top:30px;">
					<div class="form-group">
		                <div class="col-md-12 center-align">
		                    <button class='btn btn-success btn-sm' type='submit' onclick="confirm('Are you sure you want to edit the customer details? ')"  > Edit Customer</button>
		                </div>
		            </div>
					
				</div>
			</div>

		<?php echo form_close();?>
	</div>
</section>
