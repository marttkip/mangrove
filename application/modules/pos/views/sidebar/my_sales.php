<?php


?>

<div class="row">
<section class="panel">

    <div class="panel-body" style="height: 87vh;">
        <?php
                echo form_open("finance/creditors/confirm_invoice_note", array("class" => "form-horizontal","id" => "create-order"));
                ?>
	        <div class="padd">
	        	
	            <div  style="padding:10px;height: 80vh;overflow-y: scroll;">
	                <div class="col-md-12">
	                	<div class="col-md-4">
	                		<div class="col-md-12">
	                			<div class="col-md-12">
			                		<div class="form-group">
			                			<a  class="btn btn-lg btn-success col-md-12" onclick="get_period_sales(1)"> Today </a>
			                		</div>
		                			
		                			
		                			<div class="form-group">
			                			<a  class="btn btn-lg btn-warning col-md-12" onclick="get_period_sales(2)"> Yesterday </a>
			                		</div>
		                			
		                			<div class="form-group">
			                			<a   class="btn btn-lg btn-primary col-md-12" onclick="get_period_sales(3)"> This Week </a>
			                		</div>
			                			
			                		<div class="form-group">
		                				<a   class="btn btn-lg btn-default col-md-12" onclick="get_period_sales(4)"> This Month </a>
		                			</div>
		                			
		                			<div class="form-group">
		                				<a   class="btn btn-lg btn-danger col-md-12" onclick="get_period_sales(5)"> Last Month </a>
		                			</div>
		                			
		                			<div class="form-group">
		                				<a   class="btn btn-lg btn-warning col-md-12" onclick="get_period_sales(6)"> This year </a>
		                			</div>
		                		</div>
	                		</div>
	                		<br>
	                		<div class="col-md-12 center-align" style="margin-top:20px">
	                			<h4 id="sales-title"></h4>
	                		</div>
	                		
	                    </div>
	                	<div class="col-md-4">
	                		<div class="panel-body" style="height: 80vh;margin:0 auto;overflow-y: scroll;">
	                			<div id="sales-items-list"></div>
	                		</div>
	                      
	                    </div>
	                    <div class="col-md-4">

	                    	<div class="row">
								<div class="panel-body" style="height: 80vh;margin:0 auto;overflow-y: scroll;">
									<div id="sale-detail-list"></div>
								</div>
							</div>
	                    </div>
	                    
	                </div>
	            	
	                
					
	            </div>
	            

	            
	        </div>
    </div>
</section>
<div class="col-md-12">                             
    <div class="center-align">
        <a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
    </div>  
</div>
</div>