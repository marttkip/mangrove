<?php

$visit_invoice_number ='';
$preauth_date = date('Y-m-d');
$preauth_amount = '';
if(!empty($visit_invoice_id))
{
	$visit_invoice_detail = $this->pos_model->get_visit_invoice_details($visit_invoice_id);

	if($visit_invoice_detail->num_rows() > 0)
	{
		foreach ($visit_invoice_detail->result() as $key => $value) {
			# code...
			$visit_invoice_number = $value->visit_invoice_number;
			$preauth_date = $value->preauth_date;
			$preauth_amount = $value->preauth_amount;

		}
	}
	$visit__rs1 = $this->pos_model->get_visit_charges_charged($visit_id,$visit_invoice_id);
}
else
{
	$visit__rs1 = $this->pos_model->get_visit_charges_charged($visit_id);	
}

// $visit_rs = $this->accounts_model->get_visit_details($visit_id);
$visit_type_id = 1;
$close_card = 3;
// if($visit_rs->num_rows() > 0)
// {
// 	foreach ($visit_rs->result() as $key => $value) {
// 		# code...
// 		$close_card = $value->close_card;
// 		$visit_type_id = $value->visit_type;
// 		$visit_time_out = $value->visit_time_out;
// 		$parent_visit = $value->parent_visit;
// 		$close_card = $value->close_card;
// 		$visit_id = $value->visit_id;
		
// 		$visit_time_out = date('jS F Y',strtotime($visit_time_out));
// 	}
// }

// get all the procedures


// var_dump($visit__rs1->result())	;die();
echo form_open("finance/creditors/confirm_invoice_note", array("class" => "form-horizontal","id" => "confirm-invoice"));
$result = "

	<table align='center' class='table table-striped table-bordered table-condensed'>
	<tr>
		<th></th>
		<th></th>
		<th>Invoice Item</th>
		<th>Units</th>
		<th>Unit Cost</th>
		<th>Total</th>
		<th></th>

	</tr>
";
	$total= 0;  
	$number = 0;
	$total_items =0;
	if($visit__rs1->num_rows() > 0)
	{						
		foreach ($visit__rs1->result() as $key1 => $value) :
			$v_procedure_id = $value->visit_charge_id;
			$procedure_id = $value->service_charge_id;
			$visit_charge_amount = $value->visit_charge_amount;
			$units = $value->visit_charge_units;
			$procedure_name = $value->service_charge_name;
			$service_id = $value->service_id;
			$visit_invoice_id = $value->visit_invoice_id;
			// $visit_type_id = 1;
			$total= $total +($units * $visit_charge_amount);

			if($visit_invoice_id > 0)
			{
				$text_color = "success";
			}
			else
			{
				$text_color = 'default';
			}

			// if($visit_type_id == 1)
			// {
			// 	$visit = 'SELF';
			// }
			// else
			// {
			// 	$visit = 'INSURANCE';
			// }
		
			$checked="";
			$number++;

			$total_items += $units;
			$personnel_check = TRUE;
			if($personnel_check AND $close_card == 3)
			{
				$checked = "<td>
							<a class='btn btn-sm btn-primary'  onclick='calculatetotal(".$visit_charge_amount.",".$v_procedure_id.", ".$procedure_id.",".$visit_id.")'><i class='fa fa-pencil'></i></a>
								<a class='btn btn-sm btn-danger' href='#' onclick='delete_procedure(".$v_procedure_id.", ".$visit_id.")'><i class='fa fa-trash'></i></a>
							</td>";
			}

			// if($close_card == 3)
			// {

			// }
		 $checkbox_data = array(
			                    'name'        => 'visit_invoice_items[]',
			                    'id'          => 'checkbox'.$v_procedure_id,
			                    'class'          => 'css-checkbox  lrg ',
			                    'checked'=>'checked',
			                    'value'       => $v_procedure_id
			                  );
			
		
								
			endforeach;

	}
	else
	{
		
	}
	
?>
<table class="table table-condensed">
	<tr>
		<td><h3>Subtotal</h3></td>
		<td> <h3>Ksh. <?php echo number_format($total,2)?></h3></td>
	</tr>

	<tr>
		<td>Tax</td>
		<td>Ksh. 0.00</td>
	</tr>

	<tr>
		<td>Items</td>
		<td><?php echo $total_units?></td>
	</tr>

</table>
<div class="col-md-12">
	<a href="" class="btn btn-sm btn-success col-md-12"> <i class="fa fa-shopping-cart"></i> Purchase Items</a>
</div>