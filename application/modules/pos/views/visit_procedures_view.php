<?php

$order_invoice_number ='';
$preauth_date = date('Y-m-d');
$preauth_amount = '';
if(!empty($order_invoice_id))
{
	$visit_invoice_detail = $this->pos_model->get_visit_invoice_details($order_invoice_id);

	if($visit_invoice_detail->num_rows() > 0)
	{
		foreach ($visit_invoice_detail->result() as $key => $value) {
			# code...
			$order_invoice_number = $value->order_invoice_number;

		}
	}
	$visit__rs1 = $this->pos_model->get_visit_charges_charged($pos_order_id,$order_invoice_id);
}
else
{
	$visit__rs1 = $this->pos_model->get_visit_charges_charged($pos_order_id);	
}
// var_dump($visit__rs1);die()
// $visit_rs = $this->accounts_model->get_visit_details($visit_id);
$visit_type_id = 1;
$close_card = 3;
$visit_rs = $this->pos_model->get_order_details($pos_order_id);

$sale_type = 0;
$pos_order_status = 1;
$vat_status = 0;
if($visit_rs->num_rows() > 0)
{
	foreach ($visit_rs->result() as $key => $value) {
		# code...
		$sale_type = $value->sale_type;
		$pos_order_status = $value->pos_order_status;
		$vat_status = $value->vat_status;
	}
}


// var_dump($visit__rs1->result())	;die();
// echo form_open("finance/creditors/confirm_invoice_note", array("class" => "form-horizontal","id" => "confirm-invoice"));
$result = "

	<table align='center' class='table table-striped table-bordered table-condensed'>
	<tr>
		<th></th>
		<th>Invoice Item</th>
		<th>Units</th>
		<th>Unit Cost</th>
		<th>Total</th>
	

	</tr>
";
	$total= 0;  
	$total_units= 0;
	$number = 0;
	$vat_charged = 0;
	if($visit__rs1->num_rows() > 0)
	{						
		foreach ($visit__rs1->result() as $key1 => $value) :
			$v_procedure_id = $value->pos_order_item_id;
			$procedure_id = $value->service_charge_id;
			$product_code = $value->product_code;
			$pos_order_item_amount = $value->pos_order_item_amount;
			$units = $value->pos_order_item_quantity;
			$procedure_name = $value->service_charge_name;
			$service_id = $value->service_id;
			$vatable = $value->vatable;
			$product_id = $value->product_id;
			// $order_invoice_id = $value->order_invoice_id;
			// $visit_type_id = 1;
			$total= $total +($units * $pos_order_item_amount);

			if($order_invoice_id > 0)
			{
				$text_color = "success";
			}
			else
			{
				$text_color = 'default';
			}

			// if($visit_type_id == 1)
			// {
			// 	$visit = 'SELF';
			// }
			// else
			// {
			// 	$visit = 'INSURANCE';
			// }
		
			$checked="";
			$number++;

			if($vatable)
			{
				$vat_charged += ($units * $pos_order_item_amount) * 0.16;
			}

			$total_units += $units;
			$personnel_check = TRUE;
			if($personnel_check AND $close_card == 3)
			{
				$checked = "<td>
						
								<a class='btn btn-xs btn-danger' href='#' onclick='delete_procedure(".$v_procedure_id.", ".$pos_order_id.")'><i class='fa fa-trash'></i></a>
							</td>";
			}

			// if($close_card == 3)
			// {

			// }
		 $checkbox_data = array(
			                    'name'        => 'visit_invoice_items[]',
			                    'id'          => 'checkbox'.$v_procedure_id,
			                    'class'          => 'css-checkbox  lrg ',
			                    'checked'=>'checked',
			                    'value'       => $v_procedure_id
			                  );
			if($order_invoice_id > 0)
			{
				$checked = 'readonly';
			}
			else
			{
				$checked = '';
			}
		$result .='
					<tr id="procedure_id'.$v_procedure_id.'" onclick="set_procedure_status('.$v_procedure_id.','.$procedure_id.')" > 

						<td   >'.form_checkbox($checkbox_data).'<label for="checkbox'.$v_procedure_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
						<td   align="left">'.$procedure_name.'</td>
						<td   align="center">
							<input type="number" id="units'.$v_procedure_id.'" class="form-control" value="'.$units.'" size="3" onkeyup="calculatetotal('.$pos_order_item_amount.','.$v_procedure_id.', '.$procedure_id.','.$pos_order_id.')" '.$checked.'/>

							<input type="hidden" id="order_invoice_id'.$v_procedure_id.'" class="form-control" value="'.$order_invoice_id.'" size="3" />
							<input type="hidden" id="product_id'.$v_procedure_id.'" class="form-control" value="'.$product_id.'" size="3" />
						</td>
						<td   align="center"><input type="hidden" class="form-control" size="5" value="'.$pos_order_item_amount.'" id="billed_amount'.$v_procedure_id.'" onkeyup="calculatetotal('.$pos_order_item_amount.','.$v_procedure_id.', '.$procedure_id.','.$pos_order_id.')" '.$checked.'> '.$pos_order_item_amount.'</div></td>

						<td   align="center">'.number_format($units*$pos_order_item_amount,2).'</td>
					</tr>	
			';
								
			endforeach;

	}
	else
	{
		
	}
	if($vat_status)
	{
		$result .="
					<tr bgcolor='#D9EDF7'>
					<td></td>
					<td></td>
					<td></td>
					<th>VAT CHARGED: </th>
					<th colspan='1'><div id='grand_total'>".number_format(($total*0.16),2)."</div></th>
				
					";
	}
	
	$result .="
		<tr bgcolor='#D9EDF7'>
		<td></td>
		<td></td>
		<td></td>
		<th>GRAND TOTAL: </th>
		<th colspan='1'><div id='grand_total'>".number_format($total,2)."</div></th>
	
		</tr>
		 </table>
		";
	$result .= "</table>";
	$preauth_amount = $total;
	
	$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
	$job_title_id = $this->session->userdata('job_title_id');
?>








<div class="top-sale-div" style="height:50vh; overflow-y: scroll;">
	<!-- <div class="pull-right">
		<a  class="btn btn-sm btn-success" onclick="add_payment()"><i class="fa fa-plus"></i> Add Payment </a>
		 <a class='btn btn-sm btn-success'    onclick="add_invoice()"> <i class="fa fa-plus"></i> New Sale </a>  

	</div> -->
	<div class="panel-body" style="height:99%">
		<?php echo $result;?>
		<?php
		if($total_units > 0  AND $pos_order_status == 2 )
		{
			?>
			<div class="col-print-12" style="margin-bottom: 40px;">

				<div class="col-print-4">
					<a class="btn btn-sm btn-primary" id="menu-a"  onclick="print_orders_department(<?php echo $pos_order_id;?>,5)"   >  BILL ONLY  </a>
				</div>
				<div class="col-print-4">
					<a class="btn btn-sm btn-warning" id="menu-a" target="_blank" onclick="print_orders_department(<?php echo $pos_order_id;?>,0)">  PRINT </a>
				</div>
				<?php
				if($job_title_id != 4 OR $authorize_invoice_changes)
				{
					?>
					<div class="col-print-4">
						<a class="btn btn-sm btn-danger" id="menu-a" target="_blank" onclick="print_order(<?php echo $pos_order_id;?>)" > PRINT BILL  </a>
					</div>
					<?php
				}
				?>
				
				
			</div>
			<br>
			<!-- <div class="col-print-12" style="margin-top: : 15px;">
				<div class="col-print-12">
					<a class="btn btn-sm btn-default" id="menu-a" target="_blank" href="<?php echo site_url().'print-bill/'.$pos_order_id;?>"   > PRINT BILL </a>
				</div>
			</div> -->
				
			<?php
		}
		?>
	</div>

</div>

<?php


if($pos_order_status < 4)
{


	?>
		<div class="top-sale-div" style="height:40vh; background-color: white; overflow-y: scroll;">
			<div class="panel-body">

				<?php
				if($pos_order_status < 3)
				{



				?>
				<div class="col-md-12" style="height:50% !important; margin-bottom: 10px;">
					<div class="col-md-6 col-xs-6">
						<a  class="btn btn-lg btn-info" id="menu-a" onclick="add_items()"> <i class="fa fa-arrow-up"> </i>
						</a>
					</div>
					<div class="col-md-6 col-xs-6">
						<a  class="btn btn-lg btn-danger btn-delete"  style="display:none;" id="menu-a"  onclick="remove_items()"> <i class="fa fa-trash"> </i>
						</a>
					</div>
				</div>
				<?php
				}
				?>
				<div class="col-md-12" style="height:50% !important">
					<div class="col-md-6 col-xs-6">
						<?php
						// if($pos_order_status < 3 AND $authorize_invoice_changes)
						// {



						?>
						<a  class="btn btn-lg btn-info down-arrow" style="display:none" id="menu-a" onclick="reduce_items()"> <i class="fa fa-arrow-down"> </i>
						</a>
						<?php
					// }
						?>
					</div>
					<div class="col-md-6 col-xs-6">
						<!-- <a  class="btn btn-lg btn-success" id="menu-a"> POST BILL </a> -->

						<?php
						$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
						// echo $pos_order_status;

						if($total_units > 0  AND $pos_order_status == 1 )
						{
						
							?>
							<div class="col-print-6">
								<a class="btn btn-sm btn-info" id="menu-a" onclick="post_bill(0)"> POST ONLY </a>
							</div>
							<div class="col-print-6">
								<a class="btn btn-sm btn-success" id="menu-a" onclick="post_bill(1)"> POST & PRINT </a>
							</div>
							
								
							<?php	
							
						}
						else if($total_units > 0  AND $pos_order_status == 2 AND $job_title_id != 4)
						{
							?>
								<a class="btn btn-sm btn-success" id="menu-a" onclick="proceed_to_complete_sale()"  > COMPLETE SALE </a>
							<?php
						}

						else if($total_units > 0  AND $pos_order_status < 3 AND $authorize_invoice_changes AND $job_title_id != 4)
						{
							?>
								<a class="btn btn-sm btn-success" id="menu-a" onclick="complete_sale()"  > COMPLETE SALE </a>
							<?php
						}
						?>
						
					</div>
					<?php
						if($total_units > 0 AND $pos_order_status < 3  AND $job_title_id != 4)
						{
							?>
							<br>
							<div class="col-md-12">
								<a class="btn btn-sm btn-danger" id="menu-a" onclick="finalize_bill(<?php echo $preauth_amount;?>)"> FINALIZE BILL </a>
							</div>
							<?php
						}

					?>
					
				</div>
				<!-- <table class="table table-condensed">
					<tr>
						<td><h3>Subtotal</h3></td>
						<td> <h3>Ksh. <?php echo number_format($total,2)?></h3></td>
					</tr>

					<tr>
						<td>Tax (16%)</td>
						<td>Ksh. <?php echo number_format($vat_charged,2)?></td>
					</tr>

					<tr>
						<td>Items</td>
						<td><?php echo $total_units?></td>
					</tr>

				</table> -->
				<!-- <div class="col-md-12">
					
					
				</div> -->
			</div>
			
		</div>
	<?php
}

else if($pos_order_status == 3 AND $authorize_invoice_changes == 1)
{
	?>

	<div class="top-sale-div" style="height:40vh; background-color: white; overflow-y: scroll;">
			<div class="panel-body">
				<div class="col-md-12" >
					<a class="btn btn-sm btn-success" id="menu-a" onclick="verify_bill(<?php echo $pos_order_id?>)"> VERIFY BILL </a>
				</div>
			</div>
			
		</div>
	<?php
}
?>

<div class="modal fade bs-example-modal-lg" id="add_assessment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Receipt Invoice</h4>
            </div>
            <div class="modal-body">
            	<!-- <div class="row">
                	<div class='col-md-12'>
                      	<div class="form-group center-align">
								<h1 class="center-align">Ksh. <?php echo number_format($total,2)?></h1>
						</div>
						 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
                      	<div class="form-group" style="margin-top:10px;margin-left:20px;">
							<label class="col-lg-12 left-align">
							<strong> Tender:</strong> </label>
							<div class="col-lg-10">
								<input type="text" class="form-control" name="tendered_amount" id="tendered_amount" onkeyup="get_change(<?php echo $total;?>)" placeholder="" autocomplete="off" >
							</div>
						</div>

						 <input type="hidden" class="form-control" name="change" id="change" utocomplete="off">			
						  <input type="hidden" class="form-control" name="invoice_id" id="invoice_id" placeholder="Invoice id" value="<?php echo $order_invoice_id;?>">
                      	<div class="form-group" style="margin-top:10px;margin-left:20px;">
							<label class="col-lg-12 left-align">
							<strong>Change: </strong>
							</label>
						  
							<div class="col-lg-10">
								<div id="balance-div"></div>
							</div>
						</div>
                    </div>
                </div> -->
            </div>
            <div class="modal-footer">
            	<button type="submit" class='btn btn-info btn-sm' type='submit'  onclick="return confirm('Are you sure you want to finalize this sale')">Finalize Sale</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
        </div>
    </div>
</div>
 <?php //echo form_close();?>