 <section class="panel panel-info">
    <header class="panel-heading">
        <h2 class="panel-title">Search Patients</h2>
    </header>
    
    <!-- Widget content -->
   <div class="panel-body">
    	<div class="padd">
			<?php
            echo form_open("reception/search_patients", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-6">
                   
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Phone Number: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="patient_phone" placeholder="Patient Phone">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Patient number: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="patient_number" placeholder="Patient number">
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6">
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Patient name: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="surname" placeholder="Patient name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Adm No/National ID: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="patient_national_id" placeholder="Admission Number">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="center-align">
                            <button type="submit" class="btn btn-info btn-sm">Search</button>
                        </div>                    
                    </div>
            
                    
                </div>
            </div>
            <?php
            echo form_close();
            ?>
    	</div>
    </div>
</section>