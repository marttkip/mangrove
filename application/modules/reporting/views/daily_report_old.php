<?php

		$date_tomorrow = date('Y-m-d');
		$visit_date = date('jS M Y',strtotime($date_tomorrow));

		$date_tomorrow = date('Y-m-d');
		$date_tomorrow = date("Y-m-d", strtotime("-1 day", strtotime($date_tomorrow)));
		// echo $date_tomorrow;

		$branch = $this->config->item('branch_name');
		$message['subject'] =  $branch.' '.$visit_date.' report';

		$where = $where1 = $where6 = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 AND visit.visit_date = "'.$date_tomorrow.'"';
		$payments_where = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 ';
		$table = 'visit, patients';


		
		//cash payments
		$where2 = $payments_where.' AND payments.payment_method_id = 2 AND payments.payment_type = 1 AND payments.cancel = 0 AND payments.payment_created = "'.$date_tomorrow.'"';
		$total_cash_collection = $this->reports_model->get_total_cash_collection($where2, $table);
        
		// mpesa
		$where2 = $payments_where.' AND payments.payment_method_id = 5 AND payments.payment_type = 1 AND payments.cancel = 0 AND payments.payment_created = "'.$date_tomorrow.'"';
		$total_mpesa_collection = $this->reports_model->get_total_cash_collection($where2, $table);

		$where2 = $payments_where.' AND (payments.payment_method_id = 1 OR  payments.payment_method_id = 6 OR  payments.payment_method_id = 7 OR  payments.payment_method_id = 8)  AND payments.payment_type = 1 AND payments.cancel = 0 AND payments.payment_created = "'.$date_tomorrow.'"';
		$total_other_collection = $this->reports_model->get_total_cash_collection($where2, $table);


		$where4 = 'payments.payment_method_id = payment_method.payment_method_id AND payments.visit_id = visit.visit_id  AND visit.visit_delete = 0  AND visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND payments.cancel = 0 AND payments.payment_type = 2 AND payments.payment_created = "'.$date_tomorrow.'"';
		$total_waiver = $this->reports_model->get_total_cash_collection($where4, 'payments, visit, patients, visit_type, payment_method', 'cash');





		 // var_dump($total_other_collection+$total_mpesa_collection+$total_cash_collection); die();
		
		//count outpatient visits
		$where2 = $where1.' AND visit.inpatient = 0';

		(int)$outpatients = $this->reception_model->count_items($table, $where2);
		// var_dump($outpatients); die();
		//count inpatient visits
		$where2 = $where6.' AND visit.inpatient = 1';

		(int)$inpatients = $this->reception_model->count_items($table, $where2);


		$table1 = 'petty_cash,account';
		$where1 = 'petty_cash.account_id = account.account_id AND (account.account_name = "Cash Box" OR account.account_name = "Cash Collection") AND petty_cash.petty_cash_delete = 0';
		
		
		$where1 .=' AND petty_cash.petty_cash_date = "'.$date_tomorrow.'"';
		$total_transfers = $this->reports_model->get_total_transfers($where1,$table1);

		$total_patients = $outpatients + $inpatients;

		$visit_types_rs = $this->reception_model->get_visit_types();
		$visit_results = '';
		$total_balance = 0;
		$total_invoices = 0;
		$total_payments = 0;
		$total_patients = 0;
		$total_cash_invoices = 0;
		$total_insurance_invoices = 0;

		$total_cash_payments = 0;
		$total_insurance_payments = 0;

		$total_cash_balance = 0;
		$total_insurance_balance = 0;

		if($visit_types_rs->num_rows() > 0)
		{
			foreach ($visit_types_rs->result() as $key => $value) {
				# code...

				$visit_type_name = $value->visit_type_name;
				$visit_type_id = $value->visit_type_id;


				$table = 'visit';
				$where = 'visit.visit_delete = 0 AND visit_type = '.$visit_type_id.' AND visit.visit_date = "'.$date_tomorrow.'"';
				$total_visit_type_patients = $this->reception_model->count_items($table,$where);

				// calculate invoiced amounts
				$report_response = $this->reports_model->get_visit_type_invoice($visit_type_id,$date_tomorrow);

				$invoice_amount = $report_response['invoice_total'];
				$payments_value = $report_response['payments_value'];
				$balance = $report_response['balance'];

				if($visit_type_id == 1)
				{
					$total_cash_invoices += $invoice_amount;
					$total_cash_payments += $payments_value;
					$total_cash_balance += $balance;
				}
				else
				{
					$total_insurance_invoices += $invoice_amount;
					$total_insurance_payments += $payments_value;
					$total_insurance_balance += $balance;
				}	

				// calculate amounts paid
				if($total_visit_type_patients > 0)
				{
					$visit_results .='<tr>
								  		<td style="text-align:left;"> '.strtoupper($visit_type_name).'  </td>
								  		<td style="text-align:center;"> '.$total_visit_type_patients.'</td>
								  		<td style="text-align:center;"> '.number_format($invoice_amount,2).'</td>
								  		<td style="text-align:center;"> '.number_format($payments_value,2).'</td>
								  		<td style="text-align:center;"> '.number_format($balance,2).'</td>
								  	</tr>';
				}
				$total_patients = $total_patients + $total_visit_type_patients;
				$total_invoices = $total_invoices + $invoice_amount;
				$total_payments = $total_payments + $payments_value;
				$total_balance = $total_balance + $balance;


			}

			$visit_results .='<tr>
							  		<td style="text-align:left;" colspan="1"> TOTAL </td>
							  		<td style="text-align:center;border-top:2px solid #000;" > '.$total_patients.' </td>
							  		<td style="text-align:center;border-top:2px solid #000;">Ksh. '.number_format($total_invoices,2).'</td>
							  		<td style="text-align:center;border-top:2px solid #000;">Ksh. '.number_format($total_payments,2).'</td>
							  		<td style="text-align:center;border-top:2px solid #000;">Ksh.'.number_format($total_balance,2).'</td>
							  	</tr>';
		}


$this->db->where('shift_type = 0 ');
$this->db->order_by('shift_id','DESC');
$this->db->limit(1);
$query = $this->db->get('personnel_shift');
$shifts_results = '';
if($query->num_rows() > 0)
{
	$total_shift_balance = 0;
	foreach ($query->result() as $key => $value) {
		# code...
		$time = $value->time;
		$amount = $value->amount;
		$mpesa_amount = $value->mpesa_amount;
		$expense_amount = $value->expense_amount;
		$created = $value->created;
		$closing_date = date('jS M Y',strtotime($created));
		$closing_time = date('H:i:s A',strtotime($time));
		$total_shift_balance = $amount;
	}

	$shifts_results .= '<h4 style="text-decoration:underline">Shift Closed Summary Report</h4>
							<p><strong>Closing Date & Time :</strong> '.$closing_date.' '.$closing_time.'</p>
							<p><strong>Cash Collected : </strong> '.number_format($amount+$expense_amount,2).'</p>
							<p><strong>Mpesa Collected : </strong> '.number_format($mpesa_amount,2).'</p>
							<p><strong>Expensed Amount : </strong> '.number_format($expense_amount,2).'</p>
							<p><strong style="text-decoration:underline">Cash Shift Balance : </strong> '.number_format($total_shift_balance,2).'</p>
						';
}





$this->db->where('shift_type = 1');
$this->db->order_by('shift_id','DESC');
$this->db->limit(1);
$query_opening = $this->db->get('personnel_shift');

if($query_opening->num_rows() > 0)
{  
	$total_opening_shift_balance = 0;
	foreach ($query_opening->result() as $key => $value_opening) {
		# code...
		$time = $value_opening->time;
		$amount = $value_opening->amount;
		$mpesa_amount = $value_opening->mpesa_amount;
		$expense_amount = $value_opening->expense_amount;
		$created = $value_opening->created;
		$opening_date = date('jS M Y',strtotime($created));
		$opening_time = date('H:i:s A',strtotime($time));
		$total_opening_shift_balance = $mpesa_amount+ $amount - $expense_amount;
	}

	$shifts_results .= '<h4 style="text-decoration:underline">Opening Shift Summary Report</h4>
							<p><strong>Closing Date & Time :</strong> '.$opening_date.' '.$opening_time.'</p>
							<p><strong>Cash Collected : </strong> '.number_format($amount,2).'</p>
							<p><strong>Mpesa Collected : </strong> '.number_format($mpesa_amount,2).'</p>
							<p><strong>Expensed Amount : </strong> '.number_format($expense_amount,2).'</p>
							<p><strong>Shift Balance : </strong> '.number_format($total_opening_shift_balance,2).'</p>
						';
}

$shifts_results .= '<h4 style="text-decoration:underline">Total Collection</h4>
							<p><strong>Total Collection: </strong> '.number_format($total_shift_balance + $total_opening_shift_balance,2).'</p>
						';
		

echo '<p>Good evening to you,<br>
		Herein is a report of yesterdays transactions. This is sent at '.date('H:i:s A').'
		</p>
		'.$shifts_results.'

		<h4 style="text-decoration:underline"><strong>CASH VS INSURANCE SUMMARY</strong></h4>
		<table  class="table table-hover table-bordered ">
				<thead>
					<tr>
						<th style="padding:5px;">TYPE</th>
						<th style="padding:5px;">INVOICE AMOUNT (KES) </th>
						<th style="padding:5px;">PAYMENTS (KES) </th>
						<th style="padding:5px;">BALANCE (KES) </th>
					</tr>
				</thead>
				</tbody>
		  	<tr>
		  		<td>CASH  </td>
		  		<td style="text-align:center;"> '.number_format($total_cash_invoices,2).'</td>
		  		<td style="text-align:center;"> '.number_format($total_cash_payments,2).'</td>
		  		<td style="text-align:center;"> '.number_format($total_cash_balance,2).'</td>
		  	</tr>
		  	<tr>
		  		<td>INSURANCE </td>
		  		<td style="text-align:center;"> '.number_format($total_insurance_invoices,2).'</td>
		  		<td style="text-align:center;"> '.number_format($total_insurance_payments,2).'</td>
		  		<td style="text-align:center;"> '.number_format($total_insurance_balance,2).'</td>
		  	</tr>

		  	<tr>
		  		<td>TOTAL</td>
		  		<td style="text-align:center;border-top:2px solid #000;"> '.number_format($total_cash_invoices + $total_insurance_invoices,2).'</td>
		  		<td style="text-align:center;border-top:2px solid #000;"> '.number_format($total_cash_payments+$total_insurance_payments,2).'</td>
		  		<td style="text-align:center;border-top:2px solid #000;"> '.number_format($total_cash_balance+$total_insurance_balance,2).'</td>
		  	</tr>
		  	
		  	</tbody>

		</table>


		<h4 style="text-decoration:underline"><strong>COLLECTIONS SUMMARY</strong></h4>
		<table  class="table table-hover table-bordered ">
				<thead>
					<tr>
						<th width="50%"></th>
						<th width="50%"></th>
					</tr>
				</thead>
				</tbody>
		  	<tr>
		  		<td>CASH COLLECTED </td><td>KES. '.number_format($total_cash_collection,2).'</td>
		  	</tr>
		  	<tr>
		  		<td>MPESA COLLECTED </td><td> KES. '.number_format($total_mpesa_collection,2).'</td>
		  	</tr>
		  	<tr>
		  		<td>OTHER COLLECTION</td><td> KES. '.number_format($total_other_collection,2).'</td>
		  	</tr>
		  	
		  	<tr>
		  		<td>CASH - PETTY CASH TRANSFER </td><td> ( KES. '.number_format($total_transfers,2).' )</td>
		  	</tr>
		  	<tr>
		  		<td><strong>REVENUE</strong> </td><td><strong> KES. '.number_format(($total_mpesa_collection + $total_cash_collection + $total_other_collection) - $total_transfers,2).' </strong></td>
		  	</tr>
		  	<tr>
		  		<td><strong>WAIVERS</strong> </td><td> KES. '.number_format($total_waiver,2).'</td>
		  	</tr>
		  	</tbody>

		</table>

		<h4 style="text-decoration:underline"><strong>VISIT SUMMARY</strong></h4>
		<table  class="table table-hover table-bordered ">
			<thead>
				<tr>
					<th style="padding:5px;">PATIENT TYPE</th>
					<th style="padding:5px;">NO</th>
					<th style="padding:5px;">AMOUNT INVOICED</th>
					<th style="padding:5px;">AMOUNT COLLECTED</th>
					<th style="padding:5px;">DEBT</th>
				</tr>
			</thead>
			</tbody> 
			  	'.$visit_results.'
		  	</tbody>
		</table>


		';
?>