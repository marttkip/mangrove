DELIMITER $$
DROP TRIGGER IF EXISTS `t_product_update` $$
CREATE TRIGGER t_product_update AFTER UPDATE on product
FOR EACH ROW
BEGIN

	UPDATE product_stock_level 
		SET  
			 product_stock_level.category_id = `product`.`category_id`,
		WHERE product_stock_level.product_id = NEW.product_id
		
END $$
DELIMITER;
