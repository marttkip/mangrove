DELIMITER $$
DROP TRIGGER IF EXISTS `patient_journal_INSERT` $$
CREATE TRIGGER `patient_journal_INSERT` 
AFTER INSERT ON `visit_charge`
FOR EACH ROW
BEGIN
    INSERT INTO patients_journal (
	    							transaction_id, 
									reference_id,
									reference_code,
									transactionCode,
									patient_id,
									service_charge_id,
									payment_type,
									payment_type_name,
									transaction_description,
									dr_amount,
									cr_amount,
									transaction_date,
									created_at,
									status,
									party,
									transactionCategory,
									transactionClassification,
									transactionTable,
									referenceTable
								)
    

	SELECT
	visit_charge.visit_charge_id,
	visit.visit_id,
	visit.invoice_number,
	'',
	visit.patient_id,
	visit_charge.service_charge_id,
	visit.visit_type,
	visit_type.visit_type_name,	
	CONCAT( "Charged for ", service_charge.service_charge_name ),
	(visit_charge.visit_charge_amount*visit_charge.visit_charge_units),
	'0',
	visit_charge.date,
	visit_charge.visit_charge_timestamp,
	visit_charge.charged,
	'Patient',
	'Revenue',
	'Invoice Patients',
	'visit_charge',
	'visit'
	FROM
		visit_charge
		JOIN visit ON visit.visit_id = visit_charge.visit_id
		LEFT JOIN service_charge ON service_charge.service_charge_id = visit_charge.service_charge_id
		LEFT JOIN visit_type ON visit_type.visit_type_id = visit.visit_type
		WHERE visit.visit_delete = 0  AND visit_charge.visit_charge_delete = 0;
END $$
DELIMITER ;
