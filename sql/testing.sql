CREATE OR REPLACE VIEW v_current_payable AS
SELECT creditor.creditor_id,COALESCE(sum(v_payable_invoices.dr_amount),0) as dr_amount,
COALESCE(sum(v_payable_payments.cr_amount),0) as cr_amount,
COALESCE(sum(v_payable_invoices.dr_amount),0) - COALESCE(sum(v_payable_payments.cr_amount),0) as balance
FROM
 creditor
LEFT JOIN v_payable_invoices ON v_payable_invoices.recepientId = creditor.creditor_id 
AND v_payable_invoices.recepientId > 0 
AND v_payable_invoices.transactionDate >= creditor.start_date 
AND DATEDIFF( CURDATE( ), v_payable_invoices.transactionDate) = 0
LEFT JOIN v_payable_payments on  v_payable_invoices.referenceId = v_payable_payments.invoice_id
GROUP BY creditor.creditor_id;

CREATE OR REPLACE VIEW v_thirty_payable AS
SELECT creditor.creditor_id,COALESCE(sum(v_payable_invoices.dr_amount),0) as dr_amount,
COALESCE(sum(v_payable_payments.cr_amount),0) as cr_amount,
COALESCE(sum(v_payable_invoices.dr_amount),0) - COALESCE(sum(v_payable_payments.cr_amount),0) as balance
FROM
 creditor
LEFT JOIN v_payable_invoices ON v_payable_invoices.recepientId = creditor.creditor_id 
AND v_payable_invoices.recepientId > 0 
AND v_payable_invoices.transactionDate >= creditor.start_date 
AND DATEDIFF( CURDATE( ), v_payable_invoices.transactionDate) BETWEEN 1 AND 30
LEFT JOIN v_payable_payments on  v_payable_invoices.referenceId = v_payable_payments.invoice_id
GROUP BY creditor.creditor_id;

CREATE OR REPLACE VIEW v_sixty_payable AS
SELECT creditor.creditor_id,COALESCE(sum(v_payable_invoices.dr_amount),0) as dr_amount,
COALESCE(sum(v_payable_payments.cr_amount),0) as cr_amount,
COALESCE(sum(v_payable_invoices.dr_amount),0) - COALESCE(sum(v_payable_payments.cr_amount),0) as balance
FROM
 creditor
LEFT JOIN v_payable_invoices ON v_payable_invoices.recepientId = creditor.creditor_id 
AND v_payable_invoices.recepientId > 0 
AND v_payable_invoices.transactionDate >= creditor.start_date 
AND DATEDIFF( CURDATE( ), v_payable_invoices.transactionDate) BETWEEN 31 AND 60
LEFT JOIN v_payable_payments on  v_payable_invoices.referenceId = v_payable_payments.invoice_id
GROUP BY creditor.creditor_id;


CREATE OR REPLACE VIEW v_ninety_payable AS
SELECT creditor.creditor_id,COALESCE(sum(v_payable_invoices.dr_amount),0) as dr_amount,
COALESCE(sum(v_payable_payments.cr_amount),0) as cr_amount,
COALESCE(sum(v_payable_invoices.dr_amount),0) - COALESCE(sum(v_payable_payments.cr_amount),0) as balance
FROM
 creditor
LEFT JOIN v_payable_invoices ON v_payable_invoices.recepientId = creditor.creditor_id 
AND v_payable_invoices.recepientId > 0 
AND v_payable_invoices.transactionDate >= creditor.start_date 
AND DATEDIFF( CURDATE( ), v_payable_invoices.transactionDate) BETWEEN 61 AND 90
LEFT JOIN v_payable_payments on  v_payable_invoices.referenceId = v_payable_payments.invoice_id
GROUP BY creditor.creditor_id;



CREATE OR REPLACE VIEW v_over_ninety_payable_invoices AS
SELECT creditor.creditor_id,COALESCE(sum(v_payable_invoices.dr_amount),0) as dr_amount,
COALESCE(sum(v_payable_payments.cr_amount),0) as cr_amount,
COALESCE(sum(v_payable_invoices.dr_amount),0) - COALESCE(sum(v_payable_payments.cr_amount),0) as balance
FROM
 creditor
LEFT JOIN v_payable_invoices ON v_payable_invoices.recepientId = creditor.creditor_id 
AND v_payable_invoices.recepientId > 0 
AND v_payable_invoices.transactionDate >= creditor.start_date 
AND v_payable_invoices.transactionClassification = 'Creditors Invoices'
AND DATEDIFF( CURDATE( ), date( v_payable_invoices.transactionDate ) ) > 90 
LEFT JOIN v_payable_payments on  v_payable_invoices.referenceId = v_payable_payments.invoice_id
GROUP BY creditor.creditor_id;


-- CREATE OR REPLACE VIEW v_aged_payable AS 

SELECT 
  creditor.creditor_id AS recepientId,
  creditor.creditor_name as payables,
  v_current_payable.balance AS `coming_due`,
  v_thirty_payable.balance AS `thirty_days`,
  v_sixty_payable.balance AS `sixty_days`,
  v_ninety_payable.balance AS `ninety_days`,
  v_over_ninety_payable.balance AS `over_ninety_days`,
  v_current_payable.balance + v_thirty_payable.balance + v_sixty_payable.balance + v_ninety_payable.balance + v_over_ninety_payable.balance AS `Total`
FROM
 creditor
LEFT JOIN v_current_payable ON v_current_payable.creditor_id = creditor.creditor_id
LEFT JOIN v_thirty_payable ON v_thirty_payable.creditor_id = creditor.creditor_id
LEFT JOIN v_sixty_payable ON v_sixty_payable.creditor_id = creditor.creditor_id
LEFT JOIN v_sixty_payable ON v_sixty_payable.creditor_id = creditor.creditor_id
LEFT JOIN v_ninety_payable ON v_ninety_payable.creditor_id = creditor.creditor_id
LEFT JOIN v_over_ninety_payable ON v_over_ninety_payable.creditor_id = creditor.creditor_id